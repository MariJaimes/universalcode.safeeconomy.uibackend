import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { errorRoute, navbarRoute } from './layouts';
import { DEBUG_INFO_ENABLED } from './app.constants';
import {MAIN_ROUTE} from './layouts/main/main.route';
import {MainSafeEconomyRoute} from './SafeEconomyComponents/main-safe-economy/main-safe-economy.route';
import {LandingPageMainRoute} from './SafeEconomyComponents/safe-economy-landig-page/safe-economy-landing-main/landing-page-route';

const LAYOUT_ROUTES = [
    navbarRoute,
    MAIN_ROUTE,
    LandingPageMainRoute,
    MainSafeEconomyRoute,
    ...errorRoute
];

@NgModule({
    imports: [
        RouterModule.forRoot(LAYOUT_ROUTES, { useHash: true , enableTracing: DEBUG_INFO_ENABLED })
    ],
    exports: [
        RouterModule
    ]
})
export class SafeEconomyAppRoutingModule {}
