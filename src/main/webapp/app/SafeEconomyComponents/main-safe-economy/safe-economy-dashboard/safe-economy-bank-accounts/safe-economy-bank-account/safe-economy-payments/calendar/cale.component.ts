import { Component, Input } from '@angular/core';

@Component({
    selector: 'cale',
    template: `<h1>Hello {{name}}!</h1>`,
    styles: [`h1 { font-family: Lato; }`]
})
export class CaleComponent  {
    @Input() name: string;
}
