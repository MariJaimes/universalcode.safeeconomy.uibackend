import {Route} from '@angular/router';
import {SafeEconomyPaymentsComponent} from './safe-economy-payments.component';

export const PaymentsSafeEconomy: Route = {
    path: 'payments',
    component: SafeEconomyPaymentsComponent
};
