import { Component, OnInit } from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {DetailsService} from '../bank-account-detail/details.service';

@Component({
  selector: 'jhi-modal-account-delete',
  templateUrl: './delAccount.component.html',
  styles: []
})
export class DelAccountComponent implements OnInit {

    account;

    constructor(private close: NgbActiveModal,
                private detailService: DetailsService) { }

    ngOnInit() {
        this.account = this.detailService.getAccount();
    }

    confirmDeleteAccount() {
        this.detailService.deleteAccount(this.account, (res) => {
            this.detailService.removeAccount(this.account);
            setTimeout(() => {
                this.cancelDeleteAccount();
            }, 2000);
        });
    }
    cancelDeleteAccount() {
        this.close.dismiss('close');
        this.account = this.detailService.setAccountToUpdate(null);
    }

}
