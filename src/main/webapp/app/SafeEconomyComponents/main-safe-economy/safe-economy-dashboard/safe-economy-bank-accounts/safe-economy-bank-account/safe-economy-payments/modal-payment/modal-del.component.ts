import { Component, OnInit } from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {PaymentsService} from '../payments.service';

@Component({
  selector: 'jhi-modal-confirm-delete',
  templateUrl: './modal-del.component.html',
  styles: []
})
export class ModalDelComponent implements OnInit {

    payment;

    constructor(private close: NgbActiveModal,
                private paymentsService: PaymentsService) { }

    ngOnInit() {
        this.payment = this.paymentsService.getPayment();
    }

    confirmDelete() {
        this.paymentsService.deletePayment(this.payment.id, (res) => {
        this.paymentsService.removePayment(this.payment);
        setTimeout(() => {
            this.cancelDelete();
        }, 2000);
        });
    }
    cancelDelete() {
        this.close.dismiss('close');
        this.payment = this.paymentsService.setPaymentToUpdate(null);
    }

}
