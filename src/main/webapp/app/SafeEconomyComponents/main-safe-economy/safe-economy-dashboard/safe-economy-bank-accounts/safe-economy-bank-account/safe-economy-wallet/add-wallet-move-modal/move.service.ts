import {Injectable} from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import {SERVER_API_URL} from '../../../../../../../app.constants';
import {JhiDateUtils} from 'ng-jhipster';
import {BankMovementPayment} from '../../../../../../../entities/bank-movement-payment/bank-movement-payment.model';
export type EntityResponseType = HttpResponse<BankMovementPayment>;

@Injectable()
export class MoveService {
    movement;
    private resourceUrl =  SERVER_API_URL + 'api/bank-movement-payments';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(bankMovementPaymenth: BankMovementPayment): Observable<EntityResponseType> {
        const copy = this.convert(bankMovementPaymenth);
        return this.http.post<BankMovementPayment>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    setMovementToUpdate( movement ) {
        this.movement = movement;
    }
    getMovement() {
        return this.movement;
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: BankMovementPayment = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertItemFromServer(bankMovementPaymenth: BankMovementPayment): BankMovementPayment {
        const copy: BankMovementPayment = Object.assign({}, bankMovementPaymenth);
        copy.dateOfMovement = this.dateUtils
            .convertLocalDateFromServer(bankMovementPaymenth.dateOfMovement);
        return copy;
    }

    private convert(bankMovementPaymenth: BankMovementPayment): BankMovementPayment {
        const copy: BankMovementPayment = Object.assign({}, bankMovementPaymenth);
        console.log(bankMovementPaymenth);
        copy.dateOfMovement = this.dateUtils
            .convertLocalDateFromServer(bankMovementPaymenth.dateOfMovement);
        return copy;
    }
}
