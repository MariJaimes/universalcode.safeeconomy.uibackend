import {Component, OnInit, ViewChild} from '@angular/core';
import {Options} from 'fullcalendar';
import {PaymentsService} from '../payments.service';
import {SafeEconomyProfileService} from '../../../../../auth/profile-safe-economy.service';

@Component({
    selector: 'jhi-calendar',
    templateUrl: './calendar.component.html',
    styleUrls: ['./calendar.css']
})
export class CalendarTestComponent implements OnInit {
    calendarOptions: Options;
    displayEvent: any;
    payments;
    paymentsCopy;
    @ViewChild(CalendarTestComponent) ucCalendar: CalendarTestComponent;

    constructor(protected eventService: PaymentsService,
                private safeEconomyProfileService: SafeEconomyProfileService) {
    }

    ngOnInit() {
        this.safeEconomyProfileService.setSafeEconomyUser((user) => {
            this.eventService.getMovementByUser(user.id, (payments) => {
                this.payments = Object.keys(payments).map((i) => payments[i]).filter((item) =>
                    item && item.kindOfMovement === 'Pago');
                this.eventService.setPayments(this.payments);
                this.paymentsCopy = [...this.payments];
                this.getCurrentPaymentsCalendar();
                this.eventService.getEvents().subscribe((data) => {
                    this.calendarOptions = {
                        editable: true,
                        eventLimit: false,
                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'month,agendaWeek,agendaDay,listMonth'
                        },
                        events: data
                    };
                });
            });
        });
    }

    getCurrentPaymentsCalendar() {
        this.eventService.currentList.subscribe((temp) => {
            this.payments = Object.keys(temp).map((i) => temp[i]);
            temp = this.payments.filter((item) => item);
            this.payments = temp;
            this.paymentsCopy = [...this.payments];
        });
    }

    clickButtonn(model: any) {
        this.displayEvent = model;
    }

    eventButtonClick(model: any) {
        model = {
            event: {
                id: model.event.id,
                start: model.event.start,
                end: model.event.end,
                title: model.event.title,
                allDay: model.event.allDay
                // other params
            },
            duration: {}
        };
        this.displayEvent = model;
    }

    updateEventt(model: any) {
        model = {
            event: {
                id: model.event.id,
                start: model.event.start,
                end: model.event.end,
                title: model.event.title
                // other params
            },
            duration: {
                _data: model.duration._data
            }
        };
        this.displayEvent = model;
    }

    dayClick(model: any) {
        const element = new Date(model.detail.date['_i']);
        element.setDate(element.getDate() + 1);
    }
}
