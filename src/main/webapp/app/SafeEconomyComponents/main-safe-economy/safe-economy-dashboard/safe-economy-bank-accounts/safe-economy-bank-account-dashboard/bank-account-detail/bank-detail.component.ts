import {Component, Input, OnInit} from '@angular/core';
import {BankAccountModalService} from '../bank-account-modal.service';
import {DetailsService} from './details.service';

@Component({
    selector: 'jhi-bank-account-detail',
    templateUrl: './bank-detail.component.html',
    styleUrls: ['../bank-accounts-detail-styles.css']
})
export class BankAccountDetailComponent implements OnInit {
    @Input() currentBankAccount;
    @Input() currentBank;

    constructor(private modalService: BankAccountModalService,
                private detailsService: DetailsService) {
    }

    ngOnInit() {
    }

    showModalOfUpdate(bankAccount) {
        this.detailsService.setAccountToUpdate(bankAccount);
        this.modalService.openAccount();
    }
    showModalOfDelete(account) {
        this.detailsService.setAccountToUpdate(account);
        this.modalService.openAccountDelete();
    }
}
