import { Component, OnInit } from '@angular/core';
import {MoveService} from './move.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {WalletMoveService} from '../wallet-move.service';

@Component({
    selector: 'jhi-safe-economy-confirm',
    templateUrl: './dele.component.html',
})
export class DeleteComponent implements OnInit {
    movement;
    constructor( private walletMoveService: WalletMoveService,
                private safeEconomyAddMovementService: MoveService,
                private close: NgbActiveModal) {
    }
    ngOnInit() {
        this.movement = this.safeEconomyAddMovementService.getMovement();
    }
    confirmDelete() {
        this.walletMoveService.deleteMovement(this.movement.id, (res) => {
            this.walletMoveService.removeMovement(this.movement);
            setTimeout(() => {
                this.cancelDelete();
            }, 2000);
        });
    }
    cancelDelete() {
        this.close.dismiss('close');
        this.movement = this.safeEconomyAddMovementService.setMovementToUpdate(null);
    }
}
