import { Component, OnInit } from '@angular/core';
import { NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-safe-economy',
    templateUrl: './wallet-move.component.html',
    styleUrls: ['./wallet-move.component.css'],
})
export class WallettComponent implements OnInit {

    modalRef: NgbModalRef;

    constructor() { }

    ngOnInit() {
    }
}
