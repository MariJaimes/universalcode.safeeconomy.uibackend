import {Injectable} from '@angular/core';
import {BankAccountService} from '../../../../entities/bank-account/bank-account.service';

@Injectable()
export class SafeEconomyBankAccountsService {
    banks;
    constructor( private bankAccountService: BankAccountService) {}

    getAllBankAccountsByUser(user, callback) {

        this.bankAccountService.findAllAccountsByUserAndActive(user.id).subscribe((res) => {
            this.banks = Object.keys(res.body).map((i) => res.body[i]).filter((item) => item);
            callback(this.banks);
        });
    }
    getBanks() {
        return this.banks;
    }
    getBankById(id, callback) {
        this.bankAccountService.find(id).subscribe((res) => {
            callback(res.body);
        });
    }

}
