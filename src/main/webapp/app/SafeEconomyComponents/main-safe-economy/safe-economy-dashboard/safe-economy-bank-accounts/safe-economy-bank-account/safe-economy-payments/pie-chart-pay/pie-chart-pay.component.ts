import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import { Chart } from 'chart.js';
import {ChartService} from '../../../../../../safe-economy-utils/charts/chart.service';
import {PaymentsService} from '../payments.service';

@Component({
  selector: 'jhi-pie-chart-pay',
  templateUrl: './pieChartPay.component.html',
  styles: []
})
export class PieChartPayComponent implements OnInit {
    @Input() array;
    @Input() type;
    @ViewChild('chart') pieChart: ElementRef;
    chart = [];
    config;

    constructor(private chartsService: ChartService,
                private paymentService: PaymentsService) { }

    ngOnInit() {
        this.buildPieChart();
    }

    buildPieChart() {
        this.config = null;
        this.paymentService.currentList.subscribe((temp) => {
            temp = temp.filter((item) => item.kindOfMovement === 'Pago');
            temp ? this.array = temp : this.array = [];
            if (this.array.length === 0) {
                this.config = this.arrayOutDataPay();
            }else {
                this.config = this.arrayWithDataPay();
            }
            this.chart = new Chart(this.pieChart.nativeElement.getContext('2d'), this.config);
        });
    }
    arrayOutDataPay() {
        const values = this.chartsService.getTestingValue(1);
        const colors = this.chartsService.getColor(values.length);
        return this.config = {
            type: 'pie',
            data: {
                datasets: [{
                    data: values,
                    backgroundColor: colors,
                    label: 'Dataset 1'
                }],
                labels: [
                    'Pagos',
                ]
            },
            options: {
                responsive: true
            }
        };
    }
    arrayWithDataPay() {
        const values = this.array.map((item) => item.acreditationAmmount);
        const colors = this.chartsService.getColor(values.length);
        return this.config = {
            type: 'pie',
            data: {
                datasets: [{
                    data: values,
                    backgroundColor: colors,
                    label: this.array.map((item) => item.comment)
                }],
                labels: this.array.map((item) => item.comment)
            },
            options: {
                responsive: true
            }
        };
    }
}
