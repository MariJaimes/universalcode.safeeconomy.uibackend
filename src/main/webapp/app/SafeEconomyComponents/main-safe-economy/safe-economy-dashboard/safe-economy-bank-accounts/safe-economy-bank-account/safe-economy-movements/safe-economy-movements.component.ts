import {Component, OnInit} from '@angular/core';
import {SafeEconomyMovementsService} from './movements.service';
import {SafeEconomyProfileService} from '../../../../auth/profile-safe-economy.service';

@Component({
    selector: 'jhi-safe-economy-movements',
    templateUrl: './safe-economy-movements.component.html',
    styles: []
})
export class SafeEconomyMovementsComponent implements OnInit {

    allMovements;
    allMovementsForPieChart;
    allMovementsForBarChart;
    allAccounts;
    accountSafeEconomy;
    allMovementsCopy;
    dateStartMovement;
    dateLastMovement;
    todayMove;

    constructor(private movementsService: SafeEconomyMovementsService,
                private safeEconomyProfileService: SafeEconomyProfileService) {
    }

    ngOnInit() {
        this.accountSafeEconomy = null;
        this.safeEconomyProfileService.setSafeEconomyUser((user) => {
            this.movementsService.getAccountsByUserAndActive((accounts) => {
                this.allAccounts = Object.keys(accounts).map((i) => accounts[i]).filter((item) => item);
                this.accountSafeEconomy = this.allAccounts.filter((item) => item.bankEntityId === 1);
                this.allAccounts = Object.keys(this.allAccounts).map((i) => this.allAccounts[i]).filter((item) => item && item.bank !== 'Safe Economy');
            }, user);
            this.movementsService.getMovementByUserAndActive((allMovements) => {
                this.allMovements = Object.keys(allMovements).map((i) => allMovements[i]).filter((item) => item && item.bankAccountId !== this.accountSafeEconomy[0].id);
                this.allMovements = this.orderByDate(this.allMovements);
                this.getFormattedDateAllMovements(this.allMovements);
                this.getFormattedDescriptionAllMovements(this.allMovements);
                this.allMovementsCopy = [...this.allMovements];
                this.allMovementsForBarChart = this.allMovements.filter((item) => item.kindOfMovement === 'Gasto');
                this.allMovementsForBarChart = this.orderByDate(this.allMovementsForBarChart);
                this.allMovementsForPieChart = this.allMovements.filter((item) => item.kindOfMovement === 'Ingreso');
                this.allMovementsForPieChart = this.orderByDate(this.allMovementsForPieChart);
            }, user.id);
        });
    }
    getFilterByDateMovement(value) {
        this.parseDateMovement();
        this.getSelectMovementByType(value);
        if (this.dateLastMovement) {
            if (this.dateStartMovement) {
                this.allMovements = this.allMovementsCopy.filter((item) => item.dateOfMovement >= this.dateStartMovement && item.dateOfMovement <= this.dateLastMovement);
                this.allMovements = this.orderByDate(this.allMovements);
            } else {
                this.allMovements = this.allMovementsCopy.filter((item) => item.dateOfMovement <= this.dateLastMovement);
                this.allMovements = this.orderByDate(this.allMovements);
            }
        } else {
            if (this.dateStartMovement) {
                this.allMovements = this.allMovementsCopy.filter((item) => item.dateOfMovement >= this.dateStartMovement);
                this.allMovements = this.orderByDate(this.allMovements);
            } else {
                this.allMovements = [...this.allMovementsCopy];
                this.allMovements = this.orderByDate(this.allMovements);
            }
        }
    }
    getSelectByAccountMovement(accountSelect) {
        if (accountSelect) {
            this.allMovements = this.allMovementsCopy.filter((item) => item.bankAccountId === parseInt(accountSelect, 10));
            this.allMovements = this.orderByDate(this.allMovements);
        } else {
            this.allMovements = [...this.allMovementsCopy];
            this.allMovements = this.orderByDate(this.allMovements);
        }
    }
    parseDateMovement() {
        this.todayMove = new Date();
        const dd = this.todayMove.getDate();
        const mm = this.todayMove.getMonth() + 1;
        const yyyy = this.todayMove.getFullYear();
        return this.todayMove = yyyy + '-' + 0 + mm + '-' + dd;
    }
    getBankByIdAllMovements(id) {
        return this.allAccounts.filter((item) => item.id === id)[0].bank;
    }

    getSelectMovementByType(value) {
        if ((value === 'Gastos') || (value === 'Ingresos')) {
            if (value === 'Gastos') {
                this.allMovements = this.allMovementsCopy.filter((item) => item.kindOfMovement === 'Gasto');
                this.allMovements = this.orderByDate(this.allMovements);
            } else if (value === 'Ingresos') {
                this.allMovements = this.allMovementsCopy.filter((item) => item.kindOfMovement === 'Ingreso');
                this.allMovements = this.orderByDate(this.allMovements);
            }
        } else if (value === 'Sin filtro') {
            this.allMovements = [...this.allMovementsCopy];
            this.allMovements = this.orderByDate(this.allMovements);
        }
    }

    private getFormattedDateAllMovements(allMovements) {
        this.allMovements.forEach((x) => x.dateOfMovement = x.dateOfMovement.substr(0, 10));
        return allMovements;
    }

    private getFormattedDescriptionAllMovements(allMovements) {
        this.allMovements.forEach((x) => x.description = x.description.split(':')[1]);
        return allMovements;
    }

    orderByDate(arr) {
        return arr.slice().sort(function(a, b) {
            return a.dateOfMovement > b.dateOfMovement ? -1 : 1;
        });
    }
}
