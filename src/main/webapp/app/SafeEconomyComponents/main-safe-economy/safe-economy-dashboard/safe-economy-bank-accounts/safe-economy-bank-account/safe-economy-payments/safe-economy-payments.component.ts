import { Component, OnInit } from '@angular/core';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {SafeEconomyProfileService} from '../../../../auth/profile-safe-economy.service';
import {PaymentsService} from './payments.service';
import {PaymentsModalService} from './payments-modal.service';
import {SafeEconomyMovementsService} from '../safe-economy-movements/movements.service';

@Component({
  selector: 'jhi-safe-economy-payments',
  templateUrl: './safe-economy-payments.component.html',
  styles: []
})
export class SafeEconomyPaymentsComponent implements OnInit {

    modalRef: NgbModalRef;
    types;
    payments = null;
    movementsForBarChartPay;
    paymentsCopy;
    accounts;
    priority;

    constructor(private safeEconomyProfileService: SafeEconomyProfileService,
                private paymentsService: PaymentsService,
                private paymentsModalService: PaymentsModalService,
                private movementsService: SafeEconomyMovementsService) {
    }
    ngOnInit() {
        this.paymentsService.getListValue((types) => {
            this.types = Object.keys(types).map((i) => types[i]).filter((item) => item.group === 'TP');
        });
        this.safeEconomyProfileService.setSafeEconomyUser((user) => {
            this.paymentsService.getAccountsByUser((accounts) => {
                this.accounts = Object.keys(accounts).map((i) => accounts[i]).filter((item) => item);
                this.movementsService.getMovementByUserAndActive((payments) => {
                    this.payments = Object.keys(payments).map((i) => payments[i]).filter((item) =>
                        item && item.kindOfMovement === 'Pago');
                    this.getFormattedDatePayment(this.payments);
                    this.paymentsService.setPayments(this.payments);
                    this.paymentsCopy = [...this.payments];
                    this.getCurrentPayments();
                }, user.id);
            }, user);
        });
    }
    getSelectByType(value) {
        if (value) {
            this.payments = this.paymentsCopy.filter((item) => item.description === value && item.kindOfMovement === 'Pago');
        } else {
            this.payments = [...this.paymentsCopy];
        }
    }
    mySearchClick() {
        if (this.priority) {
            this.payments = this.paymentsCopy.filter((item) => item.priority === this.priority && item.kindOfMovement === 'Pago');
        } else {
            this.payments = [...this.paymentsCopy];
        }
    }
    getBankById(id) {
        return this.accounts.filter((item) => item.id === id)[0];
    }
    getCurrentPayments() {
        this.paymentsService.currentList.subscribe((temp) => {
            this.payments = Object.keys(temp).map((i) => temp[i]);
            temp = this.payments.filter((item) => item);
            this.payments = temp;
            this.movementsForBarChartPay = this.payments.filter((item) => item.kindOfMovement === 'Pago');
            this.paymentsCopy = [...this.payments];
        });
    }
    createPayment() {
        this.modalRef = this.paymentsModalService.openPayment();
    }
    updatePayment(payment) {
        this.paymentsService.setPaymentToUpdate(payment);
        this.modalRef = this.paymentsModalService.openPayment();
    }
    deletePayment(payment) {
        this.paymentsService.setPaymentToUpdate(payment);
        this.modalRef = this.paymentsModalService.openMsj();
    }
    private getFormattedDatePayment(payments) {
            this.payments.forEach((x) => x.dateOfMovement = x.dateOfMovement.substr(8, 2));
            return payments;
    }
}
