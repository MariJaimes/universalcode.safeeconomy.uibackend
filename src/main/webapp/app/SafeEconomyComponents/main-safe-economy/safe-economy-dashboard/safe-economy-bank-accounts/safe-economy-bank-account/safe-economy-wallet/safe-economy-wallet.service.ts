import {Injectable} from '@angular/core';
import {SafeEconomyProfileService} from '../../../../auth/profile-safe-economy.service';
import {BankMovementPaymentService} from '../../../../../../entities/bank-movement-payment';
import {BankAccountService} from '../../../../../../entities/bank-account';

@Injectable()
export class SafeEconomyWalletService {

    wallet;

    constructor(private safeEconomyProfileService: SafeEconomyProfileService,
                private bankAccountService: BankAccountService,
                private bankMovementPaymentService: BankMovementPaymentService) {}

    getWallet(callback) {
        this.safeEconomyProfileService.setSafeEconomyUser((account) => {
            this.bankAccountService.getWallet(account).subscribe((res) => {
                this.wallet = res.body;
                callback(res.body);
            });
        });
    }
    obtainWallet() {
        return this.wallet;
    }
    removeWallet() {
        this.wallet = null;
    }

    getMovementByAccount(id, callback) {
        this.bankMovementPaymentService.findAllMovementByAccount(id).subscribe((res) => {
            callback(res.body);
        });
    }
}
