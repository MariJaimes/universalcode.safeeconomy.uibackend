import { Injectable } from '@angular/core';
import {ModalPaymentComponent} from './modal-payment/modal-payment.component'   ;
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ModalDelComponent} from './modal-payment/modal-del.component';

@Injectable()
export class PaymentsModalService {

    private isOpen = false;

    constructor(private modalService: NgbModal) { }

    openPayment(): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;
        const modalRef = this.modalService.open(ModalPaymentComponent);
        modalRef.result.then((result) => {
            this.isOpen = false;
            }, (reason) => {
            this.isOpen = false;
            });
        return modalRef;
    }

    openMsj(): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;
        const modalRef = this.modalService.open(ModalDelComponent);
        modalRef.result.then((result) => {
            this.isOpen = false;
            }, (reason) => {
            this.isOpen = false;
        });
        return modalRef;
    }
}
