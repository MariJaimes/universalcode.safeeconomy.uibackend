import { Route } from '@angular/router';
import {SafeEconomyBankAccountComponent} from './safe-economy-bank-account.component';

export const MonederoSafeEconomy: Route = {
    path: 'wallet',
    component: SafeEconomyBankAccountComponent
};
