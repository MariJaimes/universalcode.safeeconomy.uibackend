import {Route} from '@angular/router';
import {CalendarTestComponent} from './calendar-test.component';

export const CalendarSafeEconomy: Route = {
    path: 'calendar',
    component: CalendarTestComponent
};
