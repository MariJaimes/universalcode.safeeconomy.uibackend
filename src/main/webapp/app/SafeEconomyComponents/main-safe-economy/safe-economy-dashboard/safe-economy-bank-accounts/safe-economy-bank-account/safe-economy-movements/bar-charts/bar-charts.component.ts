import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import { Chart } from 'chart.js';
import {ChartService} from '../../../../../../safe-economy-utils/charts/chart.service';

@Component({
  selector: 'jhi-bar-charts',
  templateUrl: './bar-charts.component.html',
  styles: []
})
export class BarChartsComponent implements OnInit {
    @Input() array;
    @Input() type;
    @ViewChild('chart') pieChart: ElementRef;
    chart = [];

    constructor(private chartService: ChartService) {
    }

    ngOnInit() {
        this.buildBarCharts();
    }

    buildBarCharts() {
        let temp;
        let arrayCopy;
        temp = this.array.filter((item) => item.kindOfMovement === 'Gasto');
        temp ? this.array = temp : this.array = [];
        arrayCopy = this.array;
        if (this.array.length > 10) {
            arrayCopy = this.array.slice(0, 10);
        }
        const values = arrayCopy.map((item) => Math.abs(item.acreditationAmmount));
        const config = {
            type: 'bar',
            data: {
                labels: arrayCopy.map((item) => item.description),
                datasets: [{
                    label: this.type,
                    borderColor: this.chartService.COLOR_BAR_NEGATIVE[0],
                    backgroundColor: this.chartService.COLOR_BAR_NEGATIVE[0],
                    borderWith: 2,
                    data: values
                }]
            }
        };
        this.chart = new Chart(this.pieChart.nativeElement.getContext('2d'), config);
    }
}
