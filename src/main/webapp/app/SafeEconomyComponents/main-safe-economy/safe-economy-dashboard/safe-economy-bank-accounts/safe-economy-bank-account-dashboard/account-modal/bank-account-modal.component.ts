import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DetailsService} from '../bank-account-detail/details.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {BankingEntityService} from '../../../../../../entities/banking-entity';
import {BankAccount, BankAccountService} from '../../../../../../entities/bank-account';
import {SafeEconomyProfileService} from '../../../../auth/profile-safe-economy.service';
import {SafeEconomyMovementsService} from '../../safe-economy-bank-account/safe-economy-movements/movements.service';

@Component({
    selector: 'jhi-bank-account-modal',
    templateUrl: './bank-account-modal.component.html',
    styles: []
})
export class BankAccountModalComponent implements OnInit {

    addBankAccountForm: FormGroup;
    account;
    accountSelect;
    listBank;
    confirmAccount = false;
    currentUser;

    constructor(private detailsService: DetailsService,
                private close: NgbActiveModal,
                private bankService: BankingEntityService,
                private  bankAccoountService: BankAccountService,
                private  securityService: SafeEconomyProfileService,
                private movementsService: SafeEconomyMovementsService) {
    }

    ngOnInit() {
        this.securityService.setSafeEconomyUser((user) => {
            this.currentUser = user;
        });
        this.bankService.findAllBankEntitysByActive().subscribe((res) => {
            this.listBank = Object.keys(res.body).map((i) => res.body[i]).filter((item) => item && item.id !== 1);
            this.accountSelect = this.listBank[0];
            if (this.account) {
                this.addBankAccountForm = new FormGroup({
                    nameBankAccount: new FormControl(this.account.userName, Validators.required),
                    bankPass: new FormControl(this.account.password, Validators.required),
                    bankBudget: new FormControl(this.account.budget, Validators.required),
                    bankDesc: new FormControl(this.account.bank, Validators.required)
                });
                this.accountSelect = this.listBank.filter((item) => item.id === this.account.bankEntityId)[0];
                const pos1 = this.listBank.indexOf(this.accountSelect) + 1;
                this.listBank.unshift(this.accountSelect);
                this.listBank.splice(pos1, 1);
            }
        });
            this.account = this.detailsService.getAccount();
            this.addBankAccountForm = new FormGroup({
                nameBankAccount: new FormControl('', Validators.required),
                bankPass: new FormControl('', Validators.required),
                bankBudget: new FormControl('', Validators.required),
                bankDesc: new FormControl('', Validators.required)
            });
    }

    createBankAccount() {
        const bankAccount = new BankAccount(
             null,
            this.addBankAccountForm.value['nameBankAccount'],
            this.addBankAccountForm.value['bankPass'],
            null,
            null,
            this.addBankAccountForm.value['bankBudget'],
             this.addBankAccountForm.value['bankDesc'],
            true,
            null,
            this.currentUser.id,
            null,
            null,
            this.accountSelect.id);
        this.bankAccoountService.create(bankAccount).subscribe((x) => {
            this.timeAndMsj();
            this.detailsService.addAccount(x.body);
        });
    }

    getSelectBank(value) {
        this.accountSelect = this.listBank.filter((item) => item.id === parseInt(value, 10))[0];
    }

    updateBankAccount() {
        this.account.userName = this.addBankAccountForm.value['nameBankAccount'];
        this.account.password = this.addBankAccountForm.value['bankPass'];
        this.account.budget = this.addBankAccountForm.value['bankBudget'];
        this.account.bank = this.addBankAccountForm.value['bankDesc'];
        this.account.bankEntityId = this.accountSelect.id;
        this.account.active = true;
        this.bankAccoountService.update(this.account).subscribe(() => this.timeAndMsj());
    }

    closeModalDetails() {
        this.close.dismiss('close');
        this.account = this.detailsService.setAccountToUpdate(null);
    }

    private timeAndMsj() {
        this.clean();
        this.confirmAccount = true;
        setTimeout(() => {
            this.closeModalDetails();
        }, 2000);
    }

    clean() {
        this.addBankAccountForm.reset();
    }
}
