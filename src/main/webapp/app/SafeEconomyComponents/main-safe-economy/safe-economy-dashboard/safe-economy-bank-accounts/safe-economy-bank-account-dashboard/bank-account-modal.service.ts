import {Injectable} from '@angular/core';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {BankAccountModalComponent} from './account-modal/bank-account-modal.component';
import {DelAccountComponent} from './modal-delete/delAccount.component';

@Injectable()
export class BankAccountModalService {

    private isOpen = false;

    constructor(private modalService: NgbModal) {
    }

    openAccount(): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;
        const modalRef = this.modalService.open(BankAccountModalComponent);
        modalRef.result.then((result) => {
            this.isOpen = false;
        }, (reason) => {
            this.isOpen = false;
        });
        return modalRef;
    }

    openAccountDelete(): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;
        const modalRef = this.modalService.open(DelAccountComponent);
        modalRef.result.then((result) => {
            this.isOpen = false;
        }, (reason) => {
            this.isOpen = false;
        });
        return modalRef;
    }
}
