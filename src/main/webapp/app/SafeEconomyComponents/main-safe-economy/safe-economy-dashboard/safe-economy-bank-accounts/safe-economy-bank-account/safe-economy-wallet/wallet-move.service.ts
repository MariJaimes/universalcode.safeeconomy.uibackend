import {Injectable} from '@angular/core';
import {BankMovementPaymentService} from '../../../../../../entities/bank-movement-payment';
import {SafeEconomyWalletService} from './safe-economy-wallet.service';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class WalletMoveService {
    account;
    movements;
    private table = new BehaviorSubject(null);
    currentTable = this.table.asObservable();
    constructor(private bankMovementPaymentService: BankMovementPaymentService,
                private safeEconomyWalletService: SafeEconomyWalletService) {
    }
    addMovement(movement) {
        this.table.value.push(movement);
        this.table.next(this.table.value);
    }

    removeMovement(movement) {
        const before = this.table.value.slice(this.table.value.indexOf(movement) + 1, this.table.value.length);
        const after = this.table.value.slice(0, this.table.value.indexOf(movement));
        const a = before.concat(after);
        this.table.next(a);
    }

    setMovements(movements) {
        this.table.next([...movements]);
    }

    deleteMovement(id, callback) {
        this.bankMovementPaymentService.delete(id).subscribe((res) => {
            callback(res);
        });
    }

    updateMovement(movement, callback) {
        this.bankMovementPaymentService.update(movement).subscribe((res) => {
            console.log(res.body);
            callback(res.body);
        });
    }
    deleteMovementById(movement: any) {
        this.table.next(this.table.getValue().filter(function(obj) {
            return obj.id !== movement.id;
        }));
    }
}
