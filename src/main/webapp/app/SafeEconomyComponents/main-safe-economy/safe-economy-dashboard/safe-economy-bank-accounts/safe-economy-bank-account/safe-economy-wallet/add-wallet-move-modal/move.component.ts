import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {MoveService} from './move.service';
import {SafeEconomyProfileService} from '../../../../../auth/profile-safe-economy.service';
import {SafeEconomyWalletService} from '../safe-economy-wallet.service';
import {WalletMoveService} from '../wallet-move.service';
import {BankMovementPayment} from '../../../../../../../entities/bank-movement-payment';

@Component({
    selector: 'jhi-safe-economy-add-movement-modal',
    templateUrl: './move.component.html',
    styleUrls: ['./movement.css'],
})
export class MoveComponent implements OnInit {
    confirm = false;
    addMovementForm: FormGroup;
    movement;
    showMsj = false;
    today;
    selectMovement;
    kindsMovemnts;

    constructor(private close: NgbActiveModal,
                private safeEconomyAddMovementService: MoveService,
                private safeEconomyProfileService: SafeEconomyProfileService,
                private walletService: SafeEconomyWalletService,
                private walletMoveService: WalletMoveService) { }

    ngOnInit() {
        this.parseDate();
        this.movement = this.safeEconomyAddMovementService.getMovement();
        this.kindsMovemnts = [];
        this.kindsMovemnts[0] = 'Gasto';
        this.kindsMovemnts[1] = 'Ingreso';
        this.selectMovement = this.kindsMovemnts[0];
        if (this.movement) {
            this.parseInvertDate(this.movement.dateOfMovement);
            this.addMovementForm = new FormGroup({
                descMove: new FormControl(this.movement.description, Validators.required),
                dateMove: new FormControl(this.today, Validators.required),
                amountMove: new FormControl(this.movement.acreditationAmmount, Validators.required),
                commentMove: new FormControl(this.movement.comment)
            });
            if (this.movement.kindOfMovement === 'Ingreso') {
                this.kindsMovemnts[0] = this.movement.kindOfMovement;
                this.kindsMovemnts[1] = 'Gasto';
                this.selectMovement = this.kindsMovemnts[0];
            }
        } else {
            this.initForm();
        }
    }

    parseDate() {
        this.today = new Date();
        const dd = this.today.getDate();
        const mm = this.today.getMonth() + 1;
        const yyyy = this.today.getFullYear();
        this.today = yyyy + '-' + mm + '-' +  dd;
        if (mm < 10) {
            if (dd < 10) {
                this.today = yyyy + '-' + 0 + mm + '-' + 0 + dd;
            } else {
                this.today = yyyy + '-' + 0 + mm + '-' + dd;
            }
        } else {
            this.today = yyyy + '-' + mm + '-' + 0 + dd;
        }
        return this.today;
    }
    parseInvertDate(date) {
        this.today = new Date(date);
        const dd = this.today.getDate();
        const mm = this.today.getMonth() + 1;
        const yyyy = this.today.getFullYear();
        this.today = yyyy + '-' + mm + '-' + dd;
        if (mm < 10) {
            if (dd < 10) {
                this.today = yyyy + '-' + 0 + mm + '-' + 0 + dd;
            } else {
                this.today = yyyy + '-' + 0 + mm + '-' + dd;
            }
        } else {
            this.today = yyyy + '-' + mm + '-' + 0 + dd;
        }
        return this.today;
    }
    onSubmit() {
        if (this.validateDate()) {
            this.showMsj = false;
            this.safeEconomyProfileService.setSafeEconomyUser((user) => {
                this.walletService.getWallet((wallet) => {
                    let  value = this.addMovementForm.value['amountMove'];
                    if (this.selectMovement === 'Gasto') {
                        value = '-' + value;
                    } if (!this.addMovementForm.value['dateMove']) {
                        this.addMovementForm.value['dateMove'] = this.today;
                    }
                    const move = new BankMovementPayment(
                        null,
                        this.selectMovement,
                        this.addMovementForm.value['dateMove'],
                        this.addMovementForm.value['descMove'],
                         value,
                        null,
                        this.addMovementForm.value['commentMove'],
                        null,
                        user.id,
                        wallet.id);
                    this.safeEconomyAddMovementService.create(move).subscribe((res) => {
                        move.id = res.body.id;
                        const movet = this.getFormattedDat(move);
                        this.walletMoveService.addMovement(movet);
                        this.timeAndMsj();
                    });
                });
            });
        } else {
            this.showMsj = true;
        }
    }
    getFormattedDat(move) {
        move.dateOfMovement =  new Date(move.dateOfMovement);
        move.dateOfMovement = (move.dateOfMovement.getDate() + 1) + '/' + 0 + (move.dateOfMovement.getMonth() + 1) + '/' + move.dateOfMovement.getFullYear();
        return move;
    }
    validateDate() {
        if (!this.addMovementForm.value['dateMove']) {
            this.addMovementForm.value['dateMove'] = this.today;
        }
        return this.today >= this.addMovementForm.value['dateMove'];
    }
    closeMovement() {
        this.close.dismiss('close');
        this.movement = this.safeEconomyAddMovementService.setMovementToUpdate(null);
    }

    initForm() {
        this.addMovementForm = new FormGroup({
            descMove: new FormControl('', Validators.required),
            dateMove: new FormControl(''),
            amountMove: new FormControl('', Validators.required),
            commentMove: new FormControl(''),
        });
    }

    update() {
        if (this.validateDate()) {
            let  value = this.addMovementForm.value['amountMove'];
            if (this.selectMovement === 'Gasto') {
                value =  -Math.abs(value);
            }else {
                value = Math.abs(value);
            }
            this.showMsj = false;
            const newDate = this.getFormattedDate(this.movement);
            this.movement.dateOfMovement = newDate;
            this.movement.kindOfMovement = this.selectMovement;
            this.movement.description = this.addMovementForm.value['descMove'];
            this.movement.dateOfMovement = this.addMovementForm.value['dateMove'];
            this.movement.acreditationAmmount = this.addMovementForm.value['amountMove'];
            this.movement.acreditationAmmount = value;
            this.movement.comment = this.addMovementForm.value['commentMove'];
            this.walletMoveService.updateMovement(this.movement, (movement) => {
                const movet = this.getFormattedDat(movement);
                this.walletMoveService.deleteMovementById(movement);
                this.walletMoveService.addMovement(movet);
                this.timeAndMsj();
            });
        } else {
            this.showMsj = true;
        }
    }
    getFormattedDate(date) {
        if (date.dateOfMovement) {
            const dateString = date.dateOfMovement.substr(0, 10);
            return dateString;
        }
        return null;
    }
    private timeAndMsj() {
        this.clean();
        this.confirm = true;
        setTimeout(() => {
            this.closeMovement();
        }, 2000);
    }
    clean() {
        this.addMovementForm.reset();
    }
    getSelectTypeSelectMovement(typeSelectMovement) {
        this.selectMovement = typeSelectMovement;
    }
}
