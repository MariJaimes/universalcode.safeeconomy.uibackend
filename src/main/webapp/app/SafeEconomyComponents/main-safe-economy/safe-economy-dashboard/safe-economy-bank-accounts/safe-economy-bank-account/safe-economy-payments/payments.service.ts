import { Injectable } from '@angular/core';
import {BankMovementPaymentService} from '../../../../../../entities/bank-movement-payment';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {BankAccountService} from '../../../../../../entities/bank-account';
import {KeyValueService} from '../../../../../../entities/key-value';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Injectable()
export class PaymentsService {
    payment;
    payments;
    paymentsCopy;
    private list = new BehaviorSubject(null);
    currentList = this.list.asObservable();

    constructor(private bankMovementPaymentService: BankMovementPaymentService,
                private keyValueService: KeyValueService,
                private bankAccountService: BankAccountService) { }

    updatePayment(payment, callback) {
        this.bankMovementPaymentService.update(payment).subscribe((res) => {
            callback(res.body);
        });
    }

    setPayments(payments) {
        this.list.next([...payments]);
    }

    getListValue(callback) {
        this.keyValueService.findAllTypes().subscribe((res) => {
            callback(res.body);
        });
    }

    addPayment(payment) {
        this.list.value.push(payment);
        this.list.next(this.list.value);
    }

    getAccountsByUser(callback, account) {
        this.bankAccountService.findAllAccountsByUser(account.id).subscribe((res) => {
            callback(res.body);
        });
    }

    deletePaymentById(payment: any) {
        this.list.next(this.list.getValue().filter(function(obj) {
            return obj.id !== payment.id;
        }));
    }

    removePayment(payment) {
        const before = this.list.value.slice(this.list.value.indexOf(payment) + 1, this.list.value.length);
        const after = this.list.value.slice(0, this.list.value.indexOf(payment));
        const a = before.concat(after);
        this.list.next(a);
    }

    deletePayment(id, callback) {
        this.bankMovementPaymentService.delete(id).subscribe((res) => {
            callback(res);
        });
    }

    setPaymentToUpdate( payment ) {
        this.payment = payment;
    }

    getPayment() {
        return this.payment;
    }

    getMovementByUser(id, callback) {
        this.bankMovementPaymentService.findAllMovesByUser(id).subscribe((res) => {
            callback(res.body);
        });
    }
    // -------------------------------------------------------------CALENDAR----------------------------------------------
    getEvents(): Observable<any> {
        this.currentList.subscribe((temp) => {
            this.paymentsCopy = Object.keys(temp).map((i) => temp[i]);
            temp = this.paymentsCopy.filter((item) => item);
            this.paymentsCopy = temp;
        const dateObj = new Date();
        const yearMonth = dateObj.getUTCFullYear() + '-' + (dateObj.getUTCMonth() + 1);
            this.paymentsCopy.forEach((x) =>
           this.paymentsCopy = [{
                title: x.comment,
                start: yearMonth + x.dateOfMovement.getDay()
            }]
        );
        });
        return Observable.of(this.paymentsCopy);
    }
}
