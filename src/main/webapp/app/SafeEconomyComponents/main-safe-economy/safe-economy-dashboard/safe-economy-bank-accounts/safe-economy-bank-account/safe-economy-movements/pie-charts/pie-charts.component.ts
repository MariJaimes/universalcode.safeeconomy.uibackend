import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import { Chart } from 'chart.js';
import {ChartService} from '../../../../../../safe-economy-utils/charts/chart.service';

@Component({
  selector: 'jhi-pie-charts',
  templateUrl: './pie-charts.component.html',
  styles: []
})
export class PieChartsComponent implements OnInit {
    @Input() array;
    @Input() type;
    @ViewChild('chart') pieChart: ElementRef;
    chart = [];
    config;

    constructor(private chartsService: ChartService) { }

    ngOnInit() {
        this.buildPieChart();
    }

    buildPieChart() {
        this.config = null;
        let temp;
        let arrayCopy;
        temp = this.array.filter((item) => item.kindOfMovement === 'Ingreso');
        temp ? this.array = temp : this.array = [];
        arrayCopy = this.array;
            if (this.array.length > 10) {
                arrayCopy = this.array.slice(0, 10);
            }
            if (this.array.length === 0) {
                this.config = this.arrayOutDataMove();
            }else {
                this.config = this.arrayWithDataMove(arrayCopy);
            }
            this.chart = new Chart(this.pieChart.nativeElement.getContext('2d'), this.config);
    }
    arrayOutDataMove() {
        const values = this.chartsService.getTestingValue(1);
        const colors = this.chartsService.getColor(values.length);
        return this.config = {
            type: 'pie',
            data: {
                datasets: [{
                    data: values,
                    backgroundColor: colors,
                    label: 'Dataset 1'
                }],
                labels: [
                    'Ingresos',
                ]
            },
            options: {
                responsive: true
            }
        };
    }
    arrayWithDataMove(arrayCopy) {
        const values = arrayCopy.map((item) => item.acreditationAmmount);
        const colors = this.chartsService.getColor(values.length);
        return this.config = {
            type: 'pie',
            data: {
                datasets: [{
                    data: values,
                    backgroundColor: colors,
                    label: arrayCopy.map((item) => item.description)
                }],
                labels: arrayCopy.map((item) => item.description)
            },
            options: {
                responsive: true
            }
        };
    }
}
