import { Route } from '@angular/router';
import {SafeEconomyBankAccountDashboardComponent} from './safe-economy-bank-account-dashboard.component';

export const BankAccountDashboard: Route = {
    path: 'bankdashboard',
    component: SafeEconomyBankAccountDashboardComponent
};
