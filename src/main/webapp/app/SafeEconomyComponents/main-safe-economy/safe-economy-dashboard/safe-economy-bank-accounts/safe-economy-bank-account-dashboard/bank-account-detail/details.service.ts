import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {BankAccountService} from '../../../../../../entities/bank-account';

@Injectable()
export class DetailsService {
    account;
    accounts;
    private listAccount = new BehaviorSubject(null);
    currentAccount = this.listAccount.asObservable();

    constructor(private bankAccountService: BankAccountService) {
    }

    setListOfAccount(listOfAcounts) {
        this.listAccount.next([...listOfAcounts]);
    }

    setAccountToUpdate(account) {
        this.account = account;
    }

    getAccount() {
        return this.account;
    }

    removeAccount(account) {
        const before = this.listAccount.value.slice(this.listAccount.value.indexOf(account) + 1, this.listAccount.value.length);
        const after = this.listAccount.value.slice(0, this.listAccount.value.indexOf(account));
        const a = before.concat(after);
        this.listAccount.next(a);
    }

    deleteAccount(account, callback) {
        account.active = false;
        this.bankAccountService.update(account).subscribe((res) => {
            callback(res);
        });
    }

    addAccount(account) {
        this.listAccount.value.push(account);
    }
}
