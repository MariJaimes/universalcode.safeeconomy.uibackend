import {Component, OnInit} from '@angular/core';
import {SafeEconomyProfileService} from '../../../auth/profile-safe-economy.service';
import {BankAccountService} from '../../../../../entities/bank-account';
import {BankingEntityService} from '../../../../../entities/banking-entity';
import {BankAccountModalService} from './bank-account-modal.service';
import {DetailsService} from './bank-account-detail/details.service';
import {SafeEconomyMovementsService} from '../safe-economy-bank-account/safe-economy-movements/movements.service';

@Component({
    selector: 'jhi-safe-economy-bank-account-dashboard',
    templateUrl: './safe-economy-bank-account-dashboard.component.html',
    styleUrls: ['./bank-accounts-detail-styles.css']
})
export class SafeEconomyBankAccountDashboardComponent implements OnInit {

    listOfBankAccounts;
    listOfBanks;
    currentUser;
    ReadyToShow = false;

    constructor(private securityService: SafeEconomyProfileService,
                private bankAccuntService: BankAccountService,
                private bankService: BankingEntityService,
                private bankAccountModalService: BankAccountModalService,
                private detailService: DetailsService,
                private movementsService: SafeEconomyMovementsService) {
    }
    ngOnInit() {
        this.listOfBankAccounts = [];
        this.securityService.setSafeEconomyUser((user) => {
            this.currentUser = user;
            this.movementsService.getAccountsByUserAndActive((accounts) => {
                const array = Object.keys(accounts).map((i) => accounts[i]).filter((item) => item.bankEntityId !== 1);
                this.detailService.setListOfAccount(array);
                this.ReadyToShow = true;
            }, user);
            this.bankService.findAllEntity().subscribe((res) => {
                const result = Object.keys(res.body).map((i) => res.body[i]).filter((item) => item);
                const arrayToObject = (array) =>
                    array.reduce((obj, item) => {
                        obj[item.id] = item;
                        return obj;
                    }, {});
                this.listOfBanks = arrayToObject(result);

            });
        });
        this.detailService.currentAccount.subscribe((x) => {
            this.listOfBankAccounts = x;
        });
    }
    getLogoIdBankByAccount(bankAccount) {
        return this.listOfBanks ? this.listOfBanks[bankAccount.bankEntityId] : {logo: 'http://www.asba-supervision.org/PEF/images/ahorro/banco.jpg'};
    }
    createBankAccount() {
        this.bankAccountModalService.openAccount();
    }
}
