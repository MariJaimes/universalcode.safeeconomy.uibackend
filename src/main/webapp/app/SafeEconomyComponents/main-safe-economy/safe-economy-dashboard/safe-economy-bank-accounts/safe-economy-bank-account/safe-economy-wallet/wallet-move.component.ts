import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {SafeEconomyAddMovementModalService} from './safe-economy-add-movement-modal.service';
import {SafeEconomyWalletService} from './safe-economy-wallet.service';
import {WalletMoveService} from './wallet-move.service';
import {MoveService} from './add-wallet-move-modal/move.service';
import {SafeEconomyMovementsService} from '../safe-economy-movements/movements.service';
import {SafeEconomyProfileService} from '../../../../auth/profile-safe-economy.service';

@Component({
    selector: 'jhi-safe-economy-wallet',
    templateUrl: './wallet-move.component.html',
    styleUrls: ['./wallet-move.component.css'],
})
export class WalletMoveComponent implements OnInit {
    bank;
    modalRef: NgbModalRef;
    addTableForm: FormGroup;
    movements = null;
    today;
    user;
    allAccounts;
    lastMonth;
    movementsCopy;
    movementsForBarChartExpensive;
    movementsForBarChartIncome;
    constructor(private safeEconomyAddMovementModalService: SafeEconomyAddMovementModalService,
                private safeEconomyWalletService: SafeEconomyWalletService,
                private walletMoveService: WalletMoveService,
                private movementService: MoveService,
                private movementsService: SafeEconomyMovementsService,
                private safeEconomyProfileService: SafeEconomyProfileService) {
    }

    ngOnInit() {
        this.parseDate();
        this.parseLastMonth();
        this.initForm();
        this.safeEconomyProfileService.setSafeEconomyUser((user) => {
            this.user = user;
            this.movementsService.getAccountsByUserAndActive((accounts) => {
                this.allAccounts = Object.keys(accounts).map((i) => accounts[i]).filter((item) => item);
            }, user);
            // this.safeEconomyWalletService.getMovementByAccount(bank.id, (movements) => {
            //     this.movements = Object.keys(movements).map((i) => movements[i]).filter((item) => item && item.kindOfMovement === 'Gasto' || item.kindOfMovement === 'Ingreso');
            this.movementsService.getMovementByUserAndActive((allMovements) => {
                debugger;
                this.movements = Object.keys(allMovements).map((i) => allMovements[i]).filter((item) => item);
                this.getFormattedDate(this.movements);
                this.movements = this.orderByDate(this.movements);
                this.getFormattedDescriptionMovements(this.movements);
                this.walletMoveService.setMovements(this.movements);
                this.movementsForBarChartExpensive = this.movements.filter((item) => item.kindOfMovement === 'Gasto');
                this.movementsForBarChartIncome = this.movements.filter((item) => item.kindOfMovement === 'Ingreso');
                // const lastindex = this.movementsForBarChart.length - 1;
                // if (this.movementsForBarChart.length > 12) {
                //     this.movementsForBarChart = this.movementsForBarChart.slice(lastindex - 10, lastindex);
                //  }
                this.movementsCopy = [...this.movements];
                this.getCurrentTable();
            }, user.id);
        });
    }
    parseDate() {
        this.today = new Date();
        const dd = this.today.getDate();
        const mm = this.today.getMonth() + 1;
        const yyyy = this.today.getFullYear();
        this.today = yyyy + '-' + mm + '-' + dd;
        if (mm < 10) {
            if (dd < 10) {
                this.today = yyyy + '-' + 0 + mm + '-' + 0 + dd;
            } else {
                this.today = yyyy + '-' + 0 + mm + '-' + dd;
            }
        } else {
            this.today = yyyy + '-' + mm + '-' + 0 + dd;
        }
        return this.today;
    }
    parseLastMonth() {
        this.lastMonth = new Date();
        const dd = this.lastMonth.getDate();
        const mm = this.lastMonth.getMonth();
        const yyyy = this.lastMonth.getFullYear();
        this.lastMonth = yyyy + '-' + mm + '-' + dd;
        if (mm < 10) {
            if (dd < 10) {
                this.lastMonth = yyyy + '-' + 0 + mm + '-' + 0 + dd;
            } else {
                this.lastMonth = yyyy + '-' + 0 + mm + '-' + dd;
            }
        } else {
            this.lastMonth = yyyy + '-' + mm + '-' + 0 + dd;
        }
        return this.lastMonth;
    }
    private getFormattedDescriptionMovements(allMovements) {
        this.movements.forEach((x) => x.description = x.description.split(':')[1]);
        return allMovements;
    }
    orderByDate(arr) {
        return arr.slice().sort(function(a, b) {
            return a.dateOfMovement > b.dateOfMovement ? -1 : 1;
        });
    }
    getCurrentTable() {
        this.walletMoveService.currentTable.subscribe((temp) => {
            this.movements = Object.keys(temp).map( (i) => temp[i]);
            // this.getFormattedDate(temp);
            temp = this.movements.filter((item) => item);
            this.movements = temp;
            this.movementsForBarChartExpensive = this.movements.filter((item) => item.kindOfMovement === 'Gasto');
            this.movementsForBarChartIncome = this.movements.filter((item) => item.kindOfMovement === 'Ingreso');
            // const lastindex = this.movementsForBarChart.length - 1;
            // if (this.movementsForBarChart.length > 12) {
            //     this.movementsForBarChart = this.movementsForBarChart.slice(lastindex - 10, lastindex);
            // }
            this.movementsCopy = [...this.movements];
        });
    }
    getFormattedDate(movements) {
        this.movements.forEach((x) => x.dateOfMovement = x.dateOfMovement.substr(0, 10));
        this.movements.forEach((x) => x.dateOfMovement =  new Date(x.dateOfMovement));
        this.movements.forEach((x) => x.dateOfMovement = ( x.dateOfMovement.getMonth() + 1) + '/' + ( x.dateOfMovement.getDate() + 1) + '/' + x.dateOfMovement.getFullYear());
        return movements;
    }

    getSelect(value) {
        if ((value === 'Gastos') || (value === 'Ingresos')) {
            if (value === 'Gastos') {
                this.movements = this.movementsCopy.filter( (item) => item.kindOfMovement === 'Gasto');
            } else if (value === 'Ingresos') {
                this.movements = this.movementsCopy.filter( (item) => item.kindOfMovement === 'Ingreso');
            }
        }else if ( value === 'Sin filtro' ) {
            this.movements = [ ...this.movementsCopy ];
        }
    }

    getFilterByFecha(value) {
        this.getSelect(value);
        if (!this.addTableForm.value['dateLast'] && !this.addTableForm.value['dateStart']) {
            this.addTableForm.value['dateStart'] = this.lastMonth;
            this.addTableForm.value['dateLast'] = this.today;
        }
        if (this.addTableForm.value['dateLast']) {
            if (this.addTableForm.value['dateStart']) {
                this.movements = this.movementsCopy.filter( (item) =>
                    item.dateOfMovement >= this.addTableForm.value['dateStart'] && item.dateOfMovement <= this.addTableForm.value['dateLast']);
            } else {
                this.movements = this.movementsCopy.filter( (item) =>
                    item.dateOfMovement <= this.addTableForm.value['dateLast']);
            }
        } else {
            if (this.addTableForm.value['dateStart']) {
                this.movements = this.movementsCopy.filter((item) =>
                    item.dateOfMovement >= this.addTableForm.value['dateStart']);
            } else {
                this.movements = [ ...this.movementsCopy ];
            }
        }
    }
    getBankByIdMovements(id) {
        return this.allAccounts.filter((item) => item.id === id)[0].bank;
    }
    createMovement() {
        this.modalRef = this.safeEconomyAddMovementModalService.open();
    }

    updateMovement(movement) {
        this.movementService.setMovementToUpdate(movement);
        this.modalRef = this.safeEconomyAddMovementModalService.open();
    }

    deleteMovement(movement) {
        this.movementService.setMovementToUpdate(movement);
        this.modalRef = this.safeEconomyAddMovementModalService.openMsj();
    }

    private initForm() {
        this.addTableForm = new FormGroup({
            dateLast: new FormControl(''),
            dateStart: new FormControl(''),
        });
    }

}
