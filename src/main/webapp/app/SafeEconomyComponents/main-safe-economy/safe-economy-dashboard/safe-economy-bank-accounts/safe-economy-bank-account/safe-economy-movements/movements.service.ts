import {Injectable} from '@angular/core';
import {BankAccountService} from '../../../../../../entities/bank-account';
import {BankMovementPaymentService} from '../../../../../../entities/bank-movement-payment';

@Injectable()
export class SafeEconomyMovementsService {

    constructor(private bankAccountService: BankAccountService,
                private bankMovementPaymentService: BankMovementPaymentService) {}

    getAccountsByUserAndActive(callback, user) {
        this.bankAccountService.findAllAccountsByUserAndActive(user.id).subscribe((res) => {
            callback(res.body);
        });
    }

    getAccountsByUserActive(user) {
        this.bankAccountService.findAllAccountsByUserAndActive(user.id).subscribe((res) => {
        });
    }
    getMovementByUserAndActive(callback, id) {
        this.bankMovementPaymentService.findAllMovesByUserAndActive(id).subscribe((res) => {
            callback(res.body);
        });
    }
}
