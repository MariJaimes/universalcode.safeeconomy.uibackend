import {Route} from '@angular/router';
import {SafeEconomyMovementsComponent} from './safe-economy-movements.component';

export const MovementsSafeEconomy: Route = {
    path: 'movements',
    component: SafeEconomyMovementsComponent
};
