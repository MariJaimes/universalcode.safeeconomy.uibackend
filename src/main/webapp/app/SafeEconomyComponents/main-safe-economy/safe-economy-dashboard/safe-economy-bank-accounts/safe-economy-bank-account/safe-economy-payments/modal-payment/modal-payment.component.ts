import { Component, OnInit } from '@angular/core';
import {PaymentsService} from '../payments.service';
import {SafeEconomyProfileService} from '../../../../../auth/profile-safe-economy.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {BankMovementPayment} from '../../../../../../../entities/bank-movement-payment';
import {MoveService} from '../../safe-economy-wallet/add-wallet-move-modal/move.service';
import {SafeEconomyMovementsService} from '../../safe-economy-movements/movements.service';

@Component({
  selector: 'jhi-modal-payment',
  templateUrl: './modal-pay.component.html',
  styles: []
})
export class ModalPaymentComponent implements OnInit {

    confirmPayment = false;
    payments = null;
    month;
    day;
    today;
    accounts;
    types;
    user;
    bank;
    accountSelect;
    typeSelect;
    payment;
    showMsjPayment = false;
    addPaymentsForm: FormGroup;

    constructor(private paymentsService: PaymentsService,
                private safeEconomyAddMovementService: MoveService,
                private safeEconomyProfileService: SafeEconomyProfileService,
                private close: NgbActiveModal,
                private movementsService: SafeEconomyMovementsService,
    ) {
    }

    ngOnInit() {
        this.parseDate();
        this.payment = this.paymentsService.getPayment();
        if (this.payment) {
            this.paymentsService.getListValue((types) => {
                this.types = Object.keys(types).map((i) => types[i]).filter((item) => item.group === 'TP');
                this.typeSelect = this.types.filter((item) => item.description === this.payment.description)[0];
                const pos1 = this.types.indexOf(this.typeSelect) + 1;
                this.types.unshift(this.typeSelect);
                this.types.splice(pos1, 1);
                this.typeSelect = this.typeSelect.description;
            });
            this.safeEconomyProfileService.setSafeEconomyUser((user) => {
                this.user = user;
                this.movementsService.getAccountsByUserAndActive((accounts) => {
                    this.accounts = Object.keys(accounts).map((i) => accounts[i]).filter((item) => item);
                    this.accountSelect = this.accounts.filter((item) => item.id === this.payment.bankAccountId)[0];
                    const pos = this.accounts.indexOf(this.accountSelect) + 1;
                    this.accounts.unshift(this.accountSelect);
                    this.accounts.splice(pos, 1);
                }, user);
            });
            this.addPaymentsForm = new FormGroup({
                namePayment: new FormControl(this.payment.comment, Validators.required),
                date: new FormControl(this.payment.dateOfMovement, Validators.required),
                amount: new FormControl(this.payment.acreditationAmmount, Validators.required),
                priority: new FormControl(this.payment.priority, Validators.required),
            });

        } else {
            this.initForm();
            this.paymentsService.getListValue((types) => {
                this.types = Object.keys(types).map((i) => types[i]).filter((item) => item.group === 'TP');
                this.typeSelect = this.types[0];
                this.typeSelect = this.typeSelect.description;
            });
            this.safeEconomyProfileService.setSafeEconomyUser((user) => {
                this.user = user;
                this.movementsService.getAccountsByUserAndActive((accounts) => {
                    this.accounts = Object.keys(accounts).map((i) => accounts[i]).filter((item) => item);
                    this.accountSelect = this.accounts[0];
                }, user);
            });
        }
    }

    parseDate() {
        this.today = new Date();
        const dd = this.today.getDate();
        this.day = this.today.getDate();
        const mm = this.today.getMonth() + 1;
        this.month = this.today.getMonth() + 1;
        const yyyy = this.today.getFullYear();
        return this.today = yyyy + '-' + 0 + mm + '-' + dd;
    }

    closeModal() {
        this.close.dismiss('close');
        this.payment = this.paymentsService.setPaymentToUpdate(null);
    }

    getSelectType(value) {
        this.typeSelect = value;
    }

    getSelectAccount(value) {
        const bank = this.accounts.filter((item) => item.bank === value)[0];
        this.accountSelect = bank;
    }

    onSubmitPayment() {
        if (this.validateDatePayment()) {
            this.showMsjPayment = false;
            const temp = '2018-07-' + this.addPaymentsForm.value['date'];
            const value = this.addPaymentsForm.value['amount'];
            const pay = new BankMovementPayment(
                null,
                'Pago',
                temp,
                this.typeSelect,
                value,
                null,
                this.addPaymentsForm.value['namePayment'],
                this.addPaymentsForm.value['priority'],
                this.user.id,
                this.accountSelect.id);

            this.safeEconomyAddMovementService.create(pay).subscribe((res) => {
                pay.id = res.body.id;
                const dateNewPayment = pay.dateOfMovement.substr(8, 2);
                pay.dateOfMovement = dateNewPayment;
                this.paymentsService.addPayment(pay);
                this.timeAndMsj();
            });
        }else {
            this.showMsjPayment = true;
        }
        }

    validateDatePayment() {
        return  31 >= this.addPaymentsForm.value['date'];
    }

    updatePayment() {
        const temp = '2018-07-' + this.addPaymentsForm.value['date'];
            this.showMsjPayment = false;
            this.payment.comment = this.addPaymentsForm.value['namePayment'];
            this.payment.dateOfMovement = temp;
            this.payment.description = this.typeSelect;
            this.payment.bankAccountId = this.accountSelect.id;
            this.payment.acreditationAmmount = this.addPaymentsForm.value['amount'];
            this.payment.priority = this.addPaymentsForm.value['priority'];
            this.paymentsService.updatePayment(this.payment, (payment) => {
                this.paymentsService.deletePaymentById(payment);
                const dateNew = payment.dateOfMovement.substr(8, 2);
                payment.dateOfMovement = dateNew;
                this.paymentsService.addPayment(payment);
                this.timeAndMsj();
            });
    }

    private timeAndMsj() {
        this.clean();
        this.confirmPayment = true;
        setTimeout(() => {
            this.closeModal();
        }, 2000);
    }

    private clean() {
        this.addPaymentsForm.reset();
    }

    private initForm() {
        this.addPaymentsForm = new FormGroup({
            namePayment: new FormControl('', Validators.required),
            date: new FormControl('', Validators.required),
            amount: new FormControl('', Validators.required),
            priority: new FormControl('', Validators.required),
        });
    }
}
