import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BankingEntityService} from '../../../../../entities/banking-entity/banking-entity.service';
import {JhiEventManager} from 'ng-jhipster';
import {RegisterBankEntityService} from './register-bank-entity.service';
import {SafeEconomyPreferencesService} from '../preferences.service';
import {BankingEntity} from '../../../../../entities/banking-entity/banking-entity.model';

@Component({
  selector: 'jhi-safe-economy-register-bank-entity',
  templateUrl: './safe-economy-register-bank-entity.component.html',
  styles: []
})
export class SafeEconomyRegisterBankEntityComponent implements OnInit {

    @Input() closeFunction;
    formBankEntity: FormGroup;
    bankEntity;
    confirm = false;
    sucess;

    constructor(private bankEntityService: BankingEntityService,  private eventManager: JhiEventManager,
              private registerBankEntity: RegisterBankEntityService,
              private  safeEconomyPreferencesService: SafeEconomyPreferencesService) { }

    ngOnInit() {
        this.sucess = false;
        this.bankEntity = this.registerBankEntity.getBankEntity();
        if (this.bankEntity) {
            this.formBankEntity = new FormGroup({
                'nameEntity': new FormControl(this.bankEntity.name, Validators.required),
                'siteURL': new FormControl(this.bankEntity.url, Validators.required),
                'urlLogo': new FormControl(this.bankEntity.logo, Validators.required)
            });
        } else {
            this.startForm();
        }
    }
    onSave() {
        const bankEntity = new BankingEntity(
            null,
            this.formBankEntity.value['nameEntity'],
            this.formBankEntity.value['siteURL'],
            this.formBankEntity.value['urlLogo'],
            true,
        );
        bankEntity.active = true;
        this.bankEntityService.create(bankEntity).subscribe((res) => {
            bankEntity.id = res.body.id;
            this.safeEconomyPreferencesService.addBankEntity(bankEntity);
            this.timeAndMsj();
        });

    }
    onCancel() {
        this.closeFunction(true);
        this.clean();
        this.closeBankEntity();
        this.eventManager.broadcast({ name: 'hideBankEntityForm', content: 'OK'});
    }
    startForm() {
        this.formBankEntity = new FormGroup({
            'nameEntity': new FormControl('',  Validators.required),
            'siteURL': new FormControl('', Validators.required),
            'urlLogo': new FormControl('', Validators.compose([
                Validators.required,
              Validators.maxLength(100),
            ]))
        });
    }

    onUpdate() {
        const bankEntity = {
            id: this.bankEntity.id,
            name: this.formBankEntity.value['nameEntity'],
            url: this.formBankEntity.value['siteURL'],
            logo: this.formBankEntity.value['urlLogo'],
            active: true
        };
        bankEntity.active = true;
        this.bankEntityService.update(bankEntity).subscribe((rest) => {
          this.safeEconomyPreferencesService.deleteBankEntityById(bankEntity);
            this.safeEconomyPreferencesService.addBankEntity(bankEntity);
            this.timeAndMsj();
        });
    }
    closeBankEntity() {
        this.sucess = false;
        this.bankEntity = this.registerBankEntity.setBankEntityToUpdate(null);
    }

    private clean() {
        this.formBankEntity.reset();
    }
    private timeAndMsj() {
        this.clean();
        this.sucess = true;
        setTimeout(() => {
            this.closeBankEntity();
        }, 2000);
    }

}
