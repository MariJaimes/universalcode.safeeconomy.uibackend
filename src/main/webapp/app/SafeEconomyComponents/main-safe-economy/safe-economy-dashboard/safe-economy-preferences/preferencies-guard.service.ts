import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Principal} from '../../../../shared';

@Injectable()
export class PreferenciesGuardService implements CanActivate {
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return this.principal.identity().then((account) => {
            return account.authorities.indexOf('ROLE_ADMIN') !== -1;
        });
    }

    constructor(public router: Router, private principal: Principal ) {}

}
