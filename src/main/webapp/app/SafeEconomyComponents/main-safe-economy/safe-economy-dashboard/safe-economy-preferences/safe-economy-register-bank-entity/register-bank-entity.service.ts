import {Injectable} from '@angular/core';

@Injectable()
export class RegisterBankEntityService {
    bankEntity;
    constructor( ) {}

    setBankEntityToUpdate( bankEntity ) {
        this.bankEntity = bankEntity;
    }
    getBankEntity() {
        return this.bankEntity;
    }

}
