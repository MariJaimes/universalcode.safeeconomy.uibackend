import {Component, OnInit} from '@angular/core';
import {SafeEconomyPreferencesService} from '../preferences.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-delete-modal-bank-entity',
    templateUrl: './delete-modal-bank-entity.component.html',
    styles: []
})
export class DeleteModalBankEntityComponent implements OnInit {

    bankEntity;

    constructor(private safeEconomyPreferencesService: SafeEconomyPreferencesService,
                private close: NgbActiveModal) {
    }

    ngOnInit() {
        this.bankEntity = this.safeEconomyPreferencesService.getBankEntity();
    }

    confirmDelete() {
        this.safeEconomyPreferencesService.deleteBankEntity(this.bankEntity, (res) => {
            this.safeEconomyPreferencesService.removeBankEntity(this.bankEntity);
            setTimeout(() => {
                this.cancelDelete();
            }, 2000);
        });
    }

    cancelDelete() {
        this.close.dismiss('close');
        this.bankEntity = this.safeEconomyPreferencesService.setBankEntityToUpdate(null);
    }
}
