import {Component, OnDestroy, OnInit} from '@angular/core';
import {SafeEconomyPreferencesService} from './preferences.service';
import {JhiEventManager} from 'ng-jhipster';
import {Subscription} from 'rxjs/Subscription';
import {RegisterBankEntityService} from './safe-economy-register-bank-entity/register-bank-entity.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {KeyValueService} from '../../../../entities/key-value/key-value.service';
import {KeyValue} from '../../../../entities/key-value/key-value.model';
import {AddModalDeleteBankEntityService} from './add-modal-delete-bank-entity.service';

@Component({
    selector: 'jhi-safe-economy-preferences',
    templateUrl: './safe-economy-preferences.component.html',
    styles: []
})
export class SafeEconomyPreferencesComponent implements OnInit, OnDestroy {

    modalRef;
    showBakEntityForm = false;
    bankEntities;
    eventSubscriber: Subscription;
    formPreferences: FormGroup;
    preferences;
    preferencesPercetage;
    preferencesTypePayment;
    typePayment;
    preferencesSavingPercentage;
    sucess;
    keyValue;
    keyType;

    constructor(private safeEconomyPreferencesService: SafeEconomyPreferencesService,
                private eventManager: JhiEventManager,
                private registerBankEntityService: RegisterBankEntityService,
                private keyvalueService: KeyValueService,
                private addModalDeleteBankEntityService: AddModalDeleteBankEntityService) {
        this.showBakEntityForm = false;
    }

    ngOnInit() {
        this.sucess = false;
        this.safeEconomyPreferencesService.getAllBankEntity((allEntity) => {
            this.bankEntities = Object.keys(allEntity).map((i) => allEntity[i]).filter((item) => item);
            this.safeEconomyPreferencesService.setBankEntity(this.bankEntities);
            this.getCurrentBankEntity();
        });
        this.safeEconomyPreferencesService.getAllPreferences((allPreferences) => {
            this.preferencesTypePayment = Object.keys(allPreferences).map((i) => allPreferences[i]).filter((item) => item.group === 'TP');
            this.safeEconomyPreferencesService.setPreference(this.preferencesTypePayment);
            console.log(this.preferencesTypePayment);
        });

        this.getAllPreferencesByPercentage();
        this.getAllPreferencesBySavingPercentage();
        this.registerChangeInBankEntity();
        this.sucess = false;
        this.preferences = this.safeEconomyPreferencesService.getPreferences();
        if (this.preferences) {
            this.formPreferences = new FormGroup({
                'percentage': new FormControl(this.preferences.description, Validators.required),
                'typePayment': new FormControl(this.preferences.description, Validators.required),
                'savingPorcentage': new FormControl(this.preferences.description, Validators.required)
            });
        } else {
            this.startForm();
        }
    }

    startForm() {
        this.formPreferences = new FormGroup({
            'percentage': new FormControl('', Validators.required),
            'typePayment': new FormControl('', Validators.required),
            'savingPorcentage': new FormControl('', Validators.required)
        });

    }

    timeAndMsj() {
        this.sucess = true;
        setTimeout(() => {
            this.closePreference();
        }, 2000);
    }

    closePreference() {
        this.sucess = false;
        this.preferences = this.safeEconomyPreferencesService.setPreferences(null);
    }

    getCurrentPreference() {
        this.safeEconomyPreferencesService.currentPreferences.subscribe((temp) => {
            this.preferencesTypePayment = Object.keys(temp).map((i) => temp[i]);
            temp = this.preferencesTypePayment.filter((item) => item);
            this.preferencesTypePayment = temp;
        });
    }
    getCurrentBankEntity() {
        this.safeEconomyPreferencesService.currentBankEntity.subscribe((temp) => {
            this.bankEntities = Object.keys(temp).map((i) => temp[i]);
            temp = this.bankEntities.filter((item) => item);
            this.bankEntities = temp;
        });
    }

    getAllPreferencesByPercentage() {
        this.safeEconomyPreferencesService.getAllPreferences((allPreferences) => {
            this.preferencesPercetage = Object.keys(allPreferences).map((i) => allPreferences[i]).filter((item) => item.group === 'PC')[0];
        });
    }

    savePorcetage() {
        if (!this.preferencesPercetage) {
            const percentage = new KeyValue(
                null,
                'PC',
                this.formPreferences.value['percentage']
            );
            if (percentage.description !== null) {
                this.keyvalueService.create(percentage).subscribe((res) => {
                    this.getAllPreferencesByPercentage();
                });
            }
        } else {
            const percentag = {
                id: this.preferencesPercetage.id,
                group: 'PC',
                description: this.formPreferences.value['percentage'],
            };
            this.keyvalueService.update(percentag).subscribe((rest) => {
            });
        }
        this.timeAndMsj();
    }

    updatePreferenceTypePayment(preferenceTypePayment) {
        this.preferences = preferenceTypePayment;
        this.safeEconomyPreferencesService.setPreferences(preferenceTypePayment);
    }

    getAllPreferencesByTypePayment() {
        this.safeEconomyPreferencesService.getAllPreferences((allPreferences) => {
            this.preferencesTypePayment = Object.keys(allPreferences).map((i) => allPreferences[i]).filter((item) => item.group === 'TP');
            this.safeEconomyPreferencesService.setPreference(this.preferencesTypePayment);
        });
    }

    saveTypePayment() {
        this.keyValue =  this.safeEconomyPreferencesService.getPreferences();

        if (!this.keyValue) {
            const typePayment = new KeyValue(
                null,
                'TP',
                this.formPreferences.value['typePayment']
            );
            if (typePayment.description != null) {
                this.keyvalueService.create(typePayment).subscribe((res) => {
                    typePayment.id = res.body.id;
                    this.safeEconomyPreferencesService.addPreferences(typePayment);
                    this.getAllPreferencesByTypePayment();
                });
            }
        } else {
            const typePayment = {
                id: this.preferences.id,
                group: 'TP',
                description: this.formPreferences.value['typePayment'],
            };
            this.keyvalueService.update(typePayment).subscribe((rest) => {
                this.safeEconomyPreferencesService.deletePreferences(typePayment);
                this.safeEconomyPreferencesService.addPreferences(typePayment);
            });

        }
        this.clean();
        this.timeAndMsj();
    }

    clean() {
        this.formPreferences = new FormGroup({
            'percentage': new FormControl(this.preferencesPercetage.description, Validators.required),
            'typePayment': new FormControl('', Validators.required),
            'savingPorcentage': new FormControl(this.preferencesSavingPercentage.description, Validators.required)
        });

    }

    // savingPercentage
    getAllPreferencesBySavingPercentage() {
        this.safeEconomyPreferencesService.getAllPreferences((allPreferences) => {
            this.preferencesSavingPercentage = Object.keys(allPreferences).map((i) => allPreferences[i]).filter((item) => item.group === 'SP')[0];
        });
    }

    savingPorcetage() {
        if (!this.preferencesSavingPercentage) {
            const savingPercentage = new KeyValue(
                null,
                'SP',
                this.formPreferences.value['savingPorcentage']
            );
            if (savingPercentage !== null) {
                this.keyvalueService.create(savingPercentage).subscribe((res) => {
                    this.getAllPreferencesBySavingPercentage();
                });
            }
        } else {
            const savingPercentag = {
                id: this.preferencesSavingPercentage.id,
                group: 'SP',
                description: this.formPreferences.value['savingPorcentage'],
            };
            this.keyvalueService.update(savingPercentag).subscribe((rest) => {
            });
        }
        this.timeAndMsj();
    }

// bankEntity
    registerChangeInBankEntity() {
        this.eventSubscriber = this.eventManager.subscribe('hideBankEntityForm',
            (response) => this.showBakEntityForm = false);
    }

    toggleBankEtityForm(val) {
        this.showBakEntityForm = !val;
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    updateBankEntity(bankEntity) {
        this.registerBankEntityService.setBankEntityToUpdate(bankEntity);
        this.toggleBankEtityForm(false);
    }
    deleteBankEntity(bankEntity) {
        this.safeEconomyPreferencesService.setBankEntityToUpdate(bankEntity);
        this.modalRef = this.addModalDeleteBankEntityService.addModal();
    }
}
