import {Injectable} from '@angular/core';
import {BankingEntityService} from '../../../../entities/banking-entity';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {KeyValueService} from '../../../../entities/key-value/key-value.service';

@Injectable()
export class SafeEconomyPreferencesService {
    bankEntity;
    preferences;
    private tableBankEntity = new BehaviorSubject(null);
    currentBankEntity = this.tableBankEntity.asObservable();

    private tablePreferences = new BehaviorSubject(null);
    currentPreferences = this.tableBankEntity.asObservable();
    constructor(private bankEntityService: BankingEntityService, private keyValueService: KeyValueService) {}

    // bankEntity
    getAllBankEntity(callback) {
        this.bankEntityService.findAllBankEntitysByActive().subscribe((res) => {
            callback(res.body);
        });
    }
    getBankEntity() {
        return this.bankEntity;
    }
    setBankEntity(bankEntity) {
        this.tableBankEntity.next([...bankEntity]);
    }
    setBankEntityToUpdate( bankEntity ) {
        this.bankEntity = bankEntity;
    }
    addBankEntity(bankEntity) {
        this.tableBankEntity.value.push(bankEntity);
        this.tableBankEntity.next(this.tableBankEntity.value);
    }
    deleteBankEntityById(bankEntity: any) {
        this.tableBankEntity.next(this.tableBankEntity.getValue().filter(function(obj) {
            return obj.id !== bankEntity.id;
        }));
    }
    removeBankEntity(bankEntity) {
        const before = this.tableBankEntity.value.slice(this.tableBankEntity.value.indexOf(bankEntity) + 1, this.tableBankEntity.value.length);
        const after = this.tableBankEntity.value.slice(0, this.tableBankEntity.value.indexOf(bankEntity));
        const a = before.concat(after);
        this.tableBankEntity.next(a);
    }

    deleteBankEntity(banEntity, callback) {
        const bankEntity = {
            id: banEntity.id,
            name: banEntity.name,
            url: banEntity.url,
            logo: banEntity.logo,
            active: false,
        };
        this.bankEntityService.update(bankEntity).subscribe((rest) => {
            callback(rest);
        });
    }
    // preferences
    setPreference(preference) {
        this.tablePreferences.next([...preference]);
    }
    addPreferences(preference) {
        this.tablePreferences.value.push(preference);
        this.tablePreferences.next(this.tablePreferences.value);
    }
    deletePreferences(preference: any) {
        this.tablePreferences.next(this.tablePreferences.getValue().filter(function(obj) {
            return obj.id !== preference.id;
        }));
    }
    setPreferences( preferences ) {
        this.preferences = preferences;
    }
    getAllPreferences(callback) {
        this.keyValueService.findAllKeyValue().subscribe((res) => {
            callback(res.body);
        });
    }
    getPreferences() {
        return this.preferences;
    }

    getKeyValueByKey(key, callback) {
        this.keyValueService.findAllKeyValueByKey(key).subscribe((res) => {
            callback(res.body);
        });
    }

}
