import {Injectable} from '@angular/core';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {DeleteModalBankEntityComponent} from './safe-economy-register-bank-entity/delete-modal-bank-entity.component';

@Injectable()
export class AddModalDeleteBankEntityService {
    private isOpen = false;

    constructor(
        private modalService: NgbModal,

    ) {}

    addModal(): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        const modalRef = this.modalService.open(DeleteModalBankEntityComponent);
        modalRef.result.then((result) => {
            this.isOpen = false;
        }, (reason) => {
            this.isOpen = false;
        });
        return modalRef;
    }
}
