import {Route} from '@angular/router';
import {SafeEconomyRegisterBankEntityComponent} from './safe-economy-register-bank-entity.component';

export const BankEntityRoute: Route = {
    path: 'registerBankEntity',
    component: SafeEconomyRegisterBankEntityComponent
};
