import {SafeEconomyPreferencesComponent} from './safe-economy-preferences.component';
import {Route} from '@angular/router';
import {BankEntityRoute} from './safe-economy-register-bank-entity/register-bank-entity.routes';
import {PreferenciesGuardService} from './preferencies-guard.service';
const MAIN_ROUTES = [
    BankEntityRoute
];
export const PreferencesRoute: Route = {
    path: 'preferences',
    component: SafeEconomyPreferencesComponent,
    children: MAIN_ROUTES,
    canActivate: [PreferenciesGuardService]
};
