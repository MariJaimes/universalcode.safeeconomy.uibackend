import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'jhi-bar-chart-income',
  templateUrl: './bar-chart-income.component.html',
  styles: []
})
export class BarChartIncomeComponent implements OnInit {
    @Input() arrayLabels;
    @Input() arrayNumbers;
    @Input() type;
    barChartLabels: string[];
    barChartType: string = 'bar';
    barChartLegend: boolean = true;
    barChartData: any[];
    barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true
    };

    constructor() {
    }

    ngOnInit() {
        this.buildBarCharts();
    }

    buildBarCharts() {
        this.barChartLabels = this.arrayLabels;
        this.barChartData = [
            {data: this.arrayNumbers, label: this.type}
        ];
    }

    chartClicked(e: any): void {
        console.log(e);
    }

    chartHovered(e: any): void {
        console.log(e);
    }
}
