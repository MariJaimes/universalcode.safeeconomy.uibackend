import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'jhi-bar-chart-saving',
  templateUrl: './bar-chart-saving.component.html',
  styles: []
})
export class BarChartSavingComponent implements OnInit {
    @Input() arrayLabels;
    @Input() arrayNumbers;
    @Input() type;
    barChartLabels: string[];
    barChartType = 'bar';
    barChartLegend = true;
    barChartData: any[];
    barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true
    };

    constructor() {
    }

    ngOnInit() {
        this.buildBarCharts();
    }

    buildBarCharts() {
        console.log(this.arrayNumbers, this.arrayLabels);
        this.barChartLabels = this.arrayLabels;
        this.barChartData = [
            {data: this.arrayNumbers, label: this.type}
        ];
    }

    chartClicked(e: any): void {
        console.log(e);
    }

    chartHovered(e: any): void {
        console.log(e);
    }
}
