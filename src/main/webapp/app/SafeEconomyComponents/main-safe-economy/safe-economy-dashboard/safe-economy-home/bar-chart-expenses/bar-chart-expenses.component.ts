import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {WalletMoveService} from '../../safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-wallet/wallet-move.service';
import {ChartService} from '../../../../safe-economy-utils/charts/chart.service';
import {Chart} from 'chart.js';

@Component({
  selector: 'jhi-bar-chart-expenses',
  templateUrl: './bar-chart-expenses.component.html',
  styles: []
})
export class BarChartExpensesComponent implements OnInit {
    @Input() arrayLabels;
    @Input() arrayNumbers;
    @Input() type;
    prueba;
    barChartLabels: string[];
    barChartType = 'bar';
    barChartLegend = true;
    barChartData: any[];
    barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true
    };

    constructor(private chartService: ChartService,
                private walletService: WalletMoveService) {
    }

    ngOnInit() {
        this.buildBarCharts();
    }

    buildBarCharts() {
        this.barChartLabels = this.arrayLabels;
        this.barChartData  = [
            {data: this.arrayNumbers, label: this.type}
        ];
    }
     chartClicked(e: any): void {
        console.log(e);
    }
     chartHovered(e: any): void {
        console.log(e);
    }
}
