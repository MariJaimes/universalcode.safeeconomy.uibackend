import {Component, OnInit} from '@angular/core';
import {SafeEconomyProfileService} from '../../auth/profile-safe-economy.service';
import {HomeService} from './home.service';
import {CurrencyService} from '../../../safe-economy-utils/currency/currency.service';
import {Currency} from '../../../safe-economy-utils/currency/currency';
import {SafeEconomySavingService} from '../safe-economy-savings/safe-economy-saving.service';
import {SafeEconomyWalletService} from '../safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-wallet/safe-economy-wallet.service';
import {SafeEconomyMovementsService} from '../safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-movements/movements.service';

@Component({
    selector: 'jhi-safe-economy-home',
    templateUrl: './safe-economy-home.component.html',
    styles: []
})
export class SafeEconomyHomeComponent implements OnInit {

    currentUser;
    accounts;
    movements;
    movementsList;
    startGraphicExpenses = false;
    startGraphicIncomes = false;
    startGraphicSavings = false;
    sum;
    graphicExpensesLabels = [];
    graphicExpensesNumbers = [];
    graphicIncomesLabels = [];
    graphicIncomesNumbers = [];
    graphicSavingLabels = [];
    graphicSavingNumbers = [];
    currency: Currency;
    typecoin;
    savings;
    bank;

    constructor(private safeEconomyProfileService: SafeEconomyProfileService,
                private homeService: HomeService,
                private movementsService: SafeEconomyMovementsService,
                private currencyService: CurrencyService,
                private safeEconomySavingService: SafeEconomySavingService,
                private safeEconomyWalletService: SafeEconomyWalletService) {
    }

    ngOnInit() {
        this.sum = 0;
        this.startGraphicExpense();
        this.startGraphicIncome();
        this.startGraphicSaving();
        this.startListMovements();
        this.currencyService.getCurrencyChangeFromColonsToDollas().subscribe((x) => {
            this.currency = x.body;
            this.typecoin =  this.currency.USD_CRC.val.toFixed(2);
        });

    }
    startListMovements() {
        this.safeEconomyWalletService.getWallet((bank) => {
            this.bank = bank;
            this.safeEconomyWalletService.getMovementByAccount(bank.id, (movements) => {
                this.movementsList = Object.keys(movements).map((i) => movements[i]).filter((item) => item && item.kindOfMovement === 'Gasto' || item.kindOfMovement === 'Ingreso');
                this.getFormattedDate(this.movementsList);
            });
        });
    }
    getFormattedDate(movements) {
        this.movementsList.forEach((x) => x.dateOfMovement = x.dateOfMovement.substr(0, 10));
        return movements;
    }
    startGraphicSaving() {
        this.safeEconomySavingService.observalbe.subscribe((any) => {
            this.savings = false;

        });
        this.safeEconomyProfileService.setSafeEconomyUser((userSE) => {
            this.currentUser = userSE;
            this.safeEconomyProfileService.setSafeEconomyUser((user) => {
                this.safeEconomySavingService.getAllSavingsFromUser(user, (response) => {
                    this.savings = Object.keys(response).map((i) => response[i]).filter((item) => item && item);
                    this.savings.forEach((savings) => {
                        this.graphicSavingLabels.push(savings.description);
                        this.graphicSavingNumbers.push(savings.ammount);
                    });
                    if ( this.graphicSavingLabels.length === this.graphicSavingNumbers.length) { this.startGraphicExpenses = true; }
                });
            });
        });
    }

    startGraphicExpense() {
        this.safeEconomyProfileService.setSafeEconomyUser((user) => {
            this.currentUser = user;
            this.movementsService.getAccountsByUserAndActive((accounts) => {
            // this.homeService.getAccountsUser((accounts) => {
                this.accounts = Object.keys(accounts).map((i) => accounts[i]).filter((item) => item);
                this.accounts.forEach((account) =>
                    this.homeService.getMovementByAccount(account.id, (movements) => {
                        this.graphicExpensesLabels.push(account.bank);
                        this.movements = Object.keys(movements).map((i) => movements[i]).filter((item) => item && item.kindOfMovement === 'Gasto'),
                            this.movements.forEach((movement) => {
                                this.sum = this.sum + Math.abs(movement.acreditationAmmount);
                            });
                        this.graphicExpensesNumbers.push(this.sum);
                        this.sum = 0;
                        if ( this.graphicExpensesLabels.length === this.accounts.length) { this.startGraphicSavings = true; }
                    })
                );
            }, user);
        });

    }

    startGraphicIncome() {
        this.safeEconomyProfileService.setSafeEconomyUser((user) => {
            this.currentUser = user;
            this.movementsService.getAccountsByUserAndActive((accounts) => {
            // this.homeService.getAccountsUser((accounts) => {
                this.accounts = Object.keys(accounts).map((i) => accounts[i]).filter((item) => item);
                this.accounts.forEach((account) =>
                    this.homeService.getMovementByAccount(account.id, (movements) => {
                        this.graphicIncomesLabels.push(account.bank);
                        this.movements = Object.keys(movements).map((i) => movements[i]).filter((item) => item && item.kindOfMovement === 'Ingreso'),
                            this.movements.forEach((movement) => {
                                this.sum = this.sum + Math.abs(movement.acreditationAmmount);
                            });
                        this.graphicIncomesNumbers.push(this.sum);
                        this.sum = 0;

                        if ( this.graphicIncomesLabels.length === this.accounts.length ) { this.startGraphicIncomes = true; }

                    }),
                );

            }, user);
        });

    }

}
