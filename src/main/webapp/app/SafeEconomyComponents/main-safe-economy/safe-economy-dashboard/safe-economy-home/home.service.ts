import { Injectable } from '@angular/core';
import {BankAccountService} from '../../../../entities/bank-account';
import {BankMovementPaymentService} from '../../../../entities/bank-movement-payment';

@Injectable()
export class HomeService {
    payment;
    payments;

    constructor(private bankAccountService: BankAccountService,
                private bankMovementPaymentService: BankMovementPaymentService
    ) { }

    getAccountsUser(callback, user) {
        this.bankAccountService.findAllAccountsByUser(user.id).subscribe((res) => {
            callback(res.body);
        });
    }
    getMovementByAccount(id, callback) {
        this.bankMovementPaymentService.findAllMovementByAccount(id).subscribe((res) => {
            callback(res.body);
        });
    }
}
