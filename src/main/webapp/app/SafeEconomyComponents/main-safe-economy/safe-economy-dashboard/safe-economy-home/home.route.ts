import { Route } from '@angular/router';
import {SafeEconomyHomeComponent} from './safe-economy-home.component';

export const HomeSafeEconomyRoute: Route = {
    path: '',
    component: SafeEconomyHomeComponent
};
