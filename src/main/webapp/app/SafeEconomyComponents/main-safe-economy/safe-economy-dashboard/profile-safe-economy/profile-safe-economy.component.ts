import {Component, OnInit} from '@angular/core';
import {SafeEconomyProfileService} from '../../auth/profile-safe-economy.service';
import {Principal} from '../../../../shared';
import {UserSaveEconomyService} from '../../../../entities/user-save-economy';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {SafeEconomyConfirmDeactivateService} from './confirm-deactivate.service';

@Component({
    selector: 'jhi-profile-safe-economy',
    templateUrl: './profile-safe-economy.component.html',
    styles: []
})

export class ProfileSafeEconomyComponent implements OnInit {
    user;
    originalUser;
    account;
    modalRef: NgbModalRef;

    constructor(public profileService: SafeEconomyProfileService,
                private principal: Principal,
                private userSafeEconomyService: UserSaveEconomyService,
                private confirmDeactivateService: SafeEconomyConfirmDeactivateService) {
    }

    ngOnInit() {

        this.profileService.setSafeEconomyUser((user) => {
            this.user = {...user};
            this.originalUser = {...user};
        });
        this.principal.identity().then((account) => {
            this.account = account;
            this.account.createdDate = account.createdDate.substr(0, 10);
        });
    }

    onUpdate() {
        if (this.originalUser.image !== this.user.image ||
            this.originalUser.name !== this.user.name ||
            this.originalUser.lastName !== this.user.lastName ||
            this.originalUser.telephone !== this.user.telephone) {
            const userToUpdate = {...this.user};
            this.user = null;
            this.userSafeEconomyService.update(userToUpdate).subscribe((response) => {
                this.user = {...response.body};
                this.originalUser = {...response.body};
            });
        }
    }

    onDeactivate() {
        this.modalRef = this.confirmDeactivateService.open();
    }
}
