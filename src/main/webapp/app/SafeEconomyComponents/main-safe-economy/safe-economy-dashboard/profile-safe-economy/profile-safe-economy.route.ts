import { Route } from '@angular/router';
import {ProfileSafeEconomyComponent} from './profile-safe-economy.component';

export const UserProfileSafeEconomyRoute: Route = {
    path: 'profile',
    component: ProfileSafeEconomyComponent
};
