import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {UserSaveEconomyService} from '../../../../../entities/user-save-economy/user-save-economy.service';
import {Principal} from '../../../../../shared/auth/principal.service';
import {LoginService} from '../../../../../shared';
import {Router} from '@angular/router';
import {SafeEconomyProfileService} from '../../../auth/profile-safe-economy.service';
import {SafeEconomyWalletService} from '../../safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-wallet/safe-economy-wallet.service';

@Component({
    selector: 'jhi-confirm-deactivate-account',
    templateUrl: './confirm-deactivate-account.component.html',
    styles: []
})
export class ConfirmDeactivateAccountComponent implements OnInit {

    loading = false;
    constructor(public activeModal: NgbActiveModal,
                private principal: Principal,
                private userSafeEconomyService: UserSaveEconomyService,
                private loginService: LoginService,
                private router: Router,
                private profileService: SafeEconomyProfileService,
                private walletservice: SafeEconomyWalletService) {
    }

    ngOnInit() {
    }

    cancel() {
        this.activeModal.dismiss('cancel');
    }

    onDeactivate() {
        this.loading = true;
        this.principal.identity().then((account) => {
            account.createdDate += 'T05:46:46Z';
            this.userSafeEconomyService.deactivateUser(account).subscribe((responce) => {
                this.loginService.logout();
                this.router.navigate(['']);
                this.profileService.removeSafeEconomyUser();
                this.walletservice.removeWallet();
                this.cancel();
            });
        });
    }

}
