import {Injectable} from '@angular/core';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {SERVER_API_URL} from '../../../../app.constants';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {DeleteComponentSavingComponent} from './delete/deleteSaving.component';

@Injectable()
export class DeleteModalService {

    private isOpen = false;

    constructor(
        private http: HttpClient,
        private modalService: NgbModal
    ) {}

    open(): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;
        const modalRef = this.modalService.open(DeleteComponentSavingComponent);
        modalRef.result.then((result) => {
            this.isOpen = false;
        }, (reason) => {
            this.isOpen = false;
        });
        return modalRef;
    }

    save(account: any): Observable<any> {
        return this.http.post(SERVER_API_URL + 'api/register', account);
    }
}
