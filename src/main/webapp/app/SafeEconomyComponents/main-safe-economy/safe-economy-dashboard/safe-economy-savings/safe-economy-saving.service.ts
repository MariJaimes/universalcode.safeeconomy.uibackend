import {Injectable} from '@angular/core';
import {SavingService} from '../../../../entities/saving/saving.service';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class SafeEconomySavingService {

    savingList;

    saving;

    user;

    observalbe = new Subject<null>();

    constructor( private savingService: SavingService) {}

    getAllSavingsFromUser(user, callback) {
        this.user = user;
        this.savingService.getAllSavingsByUser(user.id).subscribe((res) => {
            this.savingList = Object.keys(res.body).map((i) => res.body[i]).filter((item) => item);
            callback(this.savingList);
        });
    }
    setSaving(saving) {
        this.saving = saving;
    }

    getSaving() {
        return this.saving;
    }

    addSaveToList() {
        this.observalbe.next(null);
    }
}
