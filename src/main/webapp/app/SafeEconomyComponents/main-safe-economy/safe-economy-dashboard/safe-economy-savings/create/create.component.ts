import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Saving} from '../../../../../entities/saving';
import {SafeEconomySavingService} from '../safe-economy-saving.service';
import {SafeEconomyBankAccountsService} from '../../safe-economy-bank-accounts/bank-accounts.service';
import {SafeEconomyProfileService} from '../../../auth/profile-safe-economy.service';
import {SavingService} from '../../../../../entities/saving/saving.service';

@Component({
    selector: 'jhi-create',
    templateUrl: './create.component.html',
    styleUrls: ['create.component.css']
})
export class CreateComponent implements OnInit {

    error;
    today;
    success = false;
    loading;
    saving: Saving;
    listBank;
    currentUser;
    amountPerMonth = 1;
    creatingUpdating = false ;

    constructor(public activeModal: NgbActiveModal,
                private securityService: SafeEconomyProfileService,
                private savingService: SafeEconomySavingService,
                private bankAccountService: SafeEconomyBankAccountsService,
                private sanvingServiceBE: SavingService) {
    }

    ngOnInit() {
        this.loading = true;

        this.securityService.setSafeEconomyUser((user) => {
            this.currentUser = user;
            this.bankAccountService.getAllBankAccountsByUser(user, (res) => {
                this.saving.userSaveEconomyId = user.id;
                this.listBank = res.filter((item) => item.bank !== 'Safe Economy');
                this.saving.bankAccountId = this.listBank[0].id;
                this.loading = false;
            });
        });
        this.loadSave();
    }
    canCreateUpdate() {
        return (this.saving.ammount && this.saving.ammount > 0) &&
                this.saving.description &&
                (this.saving.savingPercentage && this.saving.savingPercentage > 0);
    }
    loadSave() {
        if ( this.savingService.getSaving()) {
            this.saving = this.savingService.getSaving();
        } else {
            this.saving = new Saving(null,
                null,
                null,
                null,
                0,
                null,
                null);
        }
    }
    close() {
        this.activeModal.dismiss('closed');
    }
    getSelectBank( id: string ) {
        this.saving.bankAccountId = parseInt(id, 10);
    }
    getAmountPerMonth() {
        const percentage = '0.' + this.saving.savingPercentage;
        const savingPercentage = parseFloat(percentage);
        const bank = this.bankAccountService.getBanks().filter((item) => item.id === this.saving.bankAccountId)[0];
        if (bank) {
            this.amountPerMonth = bank.budget * savingPercentage;
        }
        return bank.budget * savingPercentage;
    }
    getFinalDate() {
        const date = new Date();
        date.setMonth(date.getMonth() + Math.ceil(this.saving.ammount / this.amountPerMonth) + 1);
        const a = this.parseDate(date);
        this.saving.dateToFinish = a;
        return a;
    }
    createSaving() {
        this.success = false;
        this.error = false;
        this.creatingUpdating = true;
        this.saving.dateToFinish = this.today;
        if (this.saving.id) {
            this.sanvingServiceBE.update(this.saving).subscribe(( res ) => {
                this.savingService.addSaveToList();
                this.creatingUpdating = false;
                this.success = true;
                setTimeout(() => {
                    this.success = false;
                }, 7000);
            }, (error) => {
                this.error = true;
                setTimeout(() => {
                    this.error = false;
                }, 7000);
            });
        } else {
            this.sanvingServiceBE.create(this.saving).subscribe(( res ) => {
                this.savingService.addSaveToList();
                this.creatingUpdating = false;
                this.success = true;
                setTimeout(() => {
                    this.success = false;
                }, 7000);
                this.savingService.setSaving(null);
            }, (error) => {
                this.error = true;
                setTimeout(() => {
                    this.error = false;
                }, 7000);
            });
        }

    }

    parseDate(date) {
        const dd = date.getDate();
        const mm = date.getMonth() + 1;
        const yyyy = date.getFullYear();
        this.today = yyyy + '-' + 0 + mm + '-' + dd;
        return this.today;
    }
}
