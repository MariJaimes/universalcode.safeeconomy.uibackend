import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Saving} from '../../../../../entities/saving';
import {SafeEconomySavingService} from '../safe-economy-saving.service';
import {SavingService} from '../../../../../entities/saving/saving.service';

@Component({
    selector: 'jhi-delete',
    templateUrl: './delete.component.html',
    styles: []
})
export class DeleteComponentSavingComponent implements OnInit {

    savingToDel: Saving;

    success = false;

    error = false;

    loading = false;

    constructor(public activeModal: NgbActiveModal,
                private savingService: SafeEconomySavingService,
                private savingServiceBE: SavingService) {
    }

    close() {
        this.activeModal.dismiss('closed');
    }
    ngOnInit() {
        this.savingToDel = this.savingService.getSaving();
    }
    onDelete() {
        this.loading = true;
        this.savingServiceBE.delete(this.savingToDel.id).subscribe((res) => {
            this.success = true;
            this.loading = false;
            this.savingService.addSaveToList();
            setTimeout(() => {
                this.close();
            }, 2000);
        }, (error) => {
            this.loading = false;
            this.error = true;
        });
    }
}
