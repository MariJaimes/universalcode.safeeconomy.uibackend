import {Route} from '@angular/router';
import {SafeEconomySavingsComponent} from './safe-economy-savings.component';

export const SavingsRoute: Route = {
    path: 'savings',
    component: SafeEconomySavingsComponent,
};
