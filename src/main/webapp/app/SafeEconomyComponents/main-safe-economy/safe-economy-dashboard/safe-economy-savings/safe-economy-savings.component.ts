import {Component, OnInit} from '@angular/core';
import {SafeEconomySavingService} from './safe-economy-saving.service';
import {SafeEconomyProfileService} from '../../auth/profile-safe-economy.service';
import {SafeEconomyBankAccountsService} from '../safe-economy-bank-accounts/bank-accounts.service';
import {SavingModalService} from './savings-modal.service';
import {DeleteModalService} from './delete-modal.service';
import {BankingEntityService} from '../../../../entities/banking-entity';

@Component({
    selector: 'jhi-safe-economy-savings',
    templateUrl: './safe-economy-savings.component.html',
    styleUrls: ['./safe-economy-saving.css']
})
export class SafeEconomySavingsComponent implements OnInit {
    banks;
    savings;
    listOfBanks;
    constructor(private safeEconomySavingService: SafeEconomySavingService,
                private profileSafeEconomy: SafeEconomyProfileService,
                private bankAccountService: SafeEconomyBankAccountsService,
                private savingModalService: SavingModalService,
                private delModalService: DeleteModalService,
                private bankService: BankingEntityService) {

    }
    ngOnInit() {
        this.bankService.findAllEntity().subscribe( (res) => {
            this.listOfBanks = Object.keys(res.body).map((i) => res.body[i]).filter((item) => item);
        });
        this.safeEconomySavingService.observalbe.subscribe((any) => {
            this.savings = false;
            this.profileSafeEconomy.setSafeEconomyUser((user) => {
                this.safeEconomySavingService.getAllSavingsFromUser(user, (response) => {
                    this.savings = response;
                });
            });
        });

        this.profileSafeEconomy.setSafeEconomyUser((user) => {
                this.safeEconomySavingService.getAllSavingsFromUser(user, (response) => {
                    this.savings = response;
                });
            this.bankAccountService.getAllBankAccountsByUser(user, (response) => {
                this.banks = response;
            });
        });
    }
    openSavingModal() {
        this.safeEconomySavingService.setSaving(null);
        this.savingModalService.open();
    }
    updateSaving( saving ) {
        this.safeEconomySavingService.setSaving(saving);
        this.savingModalService.open();
    }
    getBankName(id) {
        return this.banks.find((item) => item.id === id);
    }
    toRemove( saving ) {
        this.safeEconomySavingService.setSaving(saving);
        this.delModalService.open();
    }
    getBankImage( saving ) {
        return this.listOfBanks.
        find((item) => item.id === this.banks.find((item2) => item2.id === saving.bankAccountId).bankEntityId).logo;
    }
    getSavingPerventage(save) {

    }
}
