import {Route} from '@angular/router';
import {UserProfileSafeEconomyRoute} from './profile-safe-economy/profile-safe-economy.route';
import {HomeSafeEconomyRoute} from './safe-economy-home/home.route';
import {SafeEconomyDashboardComponent} from './safe-economy-dashboard.component';
import {PreferencesRoute} from './safe-economy-preferences/preferences.routes';
import {PaymentsSafeEconomy} from './safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-payments/payments.route';
import {CalendarSafeEconomy} from './safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-payments/calendar/calendar.route';
import {SavingsRoute} from './safe-economy-savings/safe-economy-savings.route';
import {MovementsSafeEconomy} from './safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-movements/movements.route';

const MAIN_ROUTES = [
    UserProfileSafeEconomyRoute,
    HomeSafeEconomyRoute,
    PreferencesRoute,
    UserProfileSafeEconomyRoute,
    HomeSafeEconomyRoute,
    PreferencesRoute,
    PaymentsSafeEconomy,
    CalendarSafeEconomy,
    SavingsRoute,
    MovementsSafeEconomy
];

export const DashboardSafeEconomyRoutes: Route = {
    path: '',
    component: SafeEconomyDashboardComponent,
    children: MAIN_ROUTES
};
