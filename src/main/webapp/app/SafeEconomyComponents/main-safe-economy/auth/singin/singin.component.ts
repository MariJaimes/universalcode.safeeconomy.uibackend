import { Component, OnInit } from '@angular/core';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Register} from '../../../../account/register/register.service';
import {HttpErrorResponse} from '@angular/common/http';
import {EMAIL_ALREADY_USED_TYPE, LOGIN_ALREADY_USED_TYPE} from '../../../../shared';

@Component({
  selector: 'jhi-singin',
  templateUrl: './singin.component.html',
  styles: []
})
export class SinginComponent implements OnInit {

    modalRef: NgbModalRef;
    singinForm: FormGroup;
    loading = false;
    success = false;
    errorUserExists;
    errorEmailExists;
    error;

    constructor(private registerService: Register) { }

    ngOnInit() {
        this.initForm();
    }

    initForm() {
        this.singinForm = new FormGroup({
            'userName': new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength( 6),
                Validators.maxLength(50),
                Validators.pattern('^[_\'.@A-Za-z0-9-]*$')
            ])),
            'name': new FormControl('', Validators.required),
            'lastName': new FormControl('', Validators.required),
            'email': new FormControl('',
                Validators.compose(
                    [
                        Validators.required,
                        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
                    ]
                )
            ),
            'password': new FormControl(
                '',
                Validators.compose([
                    Validators.required,
                    Validators.minLength(8),
                    Validators.pattern('(?=^.{8,}$)((?=.*\\d)|(?=.*\\W+))(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*$')
                ])),
            'confirm_password': new FormControl(
                '',
                Validators.compose([
                    Validators.required,
                    Validators.minLength(8)
                ])
            ),
        });
    }
    singin() {
        this.loading = true;
        if (this.singinForm.value['password'] === this.singinForm.value['confirm_password'] ) {
            const user = {
                login : this.singinForm.value['userName'],
                email : this.singinForm.value['email'],
                password: this.singinForm.value['password'],
                firstName: this.singinForm.value['name'],
                lastName: this.singinForm.value['lastName'],
                langKey: 'es',
                fact: null,
                imageUrl: 'https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png'
            };
            this.registerService.save(user).subscribe((res) => {
                this.success = true;
                this.initForm();
                this.loading = false;
            }, (error) => this.processError(error));

        }
    }
    private processError(response: HttpErrorResponse) {
        this.loading = false;
        this.success = null;
        if (response.status === 400 && response.error.type === LOGIN_ALREADY_USED_TYPE) {
            this.errorUserExists = 'ERROR';
        } else if (response.status === 400 && response.error.type === EMAIL_ALREADY_USED_TYPE) {
            this.errorEmailExists = 'ERROR';
        } else {
            this.error = 'ERROR';
        }
    }

}
