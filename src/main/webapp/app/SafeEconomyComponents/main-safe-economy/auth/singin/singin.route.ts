import { Route } from '@angular/router';
import {SinginComponent} from './singin.component';

export const SafeEconomySingInRoute: Route = {
    path: 'singin',
    component: SinginComponent
};
