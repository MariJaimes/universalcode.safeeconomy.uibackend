import { Route } from '@angular/router';
import {ResetPasswordRequestComponent} from './reset-password-request.component';

export const resetPasswordRequestRoute: Route = {
    path: 'reset/request',
    component: ResetPasswordRequestComponent,

};
