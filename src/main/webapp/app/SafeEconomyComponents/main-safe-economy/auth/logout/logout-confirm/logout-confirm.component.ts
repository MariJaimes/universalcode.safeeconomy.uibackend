import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../../../../shared';
import {Router} from '@angular/router';
import {SafeEconomyProfileService} from '../../profile-safe-economy.service';
import {SafeEconomyWalletService} from '../../../safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-wallet/safe-economy-wallet.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-logout-confirm',
    templateUrl: './logout-confirm.component.html',
    styles: []
})
export class LogoutConfirmComponent implements OnInit {
    loading = false;
    constructor(public activeModal: NgbActiveModal,
                private loginService: LoginService,
                private router: Router,
                private profileService: SafeEconomyProfileService,
                private walletservice: SafeEconomyWalletService,
                ) {
    }

    ngOnInit() {
    }
    close() {
        this.activeModal.close('cancel');
    }
    onConfirm() {
        this.loading = true;
        this.loginService.logout();
        this.profileService.removeSafeEconomyUser();
        this.walletservice.removeWallet();
        this.activeModal.close('logout');
        this.router.navigate(['']);
    }

}
