import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from '../../../../shared';
import {SafeEconomyProfileService} from '../profile-safe-economy.service';
import {SafeEconomyWalletService} from '../../safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-wallet/safe-economy-wallet.service';
import {LogoutModalService} from './logout-modal.service';

@Component({
  selector: 'jhi-logout',
  templateUrl: './logout.component.html',
  styles: []
})
export class LogoutComponent implements OnInit {

  constructor( private loginService: LoginService,
               private router: Router,
               private profileService: SafeEconomyProfileService,
               private walletservice: SafeEconomyWalletService,
               private logoutModal: LogoutModalService) { }

  ngOnInit() {
  }

  logout() {
    this.logoutModal.open();
  }
}
