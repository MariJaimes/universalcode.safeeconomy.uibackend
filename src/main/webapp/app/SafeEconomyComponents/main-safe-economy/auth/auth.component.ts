import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../../shared';
import {Router} from '@angular/router';

@Component({
    selector: 'jhi-auth',
    templateUrl: './auth.component.html',
    styles: []
})
export class AuthComponent implements OnInit {

    constructor( private loginService: LoginService, private router: Router) { }

    logout() {
        this.loginService.logout();
    }
    ngOnInit() {
    }

}
