import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {LoginService} from '../../../../../shared/login/login.service';
import {Principal} from '../../../../../shared/auth/principal.service';
import {SafeEconomyProfileService} from '../../profile-safe-economy.service';
import {SafeEconomyWalletService} from '../../../safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-wallet/safe-economy-wallet.service';

@Component({
  selector: 'jhi-login-modal',
  templateUrl: './login-modal.component.html',
  styles: []
})
export class LoginModalComponent implements OnInit {

    logInForm: FormGroup;
    tokenForm: FormGroup;
    hasToken = false;
    loading = false;
    authenticationError = false;
    modalRef: NgbModalRef;
    token;
    tokenRecivedMesaje = false;
    wrongToken = false;

    constructor( public activeModal: NgbActiveModal,
                 public loginService: LoginService,
                 public profileService: SafeEconomyProfileService,
                 public principal: Principal,
                 public router: Router
                 ) { }

    ngOnInit() {
        this.initForm();
    }
    close() {
        this.activeModal.dismiss('closed');
    }
    requestResetPassword() {
        this.close();
        this.router.navigate(['/reset', 'request']);
    }
    goToSingIn() {
        this.close();
        this.router.navigate(['singin']);
    }
    getToken() {
        this.loading = true;
        this.profileService.getSafeeconomyUserByLogin(this.logInForm.value['userName'], (user) => {
            this.loading = false;
            this.authenticationError = false;
            this.hasToken = true;
            this.token = user.token;
            this.tokenRecivedMesaje = true;
            setTimeout(() => this.tokenRecivedMesaje = false, 6000);
            }, (error) => {
            this.authenticationError = true;
            this.hasToken = false;
            this.loading = false;
        });
    }

    validateToken() {
        if ( this.tokenForm.value['token'] === this.token) {
            this.hasToken = false;
            this.login();
        } else {

        }
    }
    login() {
        this.loading = true;
        const credentials = {
            username: this.logInForm.value['userName'],
            password: this.logInForm.value['password'],
            rememberMe: false
        };
        this.loginService.login(credentials).then(() => {
            this.principal.identity().then((account) => {
                this.profileService.setSafeEconomyUser( (user) => {
                    this.authenticationError = false;
                    this.activeModal.dismiss('login success');
                    this.hasToken = true;
                    this.loading = false;
                    this.router.navigate(['home']);
                });
            });
        }).catch(() => {
            this.authenticationError = true;
            this.loading = false;
            this.initForm();
        });
    }
    initForm() {
        this.logInForm = new FormGroup({
            'userName': new FormControl('', Validators.required),
            'password': new FormControl('', Validators.required)
        });
        this.tokenForm = new FormGroup({
            'token': new FormControl('')
        });
    }

}
