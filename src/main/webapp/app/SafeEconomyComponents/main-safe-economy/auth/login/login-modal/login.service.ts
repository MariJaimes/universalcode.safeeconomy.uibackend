import { Injectable } from '@angular/core';
import {SafeEconomyPrincipalService} from '../safe-economy-principal.service';
import {SafeEconomyAuthJwtService} from '../safe-economy-auth-jwt.service';

@Injectable()
export class SafeEconomyLoginService {

    constructor(
        private principal: SafeEconomyPrincipalService,
        private authServerProvider: SafeEconomyAuthJwtService
    ) {}

    login(credentials, callback?) {
        const cb = callback || function() {};
        return new Promise((resolve, reject) => {
            this.authServerProvider.login(credentials).subscribe((data) => {
                this.principal.identity(true).then((account) => {
                    resolve(data);
                });
                return cb();
            }, (err) => {
                this.logout();
                reject(err);
                return cb(err);
            });
        });
    }

    loginWithToken(jwt, rememberMe) {
        return this.authServerProvider.loginWithToken(jwt, rememberMe);
    }

    logout() {
        this.authServerProvider.logout().subscribe();
        this.principal.authenticate(null);
    }

}
