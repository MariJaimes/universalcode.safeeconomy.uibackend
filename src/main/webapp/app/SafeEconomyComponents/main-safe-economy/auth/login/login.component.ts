import { Component, OnInit } from '@angular/core';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {LoginModalService} from './login-modal.service';

@Component({
  selector: 'jhi-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent implements OnInit {
    modalRef: NgbModalRef;
    constructor( private loginService: LoginModalService ) { }
    ngOnInit() {
    }

    login() {
        this.modalRef = this.loginService.open();
    }

}
