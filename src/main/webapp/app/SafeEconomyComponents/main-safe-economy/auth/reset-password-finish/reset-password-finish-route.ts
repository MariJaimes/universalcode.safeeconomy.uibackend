import {Route} from '@angular/router';
import {ResetPasswordFinishComponent} from './reset-password-finish.component';

export const resetPasswordFinishRoute: Route = {
    path: 'reset/finish',
    component: ResetPasswordFinishComponent,

};
