import {AfterViewInit, Component, ElementRef, OnInit, Renderer} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {PasswordResetFinishService} from '../../../../account';
import {LoginModalService} from '../login/login-modal.service';

@Component({
  selector: 'jhi-reset-password-finish',
  templateUrl: './reset-password-finish.component.html',
  styles: []
})
export class ResetPasswordFinishComponent implements OnInit, AfterViewInit {
    confirmPassword: string;
    doNotMatch: string;
    error: string;
    keyMissing: boolean;
    resetAccount: any;
    success: string;
    modalRef: NgbModalRef;
    key: string;
    resetPasswordForm: FormGroup;

    constructor(
        private passwordResetFinishService: PasswordResetFinishService,
        private loginModalService: LoginModalService,
        private route: ActivatedRoute,
        private elementRef: ElementRef, private renderer: Renderer
    ) {
    }

    ngOnInit() {
        this.initForm();
        this.route.queryParams.subscribe((params) => {
            this.key = params['key'];
        });
        this.resetAccount = {};
        this.keyMissing = !this.key;
    }

    ngAfterViewInit() {
        if (this.elementRef.nativeElement.querySelector('#password') != null) {
            this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#password'), 'focus', []);
        }
    }

    initForm() {
        this.resetPasswordForm = new FormGroup({
            'newPassword': new FormControl(
                '',
                Validators.compose([
                    Validators.required,
                    Validators.minLength(8),
                    Validators.pattern('(?=^.{8,}$)((?=.*\\d)|(?=.*\\W+))(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*$')
                ])),
            'newPasswordConfirm': new FormControl(
                '',
                Validators.compose([
                    Validators.required,
                    Validators.minLength(8)
                ])
            ),
        });
    }

    finishReset() {
        this.doNotMatch = null;
        this.error = null;
        if (this.resetPasswordForm.value['newPassword'] !== this.resetPasswordForm.value['newPasswordConfirm']) {
            this.doNotMatch = 'ERROR';
        } else {
            this.passwordResetFinishService.save({key: this.key, newPassword: this.resetPasswordForm.value['newPassword']}).subscribe(() => {
                this.success = 'OK';
            }, () => {
                this.success = null;
                this.error = 'ERROR';
            });
        }
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }
}
