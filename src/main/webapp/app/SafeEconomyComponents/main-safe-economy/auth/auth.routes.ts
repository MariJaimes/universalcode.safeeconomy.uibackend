import { Route } from '@angular/router';
import {AuthComponent} from './auth.component';

export const SafeEconomyAuthRoute: Route = {
    path: '',
    component: AuthComponent
};
