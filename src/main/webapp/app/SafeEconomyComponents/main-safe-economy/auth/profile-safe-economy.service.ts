import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Principal} from '../../../shared/auth/principal.service';
import {UserSaveEconomyService} from '../../../entities/user-save-economy/user-save-economy.service';

@Injectable()
export class SafeEconomyProfileService {

    safeEconomyUser;

    constructor( public router: Router,
                 private principal: Principal,
                 private userSafeEconomyService: UserSaveEconomyService ) {}
    getSafeEconomyUser() {
        return this.safeEconomyUser;
    }
    setSafeEconomyUser(callback) {
        this.principal.identity().then((account) => {
            this.userSafeEconomyService.getSafeeconommyLoggedUser(account.id).
                subscribe((userSaveEconomyResponse) => {
                   this.safeEconomyUser = userSaveEconomyResponse.body;
                   callback(this.safeEconomyUser);
                });
        });
    }
    getSafeeconomyUserByLogin(login, callbacksuccess, callbackerror) {
        this.userSafeEconomyService.getSafeEconomyUserbyLogin(login).subscribe((res) => {
            callbacksuccess(res.body);
        },  (error) => {
            callbackerror(error);
        });
    }
    removeSafeEconomyUser() {
        this.safeEconomyUser = null;
    }

}
