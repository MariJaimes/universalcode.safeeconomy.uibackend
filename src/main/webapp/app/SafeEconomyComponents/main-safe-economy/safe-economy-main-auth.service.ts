import { Injectable } from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Principal} from '../../shared/index';
@Injectable()
export class AuthGuardService implements CanActivate {

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return this.principal.identity().then((account) => {
            if (account) {
                return account.authorities !== null;
            }
            return false;
        });
    }
    constructor(public router: Router, private principal: Principal) {}
}
