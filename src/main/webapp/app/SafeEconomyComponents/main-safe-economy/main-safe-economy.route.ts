import { Route } from '@angular/router';
import {MainSafeEconomyComponent} from './main-safe-economy.component';
import {AuthGuardService} from './safe-economy-main-auth.service';
import {DashboardSafeEconomyRoutes} from './safe-economy-dashboard/safe-conomy-dashboard.routes';
import {MonederoSafeEconomy} from './safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/monedero.route';
import {BankAccountDashboard} from './safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account-dashboard/safe-economy-bank-account-dashboard.route';

const MAIN_ROUTES = [
    DashboardSafeEconomyRoutes,
    MonederoSafeEconomy,
    BankAccountDashboard
];

export const MainSafeEconomyRoute: Route = {
    path: 'home',
    component: MainSafeEconomyComponent,
    children: MAIN_ROUTES,
    canActivate: [AuthGuardService]
};
