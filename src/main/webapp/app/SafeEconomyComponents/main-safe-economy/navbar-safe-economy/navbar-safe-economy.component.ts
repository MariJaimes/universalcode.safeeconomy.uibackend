import {Component, OnInit} from '@angular/core';
import {Principal} from '../../../shared';

@Component({
    selector: 'jhi-navbar-safe-economy',
    templateUrl: './navbar-safe-economy.component.html',
    styles: []
})
export class NavbarSafeEconomyComponent implements OnInit {
    loggedUser;

    constructor(private principal: Principal) {
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.loggedUser = account;
        });
    }
}
