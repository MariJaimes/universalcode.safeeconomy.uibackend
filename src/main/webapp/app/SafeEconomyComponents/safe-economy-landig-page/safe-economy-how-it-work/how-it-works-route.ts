import { Route } from '@angular/router';
import {SafeEconomyHowItWorkComponent} from './safe-economy-how-it-work.component';

export const HowItWorksRoute: Route = {
    path: 'how-it-works',
    component: SafeEconomyHowItWorkComponent
};
