import { Route } from '@angular/router';
import {ActivationKeyComponent} from './activation-key.component';

export const ActivationKeyRoute: Route = {
    path: 'activate',
    component: ActivationKeyComponent,
    data: {
        authorities: [],
        pageTitle: 'Activation'
    }
};
