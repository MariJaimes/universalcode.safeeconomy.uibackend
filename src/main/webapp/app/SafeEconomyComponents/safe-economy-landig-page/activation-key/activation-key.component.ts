import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ActivateService} from '../../../account/activate/activate.service';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {LoginModalService} from '../../main-safe-economy/auth/login/login-modal.service';

@Component({
  selector: 'jhi-activation-key',
  templateUrl: './activation-key.component.html',
  styles: []
})
export class ActivationKeyComponent implements OnInit {
    error;
    success;
    modalRef: NgbModalRef;
    constructor(private activateService: ActivateService,
              private loginModalService: LoginModalService,
              private route: ActivatedRoute,
              public router: Router) { }

    ngOnInit() {
      this.route.queryParams.subscribe((params) => {
          this.activateService.get(params['key']).subscribe(() => {
              this.error = null;
              this.success = 'OK';
          }, () => {
              this.success = null;
              this.error = 'ERROR';
          });
      });
    }
    toSingin() {
        this.router.navigate(['singin']);

    }
    login() {
        this.modalRef = this.loginModalService.open();
    }

}
