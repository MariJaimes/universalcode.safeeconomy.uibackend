import { Injectable } from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Principal} from '../../../shared';

@Injectable()
export class LandingPageAuthGuardService implements CanActivate {

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return this.principal.identity().then((account) => {
            return !account ;
        });
    }
    constructor(public router: Router, private principal: Principal) {}
}
