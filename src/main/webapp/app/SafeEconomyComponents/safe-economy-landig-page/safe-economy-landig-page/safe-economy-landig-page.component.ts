import { Component, OnInit } from '@angular/core';
import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'jhi-safe-economy-landig-page',
  templateUrl: './safe-economy-landig-page.component.html',
    styleUrls: ['../safe-economy-landig-page/safe-economy-landing-page.css']
})
export class SafeEconomyLandigPageComponent implements OnInit {

    constructor(config: NgbCarouselConfig) {
          config.interval = 5000;
          config.wrap = true;
          config.keyboard = true;
      }

  ngOnInit() {
  }

}
