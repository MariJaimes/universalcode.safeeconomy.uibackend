import { Route } from '@angular/router';
import {SafeEconomyLandigPageComponent} from './safe-economy-landig-page.component';
import {LandingPageAuthGuardService} from './landing-page-auth-guard.service';
export const LandingPageRoute: Route = {
    path: '',
    component: SafeEconomyLandigPageComponent,
    canActivate: [LandingPageAuthGuardService]
};
