import { Route } from '@angular/router';
import {SafeEconomyLandingMainComponent} from './safe-economy-landing-main.component';
import {LandingPageRoute} from '../safe-economy-landig-page/safe-economy-landing-page.route';
import {HowItWorksRoute} from '../safe-economy-how-it-work/how-it-works-route';
import {SafeEconomySingInRoute} from '../../main-safe-economy/auth/singin/singin.route';
import {ActivationKeyRoute} from '../activation-key/activation-key.route';
import {resetPasswordFinishRoute} from '../../main-safe-economy/auth/reset-password-finish/reset-password-finish-route';
import {resetPasswordRequestRoute} from '../../main-safe-economy/auth/reset-password-request/reset-password-request.route';

const LANDING_ROUTES = [
    LandingPageRoute,
    HowItWorksRoute,
    SafeEconomySingInRoute,
    ActivationKeyRoute,
    resetPasswordFinishRoute,
    resetPasswordRequestRoute,
];

export const LandingPageMainRoute: Route = {
    path: '',
    component: SafeEconomyLandingMainComponent,
    children: LANDING_ROUTES
};
