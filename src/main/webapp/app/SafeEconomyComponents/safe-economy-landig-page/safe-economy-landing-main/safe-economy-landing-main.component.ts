import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'jhi-safe-economy-landing-main',
  templateUrl: './safe-economy-landing-main.component.html',
    styleUrls: ['../safe-economy-landing-main/landing-page-main.css']

})
export class SafeEconomyLandingMainComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
