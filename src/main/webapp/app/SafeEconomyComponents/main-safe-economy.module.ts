import { NgModule } from '@angular/core';
import {MainSafeEconomyComponent} from './main-safe-economy/main-safe-economy.component';
import {NavbarSafeEconomyComponent} from './main-safe-economy/navbar-safe-economy/navbar-safe-economy.component';
import { SafeEconomyDashboardComponent } from './main-safe-economy/safe-economy-dashboard/safe-economy-dashboard.component';
import { SafeEconomyHomeComponent } from './main-safe-economy/safe-economy-dashboard/safe-economy-home/safe-economy-home.component';
import {RouterModule} from '@angular/router';
import { PieChartComponent } from './safe-economy-utils/charts/pie-chart/pie-chart.component';
import { LineChartComponent } from './safe-economy-utils/charts/line-chart/line-chart.component';
import {ChartService} from './safe-economy-utils/charts/chart.service';
import { BarChartComponent } from './safe-economy-utils/charts/bar-chart/bar-chart.component';
import { SinginComponent } from './main-safe-economy/auth/singin/singin.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import { SafeEconomyLandigPageComponent } from './safe-economy-landig-page/safe-economy-landig-page/safe-economy-landig-page.component';
import { LoginComponent } from './main-safe-economy/auth/login/login.component';
import { LoginModalComponent } from './main-safe-economy/auth/login/login-modal/login-modal.component';
import {AuthComponent} from './main-safe-economy/auth/auth.component';
import {LoginModalService} from './main-safe-economy/auth/login/login-modal.service';
import {SafeEconomyLoginService} from './main-safe-economy/auth/login/login-modal/login.service';
import {SafeEconomyAccountService} from './main-safe-economy/auth/login/safe-economy-account.service';
import {SafeEconomyAuthJwtService} from './main-safe-economy/auth/login/safe-economy-auth-jwt.service';
import {SafeEconomyPrincipalService} from './main-safe-economy/auth/login/safe-economy-principal.service';
import {SafeEconomyProfileInfoService} from './safe-economy-utils/safe-economy-profile/safe-economy-profile-info.service';
import { ProfileSafeEconomyComponent } from './main-safe-economy/safe-economy-dashboard/profile-safe-economy/profile-safe-economy.component';
import {AuthGuardService} from './main-safe-economy/safe-economy-main-auth.service';
import { LogoutComponent } from './main-safe-economy/auth/logout/logout.component';
import {SafeEconomyBankAccountsComponent} from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-accounts.component';
import {SafeEconomyBankAccountComponent} from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-bank-account.component';
import {SafeEconomyAddMovementModalService} from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-wallet/safe-economy-add-movement-modal.service';
import { SafeEconomyLandingMainComponent } from './safe-economy-landig-page/safe-economy-landing-main/safe-economy-landing-main.component';
import {SafeEconomyHowItWorkComponent} from './safe-economy-landig-page/safe-economy-how-it-work/safe-economy-how-it-work.component';
import {LandingPageAuthGuardService} from './safe-economy-landig-page/safe-economy-landig-page/landing-page-auth-guard.service';
import {SafeEconomyProfileService} from './main-safe-economy/auth/profile-safe-economy.service';
import {SafeEconomyWalletService} from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-wallet/safe-economy-wallet.service';
import { LoadingComponent } from './safe-economy-utils/loading/loading.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { SafeEconomyPreferencesComponent } from './main-safe-economy/safe-economy-dashboard/safe-economy-preferences/safe-economy-preferences.component';
import { SafeEconomyRegisterBankEntityComponent } from './main-safe-economy/safe-economy-dashboard/safe-economy-preferences/safe-economy-register-bank-entity/safe-economy-register-bank-entity.component';
import {SafeEconomyPreferencesService} from './main-safe-economy/safe-economy-dashboard/safe-economy-preferences/preferences.service';
import {RegisterBankEntityService} from './main-safe-economy/safe-economy-dashboard/safe-economy-preferences/safe-economy-register-bank-entity/register-bank-entity.service';
import { SafeEconomyPaymentsComponent } from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-payments/safe-economy-payments.component';
import {PaymentsService} from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-payments/payments.service';
import { ModalPaymentComponent } from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-payments/modal-payment/modal-payment.component';
import {PaymentsModalService} from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-payments/payments-modal.service';
import {ModalDelComponent} from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-payments/modal-payment/modal-del.component';
import {SafeEconomyAccountModule} from '../account/account.module';
import {ActivationKeyComponent} from './safe-economy-landig-page/activation-key/activation-key.component';
import { ConfirmDeactivateAccountComponent } from './main-safe-economy/safe-economy-dashboard/profile-safe-economy/confirm-deactivate-account/confirm-deactivate-account.component';
import {SafeEconomyConfirmDeactivateService} from './main-safe-economy/safe-economy-dashboard/profile-safe-economy/confirm-deactivate.service';
import { SafeEconomyBankAccountDashboardComponent } from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account-dashboard/safe-economy-bank-account-dashboard.component';
import { BankAccountDetailComponent } from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account-dashboard/bank-account-detail/bank-detail.component';
import { ResetPasswordRequestComponent } from './main-safe-economy/auth/reset-password-request/reset-password-request.component';
import { ResetPasswordFinishComponent } from './main-safe-economy/auth/reset-password-finish/reset-password-finish.component';
import {MoveComponent} from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-wallet/add-wallet-move-modal/move.component';
import {MoveService} from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-wallet/add-wallet-move-modal/move.service';
import {WalletMoveService} from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-wallet/wallet-move.service';
import {WalletMoveComponent} from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-wallet/wallet-move.component';
import {WallettComponent} from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-wallet/wallett.component';
import {BankAccountModalService} from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account-dashboard/bank-account-modal.service';
import {CalendarTestComponent} from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-payments/calendar/calendar-test.component';
import {FullCalendarModule} from 'ng-fullcalendar';
import { ChartsModule } from 'ng2-charts';
import {PreferenciesGuardService} from './main-safe-economy/safe-economy-dashboard/safe-economy-preferences/preferencies-guard.service';
import { PieChartPayComponent } from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-payments/pie-chart-pay/pie-chart-pay.component';
import {HomeService} from './main-safe-economy/safe-economy-dashboard/safe-economy-home/home.service';
import {DetailsService} from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account-dashboard/bank-account-detail/details.service';
import {BankAccountModalComponent} from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account-dashboard/account-modal/bank-account-modal.component';
import {DelAccountComponent} from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account-dashboard/modal-delete/delAccount.component';
import { SafeEconomySavingsComponent } from './main-safe-economy/safe-economy-dashboard/safe-economy-savings/safe-economy-savings.component';
import {SafeEconomySavingService} from './main-safe-economy/safe-economy-dashboard/safe-economy-savings/safe-economy-saving.service';
import {SafeEconomyBankAccountsService} from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/bank-accounts.service';
import {SavingModalService} from './main-safe-economy/safe-economy-dashboard/safe-economy-savings/savings-modal.service';
import { SafeEconomyMovementsComponent } from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-movements/safe-economy-movements.component';
import {SafeEconomyMovementsService} from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-movements/movements.service';
import { BarChartExpensesComponent } from './main-safe-economy/safe-economy-dashboard/safe-economy-home/bar-chart-expenses/bar-chart-expenses.component';
import { BarChartsComponent } from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-movements/bar-charts/bar-charts.component';
import { PieChartsComponent } from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-movements/pie-charts/pie-charts.component';
import {CurrencyService} from './safe-economy-utils/currency/currency.service';
import { BarChartIncomeComponent } from './main-safe-economy/safe-economy-dashboard/safe-economy-home/bar-chart-income/bar-chart-income.component';
import {CreateComponent} from './main-safe-economy/safe-economy-dashboard/safe-economy-savings/create/create.component';
import {AddModalDeleteBankEntityService} from './main-safe-economy/safe-economy-dashboard/safe-economy-preferences/add-modal-delete-bank-entity.service';
import {DeleteComponent} from './main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-wallet/add-wallet-move-modal/delete.component';
import {DeleteModalBankEntityComponent} from './main-safe-economy/safe-economy-dashboard/safe-economy-preferences/safe-economy-register-bank-entity/delete-modal-bank-entity.component';
import { BarChartSavingComponent } from './main-safe-economy/safe-economy-dashboard/safe-economy-home/bar-chart-saving/bar-chart-saving.component';
import {DeleteModalService} from './main-safe-economy/safe-economy-dashboard/safe-economy-savings/delete-modal.service';
import { LogoutConfirmComponent } from './main-safe-economy/auth/logout/logout-confirm/logout-confirm.component';
import {LogoutModalService} from './main-safe-economy/auth/logout/logout-modal.service';
import {DeleteComponentSavingComponent} from './main-safe-economy/safe-economy-dashboard/safe-economy-savings/delete/deleteSaving.component';

@NgModule({
    imports: [
        FullCalendarModule,
        BrowserModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        NgbModule.forRoot(),
        SafeEconomyAccountModule,
         ChartsModule,
        ChartsModule,
        // MatSliderModule
    ],
    declarations: [
        MainSafeEconomyComponent,
        NavbarSafeEconomyComponent,
        DelAccountComponent,
        SafeEconomyDashboardComponent,
        SafeEconomyHomeComponent,
        PieChartComponent,
        LineChartComponent,
        BarChartComponent,
        DeleteComponent,
        SinginComponent,
        LoginComponent,
        SafeEconomyLandigPageComponent,
        LoginModalComponent,
        AuthComponent,
        CalendarTestComponent,
        ProfileSafeEconomyComponent,
        LogoutComponent,
        SafeEconomyBankAccountsComponent,
        SafeEconomyBankAccountComponent,
        WalletMoveComponent,
        MoveComponent,
        DelAccountComponent,
        SafeEconomyHowItWorkComponent,
        LoadingComponent,
        SafeEconomyLandingMainComponent,
        SafeEconomyPreferencesComponent,
        SafeEconomyRegisterBankEntityComponent,
        SafeEconomyPaymentsComponent,
        ModalDelComponent,
        ModalPaymentComponent,
        ActivationKeyComponent,
        ConfirmDeactivateAccountComponent,
        SafeEconomyBankAccountDashboardComponent,
        BankAccountDetailComponent,
        ResetPasswordRequestComponent,
        ResetPasswordFinishComponent,
        MoveComponent,
        WallettComponent,
        BankAccountModalComponent,
        PieChartPayComponent,
        DelAccountComponent,
        DeleteComponent,
        DeleteComponentSavingComponent,
        SafeEconomySavingsComponent,
        SafeEconomyMovementsComponent,
        SafeEconomyMovementsComponent,
        CreateComponent,
        BarChartExpensesComponent,
        BarChartsComponent,
        PieChartsComponent,
        DeleteComponent,
        LogoutConfirmComponent,
        DeleteModalBankEntityComponent,
        BarChartSavingComponent
    ],
    entryComponents: [
        LoginModalComponent,
        MoveComponent,
        DelAccountComponent,
        ModalPaymentComponent,
        ModalDelComponent,
        DelAccountComponent,
        DelAccountComponent,
        DeleteComponent,
        DeleteComponentSavingComponent,
        ConfirmDeactivateAccountComponent,
        ConfirmDeactivateAccountComponent,
        WalletMoveComponent,
        MoveComponent,
        DeleteComponent,
        BankAccountModalComponent,
        CreateComponent,
        DeleteModalBankEntityComponent,
        DeleteComponent,
        // DeleteComponent,
        LogoutConfirmComponent
    ],
    providers: [
        PreferenciesGuardService,
        LoginModalService,
        ChartService,
        SafeEconomyLoginService,
        SafeEconomyAccountService,
        SafeEconomyAuthJwtService,
        SafeEconomyPrincipalService,
        SafeEconomyProfileInfoService,
        AuthGuardService,
        SafeEconomyAddMovementModalService,
        MoveService,
        DetailsService,
        SafeEconomyMovementsService,
        SafeEconomyProfileService,
        LandingPageAuthGuardService,
        SafeEconomyWalletService,
        WalletMoveService,
        SafeEconomyPreferencesService,
        SafeEconomyConfirmDeactivateService,
        RegisterBankEntityService,
        PaymentsService,
        MoveService,
        WalletMoveService,
        PaymentsModalService,
        SafeEconomyAddMovementModalService,
        SafeEconomyWalletService,
        SafeEconomyConfirmDeactivateService,
        BankAccountModalService,
        HomeService,
        SafeEconomySavingService,
        SafeEconomyBankAccountsService,
        SavingModalService,
        CurrencyService,
        AddModalDeleteBankEntityService,
        LogoutModalService,
        DeleteModalService
    ]
})
export class MainSafeEconomyModule {}
