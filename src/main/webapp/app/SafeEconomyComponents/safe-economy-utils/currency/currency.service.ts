import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Currency} from './currency';
import 'rxjs/add/operator/map';

export type EntityResponseType = HttpResponse<Currency>;

@Injectable()
export class CurrencyService {
    // todo agregar esta url en app.constants.
    private static currencyUrl = 'https://free.currencyconverterapi.com/api/v5/convert?q=USD_CRC&compact=y';
    private static convertItemFromServer(currency: Currency): Currency {
        return Object.assign({}, currency);
    }
    private static convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Currency = CurrencyService.convertItemFromServer(res.body);
        return res.clone({body});
    }

    constructor(private http: HttpClient) {
    }

    getCurrencyChangeFromColonsToDollas() {
        return this.http.get<Currency>(CurrencyService.currencyUrl, {observe: 'response'})
            .map((res: EntityResponseType) => CurrencyService.convertResponse(res));
    }
}
