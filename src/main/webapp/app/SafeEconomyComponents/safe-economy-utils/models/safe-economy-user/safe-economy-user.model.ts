import {Account} from '../../../../shared/user/account.model';

export class SafeEconomyUserModel {
    constructor(
        id: string,
        fullName: string,
        phone: string,
        email: string,
        token: string,
        lastVisitDate: Date,
        image: string,
        user: Account
    ) { }
}
