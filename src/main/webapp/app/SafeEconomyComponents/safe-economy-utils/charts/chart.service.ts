import { Injectable } from '@angular/core';

@Injectable()
export class ChartService {
    COLORS = [
      '#4286f4',
      '#e5a424',
      '#45c677',
      '#124391',
      '#38d13b',
      '#7410d1',
      '#1fc1a3',
      '#34a9d1',
      '#e57e24',
      '#d3230c',
      '#98e524',
      '#e5e124',
      '#d10fd1',
      '#0f28d1'
    ];
    COLOR_BAR_POSITIVE = [
      '#41a3db',
      '#75b4d8'
    ];
    COLOR_BAR_NEGATIVE = [
        '#d84141',
        '#d87575'
    ];

    constructor() { }

    getColor( limit ) {
      return this.COLORS.slice(0, limit);
    }
    getTestingValue(limit) {
      const values = [];
      for ( let i = 0; i < limit; i++ ) {
          values[i] = this.randomScalingFactor();
      }
      return values;
    }
    randomScalingFactor() {
      return Math.round(Math.random() * 100);
    }

}
