import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {ChartService} from '../chart.service';
import {Chart} from 'chart.js';
import {WalletMoveService} from '../../../main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-wallet/wallet-move.service';

@Component({
    selector: 'jhi-bar-chart',
    templateUrl: './bar-chart.component.html',
    styles: []
})
export class BarChartComponent implements OnInit {
    @Input() array;
    @Input() type;
    @ViewChild('chart') pieChart: ElementRef;
    chart = [];

    constructor(private chartService: ChartService,
                private walletService: WalletMoveService) {
    }

    ngOnInit() {
        this.buildBarCharts();
    }

    buildBarCharts() {
        this.walletService.currentTable.subscribe((temp) => {
            temp = temp.filter((item) => item.kindOfMovement === 'Gasto');
            temp ? this.array = temp : this.array = [];
            const values = this.array.map((item) => Math.abs(item.acreditationAmmount));
            const config = {
                type: 'bar',
                data: {
                    labels: this.array.map((item) => item.description),
                    datasets: [{
                        label: this.type,
                        borderColor: this.chartService.COLOR_BAR_NEGATIVE[0],
                        backgroundColor: this.chartService.COLOR_BAR_NEGATIVE[0],
                        borderWith: 2,
                        data: values
                    }]
                }
            };
            this.chart = new Chart(this.pieChart.nativeElement.getContext('2d'), config);
        });
    }

}
