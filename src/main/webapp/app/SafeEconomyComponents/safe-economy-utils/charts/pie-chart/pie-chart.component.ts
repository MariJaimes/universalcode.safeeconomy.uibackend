import {Component, OnInit, ViewChild, ElementRef, Input} from '@angular/core';
import { Chart } from 'chart.js';
import {ChartService} from '../chart.service';
import {WalletMoveService} from '../../../main-safe-economy/safe-economy-dashboard/safe-economy-bank-accounts/safe-economy-bank-account/safe-economy-wallet/wallet-move.service';

@Component({
  selector: 'jhi-pie-chart',
  templateUrl: './pie-chart.component.html',
  styles: []
})
export class PieChartComponent implements OnInit {
    @Input() array;
    @Input() type;
    @ViewChild('chart') pieChart: ElementRef;
    chart = [];
    config;

    constructor(private chartsService: ChartService,
                private walletService: WalletMoveService) { }

    ngOnInit() {
        this.buildPieChart();
    }

    buildPieChart() {
        this.config = null;
        this.walletService.currentTable.subscribe((temp) => {
            temp = temp.filter((item) => item.kindOfMovement === 'Ingreso');
            temp ? this.array = temp : this.array = [];
                if (this.array.length === 0) {
                    this.config = this.arrayOutData();
                }else {
                    this.config = this.arrayWithData();
                }
        this.chart = new Chart(this.pieChart.nativeElement.getContext('2d'), this.config);
        });
    }

    arrayOutData() {
        const values = this.chartsService.getTestingValue(1);
        const colors = this.chartsService.getColor(values.length);
        return this.config = {
            type: 'pie',
            data: {
                datasets: [{
                    data: values,
                    backgroundColor: colors,
                    label: 'Dataset 1'
                }],
                labels: [
                    'Ingresos',
                ]
            },
            options: {
                responsive: true
            }
        };
    }
    arrayWithData() {
        const values = this.array.map((item) => item.acreditationAmmount);
        const colors = this.chartsService.getColor(values.length);
        return this.config = {
            type: 'pie',
            data: {
                datasets: [{
                    data: values,
                    backgroundColor: colors,
                    label: this.array.map((item) => item.description)
                }],
                labels: this.array.map((item) => item.description)
            },
            options: {
                responsive: true
            }
        };
    }
}
