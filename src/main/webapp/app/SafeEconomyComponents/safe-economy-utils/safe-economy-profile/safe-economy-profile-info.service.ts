import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {SERVER_API_URL} from '../../../app.constants';
import {SafeEconomyProfileInfoModel} from './safe-economy-profile-info.model';

@Injectable()
export class SafeEconomyProfileInfoService {

    private profileInfoUrl = SERVER_API_URL + 'api/profile-info';
    private profileInfo: Promise<SafeEconomyProfileInfoModel>;

    constructor( private http: HttpClient ) { }
    getProfileInfo(): Promise<SafeEconomyProfileInfoModel> {
        if (!this.profileInfo) {
            this.profileInfo = this.http.get<SafeEconomyProfileInfoModel>(this.profileInfoUrl, { observe: 'response' })
                .map((res: HttpResponse<SafeEconomyProfileInfoModel>) => {
                    const data = res.body;
                    const pi = new SafeEconomyProfileInfoModel();
                    pi.activeProfiles = data.activeProfiles;
                    pi.ribbonEnv = data.ribbonEnv;
                    pi.inProduction = data.activeProfiles.includes('prod') ;
                    pi.swaggerEnabled = data.activeProfiles.includes('swagger');
                    return pi;
                }).toPromise();
        }
        return this.profileInfo;
    }
}
