export class SafeEconomyProfileInfoModel {
    activeProfiles: string[];
    ribbonEnv: string;
    inProduction: boolean;
    swaggerEnabled: boolean;
}
