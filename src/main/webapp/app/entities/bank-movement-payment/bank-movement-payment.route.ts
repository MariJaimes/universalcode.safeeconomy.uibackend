import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { BankMovementPaymentComponent } from './bank-movement-payment.component';
import { BankMovementPaymentDetailComponent } from './bank-movement-payment-detail.component';
import { BankMovementPaymentPopupComponent } from './bank-movement-payment-dialog.component';
import { BankMovementPaymentDeletePopupComponent } from './bank-movement-payment-delete-dialog.component';

@Injectable()
export class BankMovementPaymentResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const bankMovementPaymentRoute: Routes = [
    {
        path: 'cms/bank-movement-payment',
        component: BankMovementPaymentComponent,
        resolve: {
            'pagingParams': BankMovementPaymentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'BankMovementPayments'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'cms/bank-movement-payment/:id',
        component: BankMovementPaymentDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'BankMovementPayments'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const bankMovementPaymentPopupRoute: Routes = [
    {
        path: 'bank-movement-payment-new',
        component: BankMovementPaymentPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'BankMovementPayments'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'bank-movement-payment/:id/edit',
        component: BankMovementPaymentPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'BankMovementPayments'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'bank-movement-payment/:id/delete',
        component: BankMovementPaymentDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'BankMovementPayments'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
