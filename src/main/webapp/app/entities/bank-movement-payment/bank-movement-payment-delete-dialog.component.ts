import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { BankMovementPayment } from './bank-movement-payment.model';
import { BankMovementPaymentPopupService } from './bank-movement-payment-popup.service';
import { BankMovementPaymentService } from './bank-movement-payment.service';

@Component({
    selector: 'jhi-bank-movement-payment-delete-dialog',
    templateUrl: './bank-movement-payment-delete-dialog.component.html'
})
export class BankMovementPaymentDeleteDialogComponent {

    bankMovementPayment: BankMovementPayment;

    constructor(
        private bankMovementPaymentService: BankMovementPaymentService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.bankMovementPaymentService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'bankMovementPaymentListModification',
                content: 'Deleted an bankMovementPayment'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-bank-movement-payment-delete-popup',
    template: ''
})
export class BankMovementPaymentDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private bankMovementPaymentPopupService: BankMovementPaymentPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.bankMovementPaymentPopupService
                .open(BankMovementPaymentDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
