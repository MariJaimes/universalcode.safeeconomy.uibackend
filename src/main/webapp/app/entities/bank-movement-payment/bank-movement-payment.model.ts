import { BaseEntity } from './../../shared';

export class BankMovementPayment implements BaseEntity {
    constructor(
        public id?: number,
        public kindOfMovement?: string,
        public dateOfMovement?: any,
        public description?: string,
        public acreditationAmmount?: number,
        public isPaid?: boolean,
        public comment?: string,
        public priority?: number,
        public userSaveEconomyId?: number,
        public bankAccountId?: number,
    ) {
        this.isPaid = false;
    }
}
