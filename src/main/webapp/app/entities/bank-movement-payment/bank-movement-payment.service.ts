import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { BankMovementPayment } from './bank-movement-payment.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<BankMovementPayment>;

@Injectable()
export class BankMovementPaymentService {

    private resourceUrl =  SERVER_API_URL + 'api/bank-movement-payments';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(bankMovementPayment: BankMovementPayment): Observable<EntityResponseType> {
        const copy = this.convert(bankMovementPayment);
        return this.http.post<BankMovementPayment>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(bankMovementPayment: BankMovementPayment): Observable<EntityResponseType> {
        const copy = this.convert(bankMovementPayment);
        return this.http.put<BankMovementPayment>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<BankMovementPayment>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<BankMovementPayment[]>> {
        const options = createRequestOption(req);
        return this.http.get<BankMovementPayment[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<BankMovementPayment[]>) => this.convertArrayResponse(res));
    }

    // findAllMovements(bankAccount){
    //
    // }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${SERVER_API_URL}api/bank-movement-payments/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: BankMovementPayment = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<BankMovementPayment[]>): HttpResponse<BankMovementPayment[]> {
        const jsonResponse: BankMovementPayment[] = res.body;
        const body: BankMovementPayment[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to BankMovementPayment.
     */
    private convertItemFromServer(bankMovementPayment: BankMovementPayment): BankMovementPayment {
        const copy: BankMovementPayment = Object.assign({}, bankMovementPayment);
        // copy.dateOfMovement = this.dateUtils
        //     .convertLocalDateFromServer(bankMovementPayment.dateOfMovement);
        return copy;
    }

    /**
     * Convert a BankMovementPayment to a JSON which can be sent to the server.
     */
    private convert(bankMovementPayment: BankMovementPayment): BankMovementPayment {
        const copy: BankMovementPayment = Object.assign({}, bankMovementPayment);

        copy.dateOfMovement = this.dateUtils.convertLocalDateFromServer(bankMovementPayment.dateOfMovement);
        return copy;
    }

    findAllMovementByAccount(id: number): Observable<EntityResponseType> {
        return this.http.get<BankMovementPayment>(`${SERVER_API_URL}api/bank-movements-by-bankAccount/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    findAllMovesByUser(id: number): Observable<EntityResponseType> {
        return this.http.get<BankMovementPayment>(`${SERVER_API_URL}api/bank-movements-by-userSaveEconomy/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    findAllMovesByUserAndActive(id: number): Observable<EntityResponseType> {
        return this.http.get<BankMovementPayment>(`${SERVER_API_URL}api/bank-movements-by-userSaveEconomyAndActive/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }
}
