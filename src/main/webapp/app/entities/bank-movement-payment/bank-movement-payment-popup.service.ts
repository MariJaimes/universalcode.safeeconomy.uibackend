import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { BankMovementPayment } from './bank-movement-payment.model';
import { BankMovementPaymentService } from './bank-movement-payment.service';

@Injectable()
export class BankMovementPaymentPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private bankMovementPaymentService: BankMovementPaymentService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.bankMovementPaymentService.find(id)
                    .subscribe((bankMovementPaymentResponse: HttpResponse<BankMovementPayment>) => {
                        const bankMovementPayment: BankMovementPayment = bankMovementPaymentResponse.body;
                        bankMovementPayment.dateOfMovement = this.datePipe
                            .transform(bankMovementPayment.dateOfMovement, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.bankMovementPaymentModalRef(component, bankMovementPayment);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.bankMovementPaymentModalRef(component, new BankMovementPayment());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    bankMovementPaymentModalRef(component: Component, bankMovementPayment: BankMovementPayment): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.bankMovementPayment = bankMovementPayment;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
