import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SafeEconomySharedModule } from '../../shared';
import {
    BankMovementPaymentService,
    BankMovementPaymentPopupService,
    BankMovementPaymentComponent,
    BankMovementPaymentDetailComponent,
    BankMovementPaymentDialogComponent,
    BankMovementPaymentPopupComponent,
    BankMovementPaymentDeletePopupComponent,
    BankMovementPaymentDeleteDialogComponent,
    bankMovementPaymentRoute,
    bankMovementPaymentPopupRoute,
    BankMovementPaymentResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...bankMovementPaymentRoute,
    ...bankMovementPaymentPopupRoute,
];

@NgModule({
    imports: [
        SafeEconomySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        BankMovementPaymentComponent,
        BankMovementPaymentDetailComponent,
        BankMovementPaymentDialogComponent,
        BankMovementPaymentDeleteDialogComponent,
        BankMovementPaymentPopupComponent,
        BankMovementPaymentDeletePopupComponent,
    ],
    entryComponents: [
        BankMovementPaymentComponent,
        BankMovementPaymentDialogComponent,
        BankMovementPaymentPopupComponent,
        BankMovementPaymentDeleteDialogComponent,
        BankMovementPaymentDeletePopupComponent,
    ],
    providers: [
        BankMovementPaymentService,
        BankMovementPaymentPopupService,
        BankMovementPaymentResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SafeEconomyBankMovementPaymentModule {}
