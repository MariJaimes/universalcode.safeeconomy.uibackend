import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { BankMovementPayment } from './bank-movement-payment.model';
import { BankMovementPaymentService } from './bank-movement-payment.service';

@Component({
    selector: 'jhi-bank-movement-payment-detail',
    templateUrl: './bank-movement-payment-detail.component.html'
})
export class BankMovementPaymentDetailComponent implements OnInit, OnDestroy {

    bankMovementPayment: BankMovementPayment;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private bankMovementPaymentService: BankMovementPaymentService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInBankMovementPayments();
    }

    load(id) {
        this.bankMovementPaymentService.find(id)
            .subscribe((bankMovementPaymentResponse: HttpResponse<BankMovementPayment>) => {
                this.bankMovementPayment = bankMovementPaymentResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInBankMovementPayments() {
        this.eventSubscriber = this.eventManager.subscribe(
            'bankMovementPaymentListModification',
            (response) => this.load(this.bankMovementPayment.id)
        );
    }
}
