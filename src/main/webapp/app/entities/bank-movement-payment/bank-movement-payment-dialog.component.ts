import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { BankMovementPayment } from './bank-movement-payment.model';
import { BankMovementPaymentPopupService } from './bank-movement-payment-popup.service';
import { BankMovementPaymentService } from './bank-movement-payment.service';
import { UserSaveEconomy, UserSaveEconomyService } from '../user-save-economy';
import { BankAccount, BankAccountService } from '../bank-account';

@Component({
    selector: 'jhi-bank-movement-payment-dialog',
    templateUrl: './bank-movement-payment-dialog.component.html'
})
export class BankMovementPaymentDialogComponent implements OnInit {

    bankMovementPayment: BankMovementPayment;
    isSaving: boolean;

    usersaveeconomies: UserSaveEconomy[];

    bankaccounts: BankAccount[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private bankMovementPaymentService: BankMovementPaymentService,
        private userSaveEconomyService: UserSaveEconomyService,
        private bankAccountService: BankAccountService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.userSaveEconomyService.query()
            .subscribe((res: HttpResponse<UserSaveEconomy[]>) => { this.usersaveeconomies = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.bankAccountService.query()
            .subscribe((res: HttpResponse<BankAccount[]>) => { this.bankaccounts = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.bankMovementPayment.id !== undefined) {
            this.subscribeToSaveResponse(
                this.bankMovementPaymentService.update(this.bankMovementPayment));
        } else {
            this.subscribeToSaveResponse(
                this.bankMovementPaymentService.create(this.bankMovementPayment));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<BankMovementPayment>>) {
        result.subscribe((res: HttpResponse<BankMovementPayment>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: BankMovementPayment) {
        this.eventManager.broadcast({ name: 'bankMovementPaymentListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackUserSaveEconomyById(index: number, item: UserSaveEconomy) {
        return item.id;
    }

    trackBankAccountById(index: number, item: BankAccount) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-bank-movement-payment-popup',
    template: ''
})
export class BankMovementPaymentPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private bankMovementPaymentPopupService: BankMovementPaymentPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.bankMovementPaymentPopupService
                    .open(BankMovementPaymentDialogComponent as Component, params['id']);
            } else {
                this.bankMovementPaymentPopupService
                    .open(BankMovementPaymentDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
