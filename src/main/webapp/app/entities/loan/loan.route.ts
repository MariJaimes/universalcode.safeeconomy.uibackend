import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { LoanComponent } from './loan.component';
import { LoanDetailComponent } from './loan-detail.component';
import { LoanPopupComponent } from './loan-dialog.component';
import { LoanDeletePopupComponent } from './loan-delete-dialog.component';

@Injectable()
export class LoanResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const loanRoute: Routes = [
    {
        path: 'cms/loan',
        component: LoanComponent,
        resolve: {
            'pagingParams': LoanResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Loans'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'cms/loan/:id',
        component: LoanDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Loans'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const loanPopupRoute: Routes = [
    {
        path: 'loan-new',
        component: LoanPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Loans'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'loan/:id/edit',
        component: LoanPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Loans'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'loan/:id/delete',
        component: LoanDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Loans'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
