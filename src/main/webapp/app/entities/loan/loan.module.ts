import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SafeEconomySharedModule } from '../../shared';
import {
    LoanService,
    LoanPopupService,
    LoanComponent,
    LoanDetailComponent,
    LoanDialogComponent,
    LoanPopupComponent,
    LoanDeletePopupComponent,
    LoanDeleteDialogComponent,
    loanRoute,
    loanPopupRoute,
    LoanResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...loanRoute,
    ...loanPopupRoute,
];

@NgModule({
    imports: [
        SafeEconomySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        LoanComponent,
        LoanDetailComponent,
        LoanDialogComponent,
        LoanDeleteDialogComponent,
        LoanPopupComponent,
        LoanDeletePopupComponent,
    ],
    entryComponents: [
        LoanComponent,
        LoanDialogComponent,
        LoanPopupComponent,
        LoanDeleteDialogComponent,
        LoanDeletePopupComponent,
    ],
    providers: [
        LoanService,
        LoanPopupService,
        LoanResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SafeEconomyLoanModule {}
