import { BaseEntity } from './../../shared';

export class Loan implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public rate?: string,
        public isMonthly?: boolean,
        public isPaid?: boolean,
        public finishDate?: any,
        public userSaveEconomyId?: number,
    ) {
        this.isMonthly = false;
        this.isPaid = false;
    }
}
