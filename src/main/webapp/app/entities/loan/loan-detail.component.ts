import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Loan } from './loan.model';
import { LoanService } from './loan.service';

@Component({
    selector: 'jhi-loan-detail',
    templateUrl: './loan-detail.component.html'
})
export class LoanDetailComponent implements OnInit, OnDestroy {

    loan: Loan;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private loanService: LoanService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInLoans();
    }

    load(id) {
        this.loanService.find(id)
            .subscribe((loanResponse: HttpResponse<Loan>) => {
                this.loan = loanResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInLoans() {
        this.eventSubscriber = this.eventManager.subscribe(
            'loanListModification',
            (response) => this.load(this.loan.id)
        );
    }
}
