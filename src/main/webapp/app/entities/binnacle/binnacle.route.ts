import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { BinnacleComponent } from './binnacle.component';
import { BinnacleDetailComponent } from './binnacle-detail.component';
import { BinnaclePopupComponent } from './binnacle-dialog.component';
import { BinnacleDeletePopupComponent } from './binnacle-delete-dialog.component';

@Injectable()
export class BinnacleResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const binnacleRoute: Routes = [
    {
        path: 'cms/binnacle',
        component: BinnacleComponent,
        resolve: {
            'pagingParams': BinnacleResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Binnacles'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'cms/binnacle/:id',
        component: BinnacleDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Binnacles'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const binnaclePopupRoute: Routes = [
    {
        path: 'binnacle-new',
        component: BinnaclePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Binnacles'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'binnacle/:id/edit',
        component: BinnaclePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Binnacles'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'binnacle/:id/delete',
        component: BinnacleDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Binnacles'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
