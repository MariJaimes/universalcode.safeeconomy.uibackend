import { BaseEntity } from './../../shared';

export class Binnacle implements BaseEntity {
    constructor(
        public id?: number,
        public event?: string,
        public user?: string,
        public date?: any,
    ) {
    }
}
