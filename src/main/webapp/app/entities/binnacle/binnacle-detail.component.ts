import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Binnacle } from './binnacle.model';
import { BinnacleService } from './binnacle.service';

@Component({
    selector: 'jhi-binnacle-detail',
    templateUrl: './binnacle-detail.component.html'
})
export class BinnacleDetailComponent implements OnInit, OnDestroy {

    binnacle: Binnacle;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private binnacleService: BinnacleService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInBinnacles();
    }

    load(id) {
        this.binnacleService.find(id)
            .subscribe((binnacleResponse: HttpResponse<Binnacle>) => {
                this.binnacle = binnacleResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInBinnacles() {
        this.eventSubscriber = this.eventManager.subscribe(
            'binnacleListModification',
            (response) => this.load(this.binnacle.id)
        );
    }
}
