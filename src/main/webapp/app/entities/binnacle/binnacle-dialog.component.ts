import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Binnacle } from './binnacle.model';
import { BinnaclePopupService } from './binnacle-popup.service';
import { BinnacleService } from './binnacle.service';

@Component({
    selector: 'jhi-binnacle-dialog',
    templateUrl: './binnacle-dialog.component.html'
})
export class BinnacleDialogComponent implements OnInit {

    binnacle: Binnacle;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private binnacleService: BinnacleService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.binnacle.id !== undefined) {
            this.subscribeToSaveResponse(
                this.binnacleService.update(this.binnacle));
        } else {
            this.subscribeToSaveResponse(
                this.binnacleService.create(this.binnacle));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Binnacle>>) {
        result.subscribe((res: HttpResponse<Binnacle>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Binnacle) {
        this.eventManager.broadcast({ name: 'binnacleListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-binnacle-popup',
    template: ''
})
export class BinnaclePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private binnaclePopupService: BinnaclePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.binnaclePopupService
                    .open(BinnacleDialogComponent as Component, params['id']);
            } else {
                this.binnaclePopupService
                    .open(BinnacleDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
