import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SafeEconomySharedModule } from '../../shared';
import {
    BinnacleService,
    BinnaclePopupService,
    BinnacleComponent,
    BinnacleDetailComponent,
    BinnacleDialogComponent,
    BinnaclePopupComponent,
    BinnacleDeletePopupComponent,
    BinnacleDeleteDialogComponent,
    binnacleRoute,
    binnaclePopupRoute,
    BinnacleResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...binnacleRoute,
    ...binnaclePopupRoute,
];

@NgModule({
    imports: [
        SafeEconomySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        BinnacleComponent,
        BinnacleDetailComponent,
        BinnacleDialogComponent,
        BinnacleDeleteDialogComponent,
        BinnaclePopupComponent,
        BinnacleDeletePopupComponent,
    ],
    entryComponents: [
        BinnacleComponent,
        BinnacleDialogComponent,
        BinnaclePopupComponent,
        BinnacleDeleteDialogComponent,
        BinnacleDeletePopupComponent,
    ],
    providers: [
        BinnacleService,
        BinnaclePopupService,
        BinnacleResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SafeEconomyBinnacleModule {}
