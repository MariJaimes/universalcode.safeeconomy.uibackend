import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Binnacle } from './binnacle.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Binnacle>;

@Injectable()
export class BinnacleService {

    private resourceUrl =  SERVER_API_URL + 'api/binnacles';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(binnacle: Binnacle): Observable<EntityResponseType> {
        const copy = this.convert(binnacle);
        return this.http.post<Binnacle>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(binnacle: Binnacle): Observable<EntityResponseType> {
        const copy = this.convert(binnacle);
        return this.http.put<Binnacle>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Binnacle>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Binnacle[]>> {
        const options = createRequestOption(req);
        return this.http.get<Binnacle[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Binnacle[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Binnacle = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Binnacle[]>): HttpResponse<Binnacle[]> {
        const jsonResponse: Binnacle[] = res.body;
        const body: Binnacle[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Binnacle.
     */
    private convertItemFromServer(binnacle: Binnacle): Binnacle {
        const copy: Binnacle = Object.assign({}, binnacle);
        copy.date = this.dateUtils
            .convertDateTimeFromServer(binnacle.date);
        return copy;
    }

    /**
     * Convert a Binnacle to a JSON which can be sent to the server.
     */
    private convert(binnacle: Binnacle): Binnacle {
        const copy: Binnacle = Object.assign({}, binnacle);

        copy.date = this.dateUtils.toDate(binnacle.date);
        return copy;
    }
}
