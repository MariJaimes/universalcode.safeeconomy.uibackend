import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Binnacle } from './binnacle.model';
import { BinnaclePopupService } from './binnacle-popup.service';
import { BinnacleService } from './binnacle.service';

@Component({
    selector: 'jhi-binnacle-delete-dialog',
    templateUrl: './binnacle-delete-dialog.component.html'
})
export class BinnacleDeleteDialogComponent {

    binnacle: Binnacle;

    constructor(
        private binnacleService: BinnacleService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.binnacleService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'binnacleListModification',
                content: 'Deleted an binnacle'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-binnacle-delete-popup',
    template: ''
})
export class BinnacleDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private binnaclePopupService: BinnaclePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.binnaclePopupService
                .open(BinnacleDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
