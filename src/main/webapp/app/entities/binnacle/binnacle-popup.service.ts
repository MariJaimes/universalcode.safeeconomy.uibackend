import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Binnacle } from './binnacle.model';
import { BinnacleService } from './binnacle.service';

@Injectable()
export class BinnaclePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private binnacleService: BinnacleService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.binnacleService.find(id)
                    .subscribe((binnacleResponse: HttpResponse<Binnacle>) => {
                        const binnacle: Binnacle = binnacleResponse.body;
                        binnacle.date = this.datePipe
                            .transform(binnacle.date, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.binnacleModalRef(component, binnacle);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.binnacleModalRef(component, new Binnacle());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    binnacleModalRef(component: Component, binnacle: Binnacle): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.binnacle = binnacle;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
