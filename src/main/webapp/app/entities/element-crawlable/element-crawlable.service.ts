import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ElementCrawlable } from './element-crawlable.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<ElementCrawlable>;

@Injectable()
export class ElementCrawlableService {

    private resourceUrl =  SERVER_API_URL + 'api/element-crawlables';

    constructor(private http: HttpClient) { }

    create(elementCrawlable: ElementCrawlable): Observable<EntityResponseType> {
        const copy = this.convert(elementCrawlable);
        return this.http.post<ElementCrawlable>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(elementCrawlable: ElementCrawlable): Observable<EntityResponseType> {
        const copy = this.convert(elementCrawlable);
        return this.http.put<ElementCrawlable>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ElementCrawlable>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ElementCrawlable[]>> {
        const options = createRequestOption(req);
        return this.http.get<ElementCrawlable[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ElementCrawlable[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ElementCrawlable = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ElementCrawlable[]>): HttpResponse<ElementCrawlable[]> {
        const jsonResponse: ElementCrawlable[] = res.body;
        const body: ElementCrawlable[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ElementCrawlable.
     */
    private convertItemFromServer(elementCrawlable: ElementCrawlable): ElementCrawlable {
        const copy: ElementCrawlable = Object.assign({}, elementCrawlable);
        return copy;
    }

    /**
     * Convert a ElementCrawlable to a JSON which can be sent to the server.
     */
    private convert(elementCrawlable: ElementCrawlable): ElementCrawlable {
        const copy: ElementCrawlable = Object.assign({}, elementCrawlable);
        return copy;
    }
}
