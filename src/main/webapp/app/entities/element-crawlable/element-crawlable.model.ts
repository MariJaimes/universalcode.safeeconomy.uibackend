import { BaseEntity } from './../../shared';

export class ElementCrawlable implements BaseEntity {
    constructor(
        public id?: number,
        public bankId?: number,
        public userId?: number,
        public url?: string,
        public wasProcessed?: boolean,
    ) {
        this.wasProcessed = false;
    }
}
