import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ElementCrawlable } from './element-crawlable.model';
import { ElementCrawlablePopupService } from './element-crawlable-popup.service';
import { ElementCrawlableService } from './element-crawlable.service';

@Component({
    selector: 'jhi-element-crawlable-dialog',
    templateUrl: './element-crawlable-dialog.component.html'
})
export class ElementCrawlableDialogComponent implements OnInit {

    elementCrawlable: ElementCrawlable;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private elementCrawlableService: ElementCrawlableService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.elementCrawlable.id !== undefined) {
            this.subscribeToSaveResponse(
                this.elementCrawlableService.update(this.elementCrawlable));
        } else {
            this.subscribeToSaveResponse(
                this.elementCrawlableService.create(this.elementCrawlable));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ElementCrawlable>>) {
        result.subscribe((res: HttpResponse<ElementCrawlable>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ElementCrawlable) {
        this.eventManager.broadcast({ name: 'elementCrawlableListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-element-crawlable-popup',
    template: ''
})
export class ElementCrawlablePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private elementCrawlablePopupService: ElementCrawlablePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.elementCrawlablePopupService
                    .open(ElementCrawlableDialogComponent as Component, params['id']);
            } else {
                this.elementCrawlablePopupService
                    .open(ElementCrawlableDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
