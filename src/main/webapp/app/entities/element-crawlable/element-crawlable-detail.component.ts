import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ElementCrawlable } from './element-crawlable.model';
import { ElementCrawlableService } from './element-crawlable.service';

@Component({
    selector: 'jhi-element-crawlable-detail',
    templateUrl: './element-crawlable-detail.component.html'
})
export class ElementCrawlableDetailComponent implements OnInit, OnDestroy {

    elementCrawlable: ElementCrawlable;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private elementCrawlableService: ElementCrawlableService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInElementCrawlables();
    }

    load(id) {
        this.elementCrawlableService.find(id)
            .subscribe((elementCrawlableResponse: HttpResponse<ElementCrawlable>) => {
                this.elementCrawlable = elementCrawlableResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInElementCrawlables() {
        this.eventSubscriber = this.eventManager.subscribe(
            'elementCrawlableListModification',
            (response) => this.load(this.elementCrawlable.id)
        );
    }
}
