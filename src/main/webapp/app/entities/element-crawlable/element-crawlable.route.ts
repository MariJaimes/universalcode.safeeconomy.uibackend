import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ElementCrawlableComponent } from './element-crawlable.component';
import { ElementCrawlableDetailComponent } from './element-crawlable-detail.component';
import { ElementCrawlablePopupComponent } from './element-crawlable-dialog.component';
import { ElementCrawlableDeletePopupComponent } from './element-crawlable-delete-dialog.component';

@Injectable()
export class ElementCrawlableResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const elementCrawlableRoute: Routes = [
    {
        path: 'cms/element-crawlable',
        component: ElementCrawlableComponent,
        resolve: {
            'pagingParams': ElementCrawlableResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ElementCrawlables'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'cms/element-crawlable/:id',
        component: ElementCrawlableDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ElementCrawlables'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const elementCrawlablePopupRoute: Routes = [
    {
        path: 'element-crawlable-new',
        component: ElementCrawlablePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ElementCrawlables'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'element-crawlable/:id/edit',
        component: ElementCrawlablePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ElementCrawlables'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'element-crawlable/:id/delete',
        component: ElementCrawlableDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ElementCrawlables'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
