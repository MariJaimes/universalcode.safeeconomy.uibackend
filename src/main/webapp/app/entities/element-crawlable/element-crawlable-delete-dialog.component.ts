import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ElementCrawlable } from './element-crawlable.model';
import { ElementCrawlablePopupService } from './element-crawlable-popup.service';
import { ElementCrawlableService } from './element-crawlable.service';

@Component({
    selector: 'jhi-element-crawlable-delete-dialog',
    templateUrl: './element-crawlable-delete-dialog.component.html'
})
export class ElementCrawlableDeleteDialogComponent {

    elementCrawlable: ElementCrawlable;

    constructor(
        private elementCrawlableService: ElementCrawlableService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.elementCrawlableService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'elementCrawlableListModification',
                content: 'Deleted an elementCrawlable'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-element-crawlable-delete-popup',
    template: ''
})
export class ElementCrawlableDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private elementCrawlablePopupService: ElementCrawlablePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.elementCrawlablePopupService
                .open(ElementCrawlableDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
