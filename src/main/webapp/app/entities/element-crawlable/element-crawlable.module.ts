import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SafeEconomySharedModule } from '../../shared';
import {
    ElementCrawlableService,
    ElementCrawlablePopupService,
    ElementCrawlableComponent,
    ElementCrawlableDetailComponent,
    ElementCrawlableDialogComponent,
    ElementCrawlablePopupComponent,
    ElementCrawlableDeletePopupComponent,
    ElementCrawlableDeleteDialogComponent,
    elementCrawlableRoute,
    elementCrawlablePopupRoute,
    ElementCrawlableResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...elementCrawlableRoute,
    ...elementCrawlablePopupRoute,
];

@NgModule({
    imports: [
        SafeEconomySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ElementCrawlableComponent,
        ElementCrawlableDetailComponent,
        ElementCrawlableDialogComponent,
        ElementCrawlableDeleteDialogComponent,
        ElementCrawlablePopupComponent,
        ElementCrawlableDeletePopupComponent,
    ],
    entryComponents: [
        ElementCrawlableComponent,
        ElementCrawlableDialogComponent,
        ElementCrawlablePopupComponent,
        ElementCrawlableDeleteDialogComponent,
        ElementCrawlableDeletePopupComponent,
    ],
    providers: [
        ElementCrawlableService,
        ElementCrawlablePopupService,
        ElementCrawlableResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SafeEconomyElementCrawlableModule {}
