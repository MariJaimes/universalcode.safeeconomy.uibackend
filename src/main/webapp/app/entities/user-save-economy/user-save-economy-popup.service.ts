import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { UserSaveEconomy } from './user-save-economy.model';
import { UserSaveEconomyService } from './user-save-economy.service';

@Injectable()
export class UserSaveEconomyPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private userSaveEconomyService: UserSaveEconomyService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.userSaveEconomyService.find(id)
                    .subscribe((userSaveEconomyResponse: HttpResponse<UserSaveEconomy>) => {
                        const userSaveEconomy: UserSaveEconomy = userSaveEconomyResponse.body;
                        userSaveEconomy.lastVisitDate = this.datePipe
                            .transform(userSaveEconomy.lastVisitDate, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.userSaveEconomyModalRef(component, userSaveEconomy);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.userSaveEconomyModalRef(component, new UserSaveEconomy());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    userSaveEconomyModalRef(component: Component, userSaveEconomy: UserSaveEconomy): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.userSaveEconomy = userSaveEconomy;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
