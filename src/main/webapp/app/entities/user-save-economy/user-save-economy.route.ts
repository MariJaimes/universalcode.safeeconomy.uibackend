import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { UserSaveEconomyComponent } from './user-save-economy.component';
import { UserSaveEconomyDetailComponent } from './user-save-economy-detail.component';
import { UserSaveEconomyPopupComponent } from './user-save-economy-dialog.component';
import { UserSaveEconomyDeletePopupComponent } from './user-save-economy-delete-dialog.component';

@Injectable()
export class UserSaveEconomyResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const userSaveEconomyRoute: Routes = [
    {
        path: 'cms/user-save-economy',
        component: UserSaveEconomyComponent,
        resolve: {
            'pagingParams': UserSaveEconomyResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'UserSaveEconomies'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'cms/user-save-economy/:id',
        component: UserSaveEconomyDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'UserSaveEconomies'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const userSaveEconomyPopupRoute: Routes = [
    {
        path: 'user-save-economy-new',
        component: UserSaveEconomyPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'UserSaveEconomies'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'user-save-economy/:id/edit',
        component: UserSaveEconomyPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'UserSaveEconomies'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'user-save-economy/:id/delete',
        component: UserSaveEconomyDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'UserSaveEconomies'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
