import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SafeEconomySharedModule } from '../../shared';
import { SafeEconomyAdminModule } from '../../admin/admin.module';
import {
    UserSaveEconomyService,
    UserSaveEconomyPopupService,
    UserSaveEconomyComponent,
    UserSaveEconomyDetailComponent,
    UserSaveEconomyDialogComponent,
    UserSaveEconomyPopupComponent,
    UserSaveEconomyDeletePopupComponent,
    UserSaveEconomyDeleteDialogComponent,
    userSaveEconomyRoute,
    userSaveEconomyPopupRoute,
    UserSaveEconomyResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...userSaveEconomyRoute,
    ...userSaveEconomyPopupRoute,
];

@NgModule({
    imports: [
        SafeEconomySharedModule,
        SafeEconomyAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        UserSaveEconomyComponent,
        UserSaveEconomyDetailComponent,
        UserSaveEconomyDialogComponent,
        UserSaveEconomyDeleteDialogComponent,
        UserSaveEconomyPopupComponent,
        UserSaveEconomyDeletePopupComponent,
    ],
    entryComponents: [
        UserSaveEconomyComponent,
        UserSaveEconomyDialogComponent,
        UserSaveEconomyPopupComponent,
        UserSaveEconomyDeleteDialogComponent,
        UserSaveEconomyDeletePopupComponent,
    ],
    providers: [
        UserSaveEconomyService,
        UserSaveEconomyPopupService,
        UserSaveEconomyResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SafeEconomyUserSaveEconomyModule {}
