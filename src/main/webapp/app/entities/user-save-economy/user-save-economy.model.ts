import { BaseEntity } from './../../shared';

export class UserSaveEconomy implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public lastName?: string,
        public telephone?: string,
        public token?: string,
        public lastVisitDate?: any,
        public image?: string,
        public fact?: string,
        public userId?: number,
        public bankAccounts?: BaseEntity[],
        public paumenths?: BaseEntity[],
        public loans?: BaseEntity[],
        public savings?: BaseEntity[],
    ) {
    }
}
