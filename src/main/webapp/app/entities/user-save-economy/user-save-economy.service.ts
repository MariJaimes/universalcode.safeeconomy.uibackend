import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { UserSaveEconomy } from './user-save-economy.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<UserSaveEconomy>;

@Injectable()
export class UserSaveEconomyService {

    private resourceUrl =  SERVER_API_URL + 'api/user-save-economies';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(userSaveEconomy: UserSaveEconomy): Observable<EntityResponseType> {
        const copy = this.convert(userSaveEconomy);
        return this.http.post<UserSaveEconomy>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(userSaveEconomy: UserSaveEconomy): Observable<EntityResponseType> {
        const copy = this.convert(userSaveEconomy);
        return this.http.put<UserSaveEconomy>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<UserSaveEconomy>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<UserSaveEconomy[]>> {
        const options = createRequestOption(req);
        return this.http.get<UserSaveEconomy[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<UserSaveEconomy[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: UserSaveEconomy = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<UserSaveEconomy[]>): HttpResponse<UserSaveEconomy[]> {
        const jsonResponse: UserSaveEconomy[] = res.body;
        const body: UserSaveEconomy[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to UserSaveEconomy.
     */
    private convertItemFromServer(userSaveEconomy: UserSaveEconomy): UserSaveEconomy {
        const copy: UserSaveEconomy = Object.assign({}, userSaveEconomy);
        copy.lastVisitDate = this.dateUtils
            .convertDateTimeFromServer(userSaveEconomy.lastVisitDate);
        return copy;
    }

    /**
     * Convert a UserSaveEconomy to a JSON which can be sent to the server.
     */
    private convert(userSaveEconomy: UserSaveEconomy): UserSaveEconomy {
        const copy: UserSaveEconomy = Object.assign({}, userSaveEconomy);

        copy.lastVisitDate = this.dateUtils.toDate(userSaveEconomy.lastVisitDate);
        return copy;
    }
    public getSafeeconommyLoggedUser(id) {
        return this.http.get<UserSaveEconomy>(SERVER_API_URL + 'api/get-safe-economy-user-by-jhi-user/' + id,
            { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    public deactivateUser(account) {
        return this.http.get<UserSaveEconomy> (SERVER_API_URL + 'api/deactivate/' + account.id,
            { observe: 'response'})
            .map((res: EntityResponseType) => { });
    }
    public getSafeEconomyUserbyLogin(login) {
        return this.http.get<UserSaveEconomy>(SERVER_API_URL + 'api/get-safe-economy-user-by-jhi-login/' + login,
            { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

}
