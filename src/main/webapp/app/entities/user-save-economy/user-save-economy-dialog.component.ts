import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { UserSaveEconomy } from './user-save-economy.model';
import { UserSaveEconomyPopupService } from './user-save-economy-popup.service';
import { UserSaveEconomyService } from './user-save-economy.service';
import { User, UserService } from '../../shared';

@Component({
    selector: 'jhi-user-save-economy-dialog',
    templateUrl: './user-save-economy-dialog.component.html'
})
export class UserSaveEconomyDialogComponent implements OnInit {

    userSaveEconomy: UserSaveEconomy;
    isSaving: boolean;

    users: User[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private userSaveEconomyService: UserSaveEconomyService,
        private userService: UserService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.userService.query()
            .subscribe((res: HttpResponse<User[]>) => { this.users = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.userSaveEconomy.id !== undefined) {
            this.subscribeToSaveResponse(
                this.userSaveEconomyService.update(this.userSaveEconomy));
        } else {
            this.subscribeToSaveResponse(
                this.userSaveEconomyService.create(this.userSaveEconomy));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<UserSaveEconomy>>) {
        result.subscribe((res: HttpResponse<UserSaveEconomy>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: UserSaveEconomy) {
        this.eventManager.broadcast({ name: 'userSaveEconomyListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-user-save-economy-popup',
    template: ''
})
export class UserSaveEconomyPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private userSaveEconomyPopupService: UserSaveEconomyPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.userSaveEconomyPopupService
                    .open(UserSaveEconomyDialogComponent as Component, params['id']);
            } else {
                this.userSaveEconomyPopupService
                    .open(UserSaveEconomyDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
