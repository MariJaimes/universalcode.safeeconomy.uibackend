import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { UserSaveEconomy } from './user-save-economy.model';
import { UserSaveEconomyService } from './user-save-economy.service';

@Component({
    selector: 'jhi-user-save-economy-detail',
    templateUrl: './user-save-economy-detail.component.html'
})
export class UserSaveEconomyDetailComponent implements OnInit, OnDestroy {

    userSaveEconomy: UserSaveEconomy;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private userSaveEconomyService: UserSaveEconomyService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInUserSaveEconomies();
    }

    load(id) {
        this.userSaveEconomyService.find(id)
            .subscribe((userSaveEconomyResponse: HttpResponse<UserSaveEconomy>) => {
                this.userSaveEconomy = userSaveEconomyResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInUserSaveEconomies() {
        this.eventSubscriber = this.eventManager.subscribe(
            'userSaveEconomyListModification',
            (response) => this.load(this.userSaveEconomy.id)
        );
    }
}
