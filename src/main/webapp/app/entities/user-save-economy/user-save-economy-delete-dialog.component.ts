import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { UserSaveEconomy } from './user-save-economy.model';
import { UserSaveEconomyPopupService } from './user-save-economy-popup.service';
import { UserSaveEconomyService } from './user-save-economy.service';

@Component({
    selector: 'jhi-user-save-economy-delete-dialog',
    templateUrl: './user-save-economy-delete-dialog.component.html'
})
export class UserSaveEconomyDeleteDialogComponent {

    userSaveEconomy: UserSaveEconomy;

    constructor(
        private userSaveEconomyService: UserSaveEconomyService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.userSaveEconomyService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'userSaveEconomyListModification',
                content: 'Deleted an userSaveEconomy'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-user-save-economy-delete-popup',
    template: ''
})
export class UserSaveEconomyDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private userSaveEconomyPopupService: UserSaveEconomyPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.userSaveEconomyPopupService
                .open(UserSaveEconomyDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
