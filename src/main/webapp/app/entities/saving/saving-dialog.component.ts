import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Saving } from './saving.model';
import { SavingPopupService } from './saving-popup.service';
import { SavingService } from './saving.service';
import { UserSaveEconomy, UserSaveEconomyService } from '../user-save-economy';
import { BankAccount, BankAccountService } from '../bank-account';

@Component({
    selector: 'jhi-saving-dialog',
    templateUrl: './saving-dialog.component.html'
})
export class SavingDialogComponent implements OnInit {

    saving: Saving;
    isSaving: boolean;

    usersaveeconomies: UserSaveEconomy[];

    bankaccounts: BankAccount[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private savingService: SavingService,
        private userSaveEconomyService: UserSaveEconomyService,
        private bankAccountService: BankAccountService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.userSaveEconomyService.query()
            .subscribe((res: HttpResponse<UserSaveEconomy[]>) => { this.usersaveeconomies = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.bankAccountService.query()
            .subscribe((res: HttpResponse<BankAccount[]>) => { this.bankaccounts = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.saving.id !== undefined) {
            this.subscribeToSaveResponse(
                this.savingService.update(this.saving));
        } else {
            this.subscribeToSaveResponse(
                this.savingService.create(this.saving));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Saving>>) {
        result.subscribe((res: HttpResponse<Saving>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Saving) {
        this.eventManager.broadcast({ name: 'savingListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackUserSaveEconomyById(index: number, item: UserSaveEconomy) {
        return item.id;
    }

    trackBankAccountById(index: number, item: BankAccount) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-saving-popup',
    template: ''
})
export class SavingPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private savingPopupService: SavingPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.savingPopupService
                    .open(SavingDialogComponent as Component, params['id']);
            } else {
                this.savingPopupService
                    .open(SavingDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
