import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SafeEconomySharedModule } from '../../shared';
import {
    SavingService,
    SavingPopupService,
    SavingComponent,
    SavingDetailComponent,
    SavingDialogComponent,
    SavingPopupComponent,
    SavingDeletePopupComponent,
    SavingDeleteDialogComponent,
    savingRoute,
    savingPopupRoute,
    SavingResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...savingRoute,
    ...savingPopupRoute,
];

@NgModule({
    imports: [
        SafeEconomySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SavingComponent,
        SavingDetailComponent,
        SavingDialogComponent,
        SavingDeleteDialogComponent,
        SavingPopupComponent,
        SavingDeletePopupComponent,
    ],
    entryComponents: [
        SavingComponent,
        SavingDialogComponent,
        SavingPopupComponent,
        SavingDeleteDialogComponent,
        SavingDeletePopupComponent,
    ],
    providers: [
        SavingService,
        SavingPopupService,
        SavingResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SafeEconomySavingModule {}
