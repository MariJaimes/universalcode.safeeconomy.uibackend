import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Saving } from './saving.model';
import { SavingService } from './saving.service';

@Component({
    selector: 'jhi-saving-detail',
    templateUrl: './saving-detail.component.html'
})
export class SavingDetailComponent implements OnInit, OnDestroy {

    saving: Saving;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private savingService: SavingService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSavings();
    }

    load(id) {
        this.savingService.find(id)
            .subscribe((savingResponse: HttpResponse<Saving>) => {
                this.saving = savingResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSavings() {
        this.eventSubscriber = this.eventManager.subscribe(
            'savingListModification',
            (response) => this.load(this.saving.id)
        );
    }
}
