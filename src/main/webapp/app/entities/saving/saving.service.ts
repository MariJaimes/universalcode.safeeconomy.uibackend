import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Saving } from './saving.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Saving>;

@Injectable()
export class SavingService {

    private resourceUrl =  SERVER_API_URL + 'api/savings';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(saving: Saving): Observable<EntityResponseType> {
        const copy = this.convert(saving);
        return this.http.post<Saving>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(saving: Saving): Observable<EntityResponseType> {
        const copy = this.convert(saving);
        return this.http.put<Saving>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Saving>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    getAllSavingsByUser(id): Observable<EntityResponseType> {
        return this.http.get<Saving>(SERVER_API_URL + 'api/savings-by-user/' + id, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Saving[]>> {
        const options = createRequestOption(req);
        return this.http.get<Saving[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Saving[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Saving = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Saving[]>): HttpResponse<Saving[]> {
        const jsonResponse: Saving[] = res.body;
        const body: Saving[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Saving.
     */
    private convertItemFromServer(saving: Saving): Saving {
        const copy: Saving = Object.assign({}, saving);
        copy.dateToFinish = this.dateUtils
            .convertDateTimeFromServer(saving.dateToFinish);
        return copy;
    }

    /**
     * Convert a Saving to a JSON which can be sent to the server.
     */
    private convert(saving: Saving): Saving {
        const copy: Saving = Object.assign({}, saving);

        copy.dateToFinish = this.dateUtils.toDate(saving.dateToFinish);
        return copy;
    }
}
