import { BaseEntity } from './../../shared';

export class Saving implements BaseEntity {
    constructor(
        public id?: number,
        public description?: string,
        public dateToFinish?: any,
        public ammount?: number,
        public savingPercentage?: number,
        public userSaveEconomyId?: number,
        public bankAccountId?: number,
    ) {
    }
}
