import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { SavingComponent } from './saving.component';
import { SavingDetailComponent } from './saving-detail.component';
import { SavingPopupComponent } from './saving-dialog.component';
import { SavingDeletePopupComponent } from './saving-delete-dialog.component';

@Injectable()
export class SavingResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const savingRoute: Routes = [
    {
        path: 'cms/saving',
        component: SavingComponent,
        resolve: {
            'pagingParams': SavingResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Savings'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'cms/saving/:id',
        component: SavingDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Savings'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const savingPopupRoute: Routes = [
    {
        path: 'saving-new',
        component: SavingPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Savings'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'saving/:id/edit',
        component: SavingPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Savings'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'saving/:id/delete',
        component: SavingDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Savings'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
