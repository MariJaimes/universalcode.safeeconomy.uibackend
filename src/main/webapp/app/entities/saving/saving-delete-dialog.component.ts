import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Saving } from './saving.model';
import { SavingPopupService } from './saving-popup.service';
import { SavingService } from './saving.service';

@Component({
    selector: 'jhi-saving-delete-dialog',
    templateUrl: './saving-delete-dialog.component.html'
})
export class SavingDeleteDialogComponent {

    saving: Saving;

    constructor(
        private savingService: SavingService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.savingService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'savingListModification',
                content: 'Deleted an saving'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-saving-delete-popup',
    template: ''
})
export class SavingDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private savingPopupService: SavingPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.savingPopupService
                .open(SavingDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
