import { BaseEntity } from './../../shared';

export class BankAccount implements BaseEntity {
    constructor(
        public id?: number,
        public userName?: string,
        public password?: string,
        public token?: string,
        public mont?: number,
        public budget?: number,
        public bank?: string,
        public active?: boolean,
        public typeCoin?: string,
        public userSaveEconomyId?: number,
        public movements?: BaseEntity[],
        public savings?: BaseEntity[],
        public bankEntityId?: number,
    ) {
    }
}
