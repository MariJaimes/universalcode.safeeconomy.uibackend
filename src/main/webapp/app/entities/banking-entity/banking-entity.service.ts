import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { BankingEntity } from './banking-entity.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<BankingEntity>;

@Injectable()
export class BankingEntityService {

    private resourceUrl =  SERVER_API_URL + 'api/banking-entities';

    constructor(private http: HttpClient) { }

    create(bankingEntity: BankingEntity): Observable<EntityResponseType> {
        const copy = this.convert(bankingEntity);
        return this.http.post<BankingEntity>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(bankingEntity: BankingEntity): Observable<EntityResponseType> {
        const copy = this.convert(bankingEntity);
        return this.http.put<BankingEntity>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<BankingEntity>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<BankingEntity[]>> {
        const options = createRequestOption(req);
        return this.http.get<BankingEntity[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<BankingEntity[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: BankingEntity = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<BankingEntity[]>): HttpResponse<BankingEntity[]> {
        const jsonResponse: BankingEntity[] = res.body;
        const body: BankingEntity[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to BankingEntity.
     */
    private convertItemFromServer(bankingEntity: BankingEntity): BankingEntity {
        const copy: BankingEntity = Object.assign({}, bankingEntity);
        return copy;
    }

    /**
     * Convert a BankingEntity to a JSON which can be sent to the server.
     */
    private convert(bankingEntity: BankingEntity): BankingEntity {
        const copy: BankingEntity = Object.assign({}, bankingEntity);
        return copy;
    }
    findAllEntity(): Observable<EntityResponseType> {
        return this.http.get<BankingEntity>(`${SERVER_API_URL}api/getAllBankEntity`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }
    findAllBankEntitysByActive(): Observable<EntityResponseType> {
        return this.http.get<BankingEntity>(`${SERVER_API_URL}api/getAllBankEntitysByActive`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

}
