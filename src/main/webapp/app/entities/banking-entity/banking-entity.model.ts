import { BaseEntity } from './../../shared';

export class BankingEntity implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public url?: string,
        public logo?: string,
        public active?: boolean,
    ) {
        this.active = false;
    }
}
