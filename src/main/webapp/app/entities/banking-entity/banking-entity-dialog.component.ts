import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { BankingEntity } from './banking-entity.model';
import { BankingEntityPopupService } from './banking-entity-popup.service';
import { BankingEntityService } from './banking-entity.service';

@Component({
    selector: 'jhi-banking-entity-dialog',
    templateUrl: './banking-entity-dialog.component.html'
})
export class BankingEntityDialogComponent implements OnInit {

    bankingEntity: BankingEntity;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private bankingEntityService: BankingEntityService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.bankingEntity.id !== undefined) {
            this.subscribeToSaveResponse(
                this.bankingEntityService.update(this.bankingEntity));
        } else {
            this.subscribeToSaveResponse(
                this.bankingEntityService.create(this.bankingEntity));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<BankingEntity>>) {
        result.subscribe((res: HttpResponse<BankingEntity>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: BankingEntity) {
        this.eventManager.broadcast({ name: 'bankingEntityListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-banking-entity-popup',
    template: ''
})
export class BankingEntityPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private bankingEntityPopupService: BankingEntityPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.bankingEntityPopupService
                    .open(BankingEntityDialogComponent as Component, params['id']);
            } else {
                this.bankingEntityPopupService
                    .open(BankingEntityDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
