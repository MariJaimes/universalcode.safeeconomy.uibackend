import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { BankingEntityComponent } from './banking-entity.component';
import { BankingEntityDetailComponent } from './banking-entity-detail.component';
import { BankingEntityPopupComponent } from './banking-entity-dialog.component';
import { BankingEntityDeletePopupComponent } from './banking-entity-delete-dialog.component';

@Injectable()
export class BankingEntityResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const bankingEntityRoute: Routes = [
    {
        path: 'cms/banking-entity',
        component: BankingEntityComponent,
        resolve: {
            'pagingParams': BankingEntityResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'BankingEntities'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'cms/banking-entity/:id',
        component: BankingEntityDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'BankingEntities'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const bankingEntityPopupRoute: Routes = [
    {
        path: 'banking-entity-new',
        component: BankingEntityPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'BankingEntities'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'banking-entity/:id/edit',
        component: BankingEntityPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'BankingEntities'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'banking-entity/:id/delete',
        component: BankingEntityDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'BankingEntities'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
