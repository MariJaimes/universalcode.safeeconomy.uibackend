import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { BankingEntity } from './banking-entity.model';
import { BankingEntityPopupService } from './banking-entity-popup.service';
import { BankingEntityService } from './banking-entity.service';

@Component({
    selector: 'jhi-banking-entity-delete-dialog',
    templateUrl: './banking-entity-delete-dialog.component.html'
})
export class BankingEntityDeleteDialogComponent {

    bankingEntity: BankingEntity;

    constructor(
        private bankingEntityService: BankingEntityService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.bankingEntityService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'bankingEntityListModification',
                content: 'Deleted an bankingEntity'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-banking-entity-delete-popup',
    template: ''
})
export class BankingEntityDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private bankingEntityPopupService: BankingEntityPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.bankingEntityPopupService
                .open(BankingEntityDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
