import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { BankingEntity } from './banking-entity.model';
import { BankingEntityService } from './banking-entity.service';

@Component({
    selector: 'jhi-banking-entity-detail',
    templateUrl: './banking-entity-detail.component.html'
})
export class BankingEntityDetailComponent implements OnInit, OnDestroy {

    bankingEntity: BankingEntity;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private bankingEntityService: BankingEntityService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInBankingEntities();
    }

    load(id) {
        this.bankingEntityService.find(id)
            .subscribe((bankingEntityResponse: HttpResponse<BankingEntity>) => {
                this.bankingEntity = bankingEntityResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInBankingEntities() {
        this.eventSubscriber = this.eventManager.subscribe(
            'bankingEntityListModification',
            (response) => this.load(this.bankingEntity.id)
        );
    }
}
