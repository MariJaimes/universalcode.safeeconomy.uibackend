import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SafeEconomySharedModule } from '../../shared';
import {
    BankingEntityService,
    BankingEntityPopupService,
    BankingEntityComponent,
    BankingEntityDetailComponent,
    BankingEntityDialogComponent,
    BankingEntityPopupComponent,
    BankingEntityDeletePopupComponent,
    BankingEntityDeleteDialogComponent,
    bankingEntityRoute,
    bankingEntityPopupRoute,
    BankingEntityResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...bankingEntityRoute,
    ...bankingEntityPopupRoute,
];

@NgModule({
    imports: [
        SafeEconomySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        BankingEntityComponent,
        BankingEntityDetailComponent,
        BankingEntityDialogComponent,
        BankingEntityDeleteDialogComponent,
        BankingEntityPopupComponent,
        BankingEntityDeletePopupComponent,
    ],
    entryComponents: [
        BankingEntityComponent,
        BankingEntityDialogComponent,
        BankingEntityPopupComponent,
        BankingEntityDeleteDialogComponent,
        BankingEntityDeletePopupComponent,
    ],
    providers: [
        BankingEntityService,
        BankingEntityPopupService,
        BankingEntityResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SafeEconomyBankingEntityModule {}
