import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { SafeEconomyBinnacleModule } from './binnacle/binnacle.module';
import { SafeEconomyUserSaveEconomyModule } from './user-save-economy/user-save-economy.module';
import { SafeEconomyBankAccountModule } from './bank-account/bank-account.module';
import { SafeEconomyLoanModule } from './loan/loan.module';
import { SafeEconomyBankMovementPaymentModule } from './bank-movement-payment/bank-movement-payment.module';
import { SafeEconomyBankingEntityModule } from './banking-entity/banking-entity.module';
import { SafeEconomyKeyValueModule } from './key-value/key-value.module';
import { SafeEconomySavingModule } from './saving/saving.module';
import { SafeEconomyElementCrawlableModule } from './element-crawlable/element-crawlable.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        SafeEconomyBinnacleModule,
        SafeEconomyUserSaveEconomyModule,
        SafeEconomyBankAccountModule,
        SafeEconomyLoanModule,
        SafeEconomyBankMovementPaymentModule,
        SafeEconomyBankingEntityModule,
        SafeEconomyKeyValueModule,
        SafeEconomySavingModule,
        SafeEconomyElementCrawlableModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SafeEconomyEntityModule {}
