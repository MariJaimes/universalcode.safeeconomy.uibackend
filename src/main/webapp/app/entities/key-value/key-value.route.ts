import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { KeyValueComponent } from './key-value.component';
import { KeyValueDetailComponent } from './key-value-detail.component';
import { KeyValuePopupComponent } from './key-value-dialog.component';
import { KeyValueDeletePopupComponent } from './key-value-delete-dialog.component';

@Injectable()
export class KeyValueResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const keyValueRoute: Routes = [
    {
        path: 'cms/key-value',
        component: KeyValueComponent,
        resolve: {
            'pagingParams': KeyValueResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'KeyValues'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'cms/key-value/:id',
        component: KeyValueDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'KeyValues'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const keyValuePopupRoute: Routes = [
    {
        path: 'key-value-new',
        component: KeyValuePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'KeyValues'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'key-value/:id/edit',
        component: KeyValuePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'KeyValues'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'key-value/:id/delete',
        component: KeyValueDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'KeyValues'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
