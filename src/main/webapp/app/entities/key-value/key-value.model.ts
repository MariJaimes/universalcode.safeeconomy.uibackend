import { BaseEntity } from './../../shared';

export class KeyValue implements BaseEntity {
    constructor(
        public id?: number,
        public group?: string,
        public description?: string,
    ) {
    }
}
