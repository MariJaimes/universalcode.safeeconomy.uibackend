import { Route } from '@angular/router';

import {JhiMainComponent} from './main.component';
import {navbarRoute} from '../';
import {HOME_ROUTE} from '../../home';
import {AuthGuardService} from './auth-guard-admin.service';
import {passwordRoute} from '../../account';

export const MAIN_ROUTE: Route = {
    path: 'cms',
    component: JhiMainComponent,
    data: {
        authorities: [],
        pageTitle: 'Welcome, Java Hipster!'
    },
    children: [
        navbarRoute,
        HOME_ROUTE,
        passwordRoute
    ],
    canActivate: [AuthGuardService]
};
