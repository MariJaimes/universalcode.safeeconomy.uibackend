import { Injectable } from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Principal} from '../../shared/auth/principal.service';
@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(public router: Router, private principal: Principal) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return this.principal.identity().then( (account) => {
            if (account) {
                return account.authorities.indexOf('ROLE_ADMIN') !== -1 ;
            }
            return false;
        });
    }
}
