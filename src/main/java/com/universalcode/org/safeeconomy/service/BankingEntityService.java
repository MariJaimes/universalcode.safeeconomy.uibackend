package com.universalcode.org.safeeconomy.service;

import com.universalcode.org.safeeconomy.domain.BankingEntity;
import com.universalcode.org.safeeconomy.repository.BankingEntityRepository;
import com.universalcode.org.safeeconomy.service.dto.BankingEntityDTO;
import com.universalcode.org.safeeconomy.service.mapper.BankingEntityMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Service Implementation for managing BankingEntity.
 */
@Service
@Transactional
public class BankingEntityService {

    private final Logger log = LoggerFactory.getLogger(BankingEntityService.class);

    private final BankingEntityRepository bankingEntityRepository;

    private final BankingEntityMapper bankingEntityMapper;

    public BankingEntityService(BankingEntityRepository bankingEntityRepository, BankingEntityMapper bankingEntityMapper) {
        this.bankingEntityRepository = bankingEntityRepository;
        this.bankingEntityMapper = bankingEntityMapper;
    }

    /**
     * Save a bankingEntity.
     *
     * @param bankingEntityDTO the entity to save
     * @return the persisted entity
     */
    public BankingEntityDTO save(BankingEntityDTO bankingEntityDTO) {
        log.debug("Request to save BankingEntity : {}", bankingEntityDTO);
        BankingEntity bankingEntity = bankingEntityMapper.toEntity(bankingEntityDTO);
        bankingEntity.setActive(bankingEntityDTO.getActive());
        bankingEntity = bankingEntityRepository.save(bankingEntity);
        return bankingEntityMapper.toDto(bankingEntity);
    }

    /**
     * Get all the bankingEntities.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BankingEntityDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BankingEntities");
        return bankingEntityRepository.findAll(pageable)
            .map(bankingEntityMapper::toDto);
    }

    /**
     * Get one bankingEntity by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public BankingEntityDTO findOne(Long id) {
        log.debug("Request to get BankingEntity : {}", id);
        BankingEntity bankingEntity = bankingEntityRepository.findOne(id);
        return bankingEntityMapper.toDto(bankingEntity);
    }

    /**
     * Delete the bankingEntity by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete BankingEntity : {}", id);
        bankingEntityRepository.delete(id);
    }
    @Transactional(readOnly = true)
    public List<BankingEntityDTO> findAllBankEntity() {
        List<BankingEntity> listBankEntity = bankingEntityRepository.findAll();
        return bankingEntityMapper.toDto(listBankEntity);
    }
    @Transactional(readOnly = true)
    public List<BankingEntityDTO> findAllBankEntityByActive() {
        List<BankingEntity> listBankEntity = bankingEntityRepository.findAllBankEntityByActive(true);
        return bankingEntityMapper.toDto(listBankEntity);
    }
    public BankingEntity toEntity (BankingEntityDTO dto){
        return this.bankingEntityMapper.toEntity(dto);
    }



}
