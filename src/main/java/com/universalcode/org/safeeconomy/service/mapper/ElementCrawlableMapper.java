package com.universalcode.org.safeeconomy.service.mapper;

import com.universalcode.org.safeeconomy.domain.*;
import com.universalcode.org.safeeconomy.service.dto.ElementCrawlableDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ElementCrawlable and its DTO ElementCrawlableDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ElementCrawlableMapper extends EntityMapper<ElementCrawlableDTO, ElementCrawlable> {



    default ElementCrawlable fromId(Long id) {
        if (id == null) {
            return null;
        }
        ElementCrawlable elementCrawlable = new ElementCrawlable();
        elementCrawlable.setId(id);
        return elementCrawlable;
    }
}
