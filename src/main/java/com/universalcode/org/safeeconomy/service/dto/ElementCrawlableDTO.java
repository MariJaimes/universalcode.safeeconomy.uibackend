package com.universalcode.org.safeeconomy.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ElementCrawlable entity.
 */
public class ElementCrawlableDTO implements Serializable {

    private Long id;

    private Integer bankId;

    private Integer userId;

    private String url;

    private Boolean wasProcessed;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean isWasProcessed() {
        return wasProcessed;
    }

    public void setWasProcessed(Boolean wasProcessed) {
        this.wasProcessed = wasProcessed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ElementCrawlableDTO elementCrawlableDTO = (ElementCrawlableDTO) o;
        if(elementCrawlableDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), elementCrawlableDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ElementCrawlableDTO{" +
            "id=" + getId() +
            ", bankId=" + getBankId() +
            ", userId=" + getUserId() +
            ", url='" + getUrl() + "'" +
            ", wasProcessed='" + isWasProcessed() + "'" +
            "}";
    }
}
