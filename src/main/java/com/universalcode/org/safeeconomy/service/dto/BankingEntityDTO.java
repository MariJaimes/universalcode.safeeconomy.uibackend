package com.universalcode.org.safeeconomy.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the BankingEntity entity.
 */
public class BankingEntityDTO implements Serializable {

    private Long id;

    private String name;

    private String url;

    private String logo;

    private Boolean active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BankingEntityDTO bankingEntityDTO = (BankingEntityDTO) o;
        if(bankingEntityDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bankingEntityDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BankingEntityDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", url='" + getUrl() + "'" +
            ", logo='" + getLogo() + "'" +
            ", active='" + getActive() + "'" +
            "}";
    }
}
