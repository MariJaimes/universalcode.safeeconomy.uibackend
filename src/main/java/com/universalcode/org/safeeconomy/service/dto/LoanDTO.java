package com.universalcode.org.safeeconomy.service.dto;


import java.time.Instant;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Loan entity.
 */
public class LoanDTO implements Serializable {

    private Long id;

    private String name;

    private String description;

    private String rate;

    private Boolean isMonthly;

    private Boolean isPaid;

    private Instant finishDate;

    private Long userSaveEconomyId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public Boolean isIsMonthly() {
        return isMonthly;
    }

    public void setIsMonthly(Boolean isMonthly) {
        this.isMonthly = isMonthly;
    }

    public Boolean isIsPaid() {
        return isPaid;
    }

    public void setIsPaid(Boolean isPaid) {
        this.isPaid = isPaid;
    }

    public Instant getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Instant finishDate) {
        this.finishDate = finishDate;
    }

    public Long getUserSaveEconomyId() {
        return userSaveEconomyId;
    }

    public void setUserSaveEconomyId(Long userSaveEconomyId) {
        this.userSaveEconomyId = userSaveEconomyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LoanDTO loanDTO = (LoanDTO) o;
        if(loanDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), loanDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LoanDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", rate='" + getRate() + "'" +
            ", isMonthly='" + isIsMonthly() + "'" +
            ", isPaid='" + isIsPaid() + "'" +
            ", finishDate='" + getFinishDate() + "'" +
            "}";
    }
}
