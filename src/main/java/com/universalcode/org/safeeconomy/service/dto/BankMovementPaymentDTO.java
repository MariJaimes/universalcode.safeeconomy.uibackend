package com.universalcode.org.safeeconomy.service.dto;


import java.time.Instant;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the BankMovementPayment entity.
 */
public class BankMovementPaymentDTO implements Serializable {

    private Long id;

    private String kindOfMovement;

    private Instant dateOfMovement;

    private String description;

    private Double acreditationAmmount;

    private Boolean isPaid;

    private String comment;

    private Integer priority;

    private Long userSaveEconomyId;

    private Long bankAccountId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKindOfMovement() {
        return kindOfMovement;
    }

    public void setKindOfMovement(String kindOfMovement) {
        this.kindOfMovement = kindOfMovement;
    }

    public Instant getDateOfMovement() {
        return dateOfMovement;
    }

    public void setDateOfMovement(Instant dateOfMovement) {
        this.dateOfMovement = dateOfMovement;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getAcreditationAmmount() {
        return acreditationAmmount;
    }

    public void setAcreditationAmmount(Double acreditationAmmount) {
        this.acreditationAmmount = acreditationAmmount;
    }

    public Boolean isIsPaid() {
        return isPaid;
    }

    public void setIsPaid(Boolean isPaid) {
        this.isPaid = isPaid;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Long getUserSaveEconomyId() {
        return userSaveEconomyId;
    }

    public void setUserSaveEconomyId(Long userSaveEconomyId) {
        this.userSaveEconomyId = userSaveEconomyId;
    }

    public Long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BankMovementPaymentDTO bankMovementPaymentDTO = (BankMovementPaymentDTO) o;
        if(bankMovementPaymentDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bankMovementPaymentDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BankMovementPaymentDTO{" +
            "id=" + getId() +
            ", kindOfMovement='" + getKindOfMovement() + "'" +
            ", dateOfMovement='" + getDateOfMovement() + "'" +
            ", description='" + getDescription() + "'" +
            ", acreditationAmmount=" + getAcreditationAmmount() +
            ", isPaid='" + isIsPaid() + "'" +
            ", comment='" + getComment() + "'" +
            ", priority=" + getPriority() +
            "}";
    }
}
