package com.universalcode.org.safeeconomy.service.mapper;

import com.universalcode.org.safeeconomy.domain.*;
import com.universalcode.org.safeeconomy.service.dto.BankingEntityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity BankingEntity and its DTO BankingEntityDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BankingEntityMapper extends EntityMapper<BankingEntityDTO, BankingEntity> {



    default BankingEntity fromId(Long id) {
        if (id == null) {
            return null;
        }
        BankingEntity bankingEntity = new BankingEntity();
        bankingEntity.setId(id);
        return bankingEntity;
    }
}
