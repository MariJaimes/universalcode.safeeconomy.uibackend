package com.universalcode.org.safeeconomy.service.mapper;

import com.universalcode.org.safeeconomy.domain.*;
import com.universalcode.org.safeeconomy.service.dto.BankAccountDTO;

import org.mapstruct.*;

import java.util.List;

/**
 * Mapper for the entity BankAccount and its DTO BankAccountDTO.
 */
@Mapper(componentModel = "spring", uses = {UserSaveEconomyMapper.class, BankingEntityMapper.class})
public interface BankAccountMapper extends EntityMapper<BankAccountDTO, BankAccount> {

    List<BankAccount> toEntity(List<BankAccountDTO> dtoList);
    List<BankAccountDTO> toDto(List<BankAccount> entityList);


    @Mapping(source = "userSaveEconomy.id", target = "userSaveEconomyId")
    @Mapping(source = "bankEntity.id", target = "bankEntityId")
    BankAccountDTO toDto(BankAccount bankAccount);

    @Mapping(source = "userSaveEconomyId", target = "userSaveEconomy")
    @Mapping(target = "movements", ignore = true)
    @Mapping(target = "savings", ignore = true)
    @Mapping(source = "bankEntityId", target = "bankEntity")
    BankAccount toEntity(BankAccountDTO bankAccountDTO);

    default BankAccount fromId(Long id) {
        if (id == null) {
            return null;
        }
        BankAccount bankAccount = new BankAccount();
        bankAccount.setId(id);
        return bankAccount;
    }
}
