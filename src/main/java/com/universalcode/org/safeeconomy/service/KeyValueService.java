package com.universalcode.org.safeeconomy.service;

import com.universalcode.org.safeeconomy.domain.KeyValue;
import com.universalcode.org.safeeconomy.repository.KeyValueRepository;
import com.universalcode.org.safeeconomy.service.dto.KeyValueDTO;
import com.universalcode.org.safeeconomy.service.mapper.KeyValueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Service Implementation for managing KeyValue.
 */
@Service
@Transactional
public class KeyValueService {

    private final Logger log = LoggerFactory.getLogger(KeyValueService.class);

    private final KeyValueRepository keyValueRepository;

    private final KeyValueMapper keyValueMapper;

    public KeyValueService(KeyValueRepository keyValueRepository, KeyValueMapper keyValueMapper) {
        this.keyValueRepository = keyValueRepository;
        this.keyValueMapper = keyValueMapper;
    }

    /**
     * Save a keyValue.
     *
     * @param keyValueDTO the entity to save
     * @return the persisted entity
     */
    public KeyValueDTO save(KeyValueDTO keyValueDTO) {
        log.debug("Request to save KeyValue : {}", keyValueDTO);
        KeyValue keyValue = keyValueMapper.toEntity(keyValueDTO);
        keyValue = keyValueRepository.save(keyValue);
        return keyValueMapper.toDto(keyValue);
    }

    /**
     * Get all the keyValues.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<KeyValueDTO> findAll(Pageable pageable) {
        log.debug("Request to get all KeyValues");
        return keyValueRepository.findAll(pageable)
            .map(keyValueMapper::toDto);
    }

    @Transactional(readOnly = true)
    public List<KeyValueDTO> findAllTypes() {
        List<KeyValue> listKeyValue = this.keyValueRepository.findAll();
        return this.keyValueMapper.toDto(listKeyValue);
    }

    /**
     * Get one keyValue by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public KeyValueDTO findOne(Long id) {
        log.debug("Request to get KeyValue : {}", id);
        KeyValue keyValue = keyValueRepository.findOne(id);
        return keyValueMapper.toDto(keyValue);
    }


    /**
     * Delete the keyValue by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete KeyValue : {}", id);
        keyValueRepository.delete(id);
    }

    public List<KeyValueDTO> getAllKeyValue() {
        List<KeyValue> listKeyValue = keyValueRepository.findAll();
        return keyValueMapper.toDto(listKeyValue);

    }

}
