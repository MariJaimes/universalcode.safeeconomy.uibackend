package com.universalcode.org.safeeconomy.service;

import com.universalcode.org.safeeconomy.domain.BankAccount;
import com.universalcode.org.safeeconomy.domain.Saving;
import com.universalcode.org.safeeconomy.repository.SavingRepository;
import com.universalcode.org.safeeconomy.service.dto.BankAccountDTO;
import com.universalcode.org.safeeconomy.service.dto.SavingDTO;
import com.universalcode.org.safeeconomy.service.mapper.SavingMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Service Implementation for managing Saving.
 */
@Service
@Transactional
public class SavingService {

    private final Logger log = LoggerFactory.getLogger(SavingService.class);

    private final SavingRepository savingRepository;

    private final SavingMapper savingMapper;

    private final BankAccountService bankAccountService;

    private final UserSaveEconomyService userSaveEconomyService;

    public SavingService(SavingRepository savingRepository,
                         SavingMapper savingMapper,
                         BankAccountService bankAccountService,
                         UserSaveEconomyService userSaveEconomyService) {
        this.savingRepository = savingRepository;
        this.savingMapper = savingMapper;
        this.bankAccountService = bankAccountService;
        this.userSaveEconomyService = userSaveEconomyService;
    }

    /**
     * Save a saving.
     *
     * @param savingDTO the entity to save
     * @return the persisted entity
     */
    public SavingDTO save(SavingDTO savingDTO) {
        log.debug("Request to save Saving : {}", savingDTO);
        BankAccountDTO bank = bankAccountService.findOne(savingDTO.getBankAccountId());
        double amountPerMonth = (savingDTO.getSavingPercentage() * bank.getBudget())/100;
        double  monthsToSave =  (savingDTO.getAmmount() / amountPerMonth);
        int f = (int) monthsToSave;
        Date d = new Date();
        Date k = new Date(d.getYear(), d.getMonth()+f, d.getDate());
        savingDTO.setDateToFinish(k.toInstant());
        Saving saving = savingMapper.toEntity(savingDTO);
        saving = savingRepository.save(saving);
        return savingMapper.toDto(saving);
    }

    /**
     * Get all the savings.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SavingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Savings");
        return savingRepository.findAll(pageable)
            .map(savingMapper::toDto);
    }

    /**
     * Get one saving by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public SavingDTO findOne(Long id) {
        log.debug("Request to get Saving : {}", id);
        Saving saving = savingRepository.findOne(id);
        return savingMapper.toDto(saving);
    }

    /**
     * Delete the saving by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Saving : {}", id);
        savingRepository.delete(id);
    }

    public List<SavingDTO> getSavingsByUser(Long id) {
        List<BankAccountDTO> bankAccountsDTOS = this.bankAccountService.findAllByUserSaveEconomy(id);
        List<BankAccount> bankAccounts = this.bankAccountService.toEntityList(bankAccountsDTOS);
        List<Saving> savingList = new ArrayList<>();
        for (BankAccount bankAccount: bankAccounts) {
            if(bankAccount.getBankEntity().getId() != 1){
                savingList.addAll(this.savingRepository.getSavingsByBankAccount(bankAccount));
            }
        }
        List<SavingDTO> savingDTOList = savingMapper.toDto(savingList);
        for(int i = 0; i < savingDTOList.size(); i++) {
            savingDTOList.get(i).setUserSaveEconomyId(id);
            savingDTOList.get(i).setSavingPercentage(savingList.get(i).getSavingPercentage());
            savingDTOList.get(i).setSavingPercentage(savingList.get(i).getSavingPercentage());
            savingDTOList.get(i).setDateToFinish(savingList.get(i).getDateToFinish());
        }


        return savingDTOList;
    }
}
