package com.universalcode.org.safeeconomy.service;

import com.universalcode.org.safeeconomy.domain.ElementCrawlable;
import com.universalcode.org.safeeconomy.repository.ElementCrawlableRepository;
import com.universalcode.org.safeeconomy.service.dto.ElementCrawlableDTO;
import com.universalcode.org.safeeconomy.service.mapper.ElementCrawlableMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing ElementCrawlable.
 */
@Service
@Transactional
public class ElementCrawlableService {

    private final Logger log = LoggerFactory.getLogger(ElementCrawlableService.class);

    private final ElementCrawlableRepository elementCrawlableRepository;

    private final ElementCrawlableMapper elementCrawlableMapper;

    public ElementCrawlableService(ElementCrawlableRepository elementCrawlableRepository, ElementCrawlableMapper elementCrawlableMapper) {
        this.elementCrawlableRepository = elementCrawlableRepository;
        this.elementCrawlableMapper = elementCrawlableMapper;
    }

    /**
     * Save a elementCrawlable.
     *
     * @param elementCrawlableDTO the entity to save
     * @return the persisted entity
     */
    public ElementCrawlableDTO save(ElementCrawlableDTO elementCrawlableDTO) {
        log.debug("Request to save ElementCrawlable : {}", elementCrawlableDTO);
        ElementCrawlable elementCrawlable = elementCrawlableMapper.toEntity(elementCrawlableDTO);
        elementCrawlable = elementCrawlableRepository.save(elementCrawlable);
        return elementCrawlableMapper.toDto(elementCrawlable);
    }

    /**
     * Get all the elementCrawlables.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ElementCrawlableDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ElementCrawlables");
        return elementCrawlableRepository.findAll(pageable)
            .map(elementCrawlableMapper::toDto);
    }

    /**
     * Get one elementCrawlable by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ElementCrawlableDTO findOne(Long id) {
        log.debug("Request to get ElementCrawlable : {}", id);
        ElementCrawlable elementCrawlable = elementCrawlableRepository.findOne(id);
        return elementCrawlableMapper.toDto(elementCrawlable);
    }

    /**
     * Delete the elementCrawlable by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ElementCrawlable : {}", id);
        elementCrawlableRepository.delete(id);
    }
}
