package com.universalcode.org.safeeconomy.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the BankAccount entity.
 */
public class BankAccountDTO implements Serializable {

    private Long id;

    private String userName;

    private String password;

    private String token;

    private Double mont;

    private Double budget;

    private String bank;

    private Boolean active;

    private String typeCoin;

    private Long userSaveEconomyId;

    private Long bankEntityId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Double getMont() {
        return mont;
    }

    public void setMont(Double mont) {
        this.mont = mont;
    }

    public Double getBudget() {
        return budget;
    }

    public void setBudget(Double budget) {
        this.budget = budget;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getTypeCoin() {
        return typeCoin;
    }

    public void setTypeCoin(String typeCoin) {
        this.typeCoin = typeCoin;
    }

    public Long getUserSaveEconomyId() {
        return userSaveEconomyId;
    }

    public void setUserSaveEconomyId(Long userSaveEconomyId) {
        this.userSaveEconomyId = userSaveEconomyId;
    }

    public Long getBankEntityId() {
        return bankEntityId;
    }

    public void setBankEntityId(Long bankingEntityId) {
        this.bankEntityId = bankingEntityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BankAccountDTO bankAccountDTO = (BankAccountDTO) o;
        if(bankAccountDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bankAccountDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BankAccountDTO{" +
            "id=" + getId() +
            ", userName='" + getUserName() + "'" +
            ", password='" + getPassword() + "'" +
            ", token='" + getToken() + "'" +
            ", mont=" + getMont() +
            ", budget=" + getBudget() +
            ", bank='" + getBank() + "'" +
            ", active='" + isActive() + "'" +
            ", typeCoin='" + getTypeCoin() + "'" +
            "}";
    }
}
