package com.universalcode.org.safeeconomy.service;

import com.universalcode.org.safeeconomy.domain.Binnacle;
import com.universalcode.org.safeeconomy.repository.BinnacleRepository;
import com.universalcode.org.safeeconomy.service.dto.BinnacleDTO;
import com.universalcode.org.safeeconomy.service.mapper.BinnacleMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Binnacle.
 */
@Service
@Transactional
public class BinnacleService {

    private final Logger log = LoggerFactory.getLogger(BinnacleService.class);

    private final BinnacleRepository binnacleRepository;

    private final BinnacleMapper binnacleMapper;

    public BinnacleService(BinnacleRepository binnacleRepository, BinnacleMapper binnacleMapper) {
        this.binnacleRepository = binnacleRepository;
        this.binnacleMapper = binnacleMapper;
    }

    /**
     * Save a binnacle.
     *
     * @param binnacleDTO the entity to save
     * @return the persisted entity
     */
    public BinnacleDTO save(BinnacleDTO binnacleDTO) {
        log.debug("Request to save Binnacle : {}", binnacleDTO);
        Binnacle binnacle = binnacleMapper.toEntity(binnacleDTO);
        binnacle = binnacleRepository.save(binnacle);
        return binnacleMapper.toDto(binnacle);
    }

    /**
     * Get all the binnacles.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BinnacleDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Binnacles");
        return binnacleRepository.findAll(pageable)
            .map(binnacleMapper::toDto);
    }

    /**
     * Get one binnacle by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public BinnacleDTO findOne(Long id) {
        log.debug("Request to get Binnacle : {}", id);
        Binnacle binnacle = binnacleRepository.findOne(id);
        return binnacleMapper.toDto(binnacle);
    }

    /**
     * Delete the binnacle by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Binnacle : {}", id);
        binnacleRepository.delete(id);
    }
}
