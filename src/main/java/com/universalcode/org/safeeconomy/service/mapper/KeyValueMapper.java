package com.universalcode.org.safeeconomy.service.mapper;

import com.universalcode.org.safeeconomy.domain.*;
import com.universalcode.org.safeeconomy.service.dto.KeyValueDTO;

import org.mapstruct.*;

import java.util.List;

/**
 * Mapper for the entity KeyValue and its DTO KeyValueDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface KeyValueMapper extends EntityMapper<KeyValueDTO, KeyValue> {

    List<KeyValue> toEntity(List<KeyValueDTO> dtoList);
    List<KeyValueDTO> toDto(List<KeyValue> entityList);

    KeyValueDTO toDto(KeyValue keyValue);

    KeyValue toEntity(KeyValueDTO keyValueDTO);

    default KeyValue fromId(Long id) {
        if (id == null) {
            return null;
        }
        KeyValue keyValue = new KeyValue();
        keyValue.setId(id);
        return keyValue;
    }
}
