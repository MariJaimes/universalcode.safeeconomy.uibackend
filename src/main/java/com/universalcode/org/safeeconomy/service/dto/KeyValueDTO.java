package com.universalcode.org.safeeconomy.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the KeyValue entity.
 */
public class KeyValueDTO implements Serializable {

    private Long id;

    private String group;

    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        KeyValueDTO keyValueDTO = (KeyValueDTO) o;
        if(keyValueDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), keyValueDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "KeyValueDTO{" +
            "id=" + getId() +
            ", group='" + getGroup() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
