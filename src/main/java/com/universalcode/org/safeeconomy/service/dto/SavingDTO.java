package com.universalcode.org.safeeconomy.service.dto;


import java.time.Instant;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Saving entity.
 */
public class SavingDTO implements Serializable {

    private Long id;

    private String description;

    private Instant dateToFinish;

    private Double ammount;

    private Double savingPercentage;

    private Long userSaveEconomyId;

    private Long bankAccountId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Instant getDateToFinish() {
        return dateToFinish;
    }

    public void setDateToFinish(Instant dateToFinish) {
        this.dateToFinish = dateToFinish;
    }

    public Double getAmmount() {
        return ammount;
    }

    public void setAmmount(Double ammount) {
        this.ammount = ammount;
    }

    public Double getSavingPercentage() {
        return savingPercentage;
    }

    public void setSavingPercentage(Double savingPercentage) {
        this.savingPercentage = savingPercentage;
    }

    public Long getUserSaveEconomyId() {
        return userSaveEconomyId;
    }

    public void setUserSaveEconomyId(Long userSaveEconomyId) {
        this.userSaveEconomyId = userSaveEconomyId;
    }

    public Long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SavingDTO savingDTO = (SavingDTO) o;
        if(savingDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), savingDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SavingDTO{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", dateToFinish='" + getDateToFinish() + "'" +
            ", ammount=" + getAmmount() +
            ", savingPercentage=" + getSavingPercentage() +
            "}";
    }
}
