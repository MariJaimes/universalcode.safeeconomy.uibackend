package com.universalcode.org.safeeconomy.service.mapper;

import com.universalcode.org.safeeconomy.domain.*;
import com.universalcode.org.safeeconomy.service.dto.UserSaveEconomyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity UserSaveEconomy and its DTO UserSaveEconomyDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface UserSaveEconomyMapper extends EntityMapper<UserSaveEconomyDTO, UserSaveEconomy> {

    @Mapping(source = "user.id", target = "userId")
    UserSaveEconomyDTO toDto(UserSaveEconomy userSaveEconomy);

    @Mapping(source = "userId", target = "user")
    @Mapping(target = "bankAccounts", ignore = true)
    @Mapping(target = "paumenths", ignore = true)
    @Mapping(target = "loans", ignore = true)
    @Mapping(target = "savings", ignore = true)
    UserSaveEconomy toEntity(UserSaveEconomyDTO userSaveEconomyDTO);

    default UserSaveEconomy fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserSaveEconomy userSaveEconomy = new UserSaveEconomy();
        userSaveEconomy.setId(id);
        return userSaveEconomy;
    }
}
