package com.universalcode.org.safeeconomy.service.dto;


import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Binnacle entity.
 */
public class BinnacleDTO implements Serializable {

    private Long id;

    private String event;

    private String user;

    private Instant date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BinnacleDTO binnacleDTO = (BinnacleDTO) o;
        if(binnacleDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), binnacleDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BinnacleDTO{" +
            "id=" + getId() +
            ", event='" + getEvent() + "'" +
            ", user='" + getUser() + "'" +
            ", date='" + getDate() + "'" +
            "}";
    }
}
