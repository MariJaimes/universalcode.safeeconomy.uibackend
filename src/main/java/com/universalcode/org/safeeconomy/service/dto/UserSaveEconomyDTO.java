package com.universalcode.org.safeeconomy.service.dto;


import java.time.Instant;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the UserSaveEconomy entity.
 */
public class UserSaveEconomyDTO implements Serializable {

    private Long id;

    private String name;

    private String lastName;

    private String telephone;

    private String token;

    private Instant lastVisitDate;

    private String image;

    private String fact;

    private Long userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Instant getLastVisitDate() {
        return lastVisitDate;
    }

    public void setLastVisitDate(Instant lastVisitDate) {
        this.lastVisitDate = lastVisitDate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFact() {
        return fact;
    }

    public void setFact(String fact) {
        this.fact = fact;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserSaveEconomyDTO userSaveEconomyDTO = (UserSaveEconomyDTO) o;
        if(userSaveEconomyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userSaveEconomyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserSaveEconomyDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", telephone='" + getTelephone() + "'" +
            ", token='" + getToken() + "'" +
            ", lastVisitDate='" + getLastVisitDate() + "'" +
            ", image='" + getImage() + "'" +
            ", fact='" + getFact() + "'" +
            "}";
    }
}
