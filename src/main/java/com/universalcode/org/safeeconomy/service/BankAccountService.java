package com.universalcode.org.safeeconomy.service;

import com.universalcode.org.safeeconomy.domain.BankAccount;
import com.universalcode.org.safeeconomy.domain.BankingEntity;
import com.universalcode.org.safeeconomy.domain.UserSaveEconomy;
import com.universalcode.org.safeeconomy.repository.BankAccountRepository;
import com.universalcode.org.safeeconomy.repository.BankingEntityRepository;
import com.universalcode.org.safeeconomy.service.dto.BankAccountDTO;
import com.universalcode.org.safeeconomy.service.dto.BankingEntityDTO;
import com.universalcode.org.safeeconomy.service.dto.UserSaveEconomyDTO;
import com.universalcode.org.safeeconomy.service.mapper.BankAccountMapper;
import com.universalcode.org.safeeconomy.service.mapper.BankingEntityMapper;
import com.universalcode.org.safeeconomy.service.mapper.UserSaveEconomyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Service Implementation for managing BankAccount.
 */
@Service
@Transactional
public class BankAccountService {

    private final Logger log = LoggerFactory.getLogger(BankAccountService.class);

    private final BankAccountRepository bankAccountRepository;

    private final BankingEntityService bankingEntityService;

    private final BankAccountMapper bankAccountMapper;

    private final BankingEntityMapper bankingEntityMapper;

    private final UserSaveEconomyService userSaveEconomyService;

    private final UserSaveEconomyMapper userSaveEconomyMapper;

    public BankAccountService(BankAccountRepository bankAccountRepository,
                              BankAccountMapper bankAccountMapper,
                              UserSaveEconomyService userSaveEconomyService,
                              UserSaveEconomyMapper userSaveEconomyMapper,
                              BankingEntityService bankEntity,
                              BankingEntityMapper bankingEntityMapper) {
        this.bankAccountRepository = bankAccountRepository;
        this.bankAccountMapper = bankAccountMapper;
        this.userSaveEconomyService = userSaveEconomyService;
        this.userSaveEconomyMapper = userSaveEconomyMapper;
        this.bankingEntityService = bankEntity;
        this.bankingEntityMapper=bankingEntityMapper;
    }

    /**
     * Save a bankAccount.
     *
     * @param bankAccountDTO the entity to save
     * @return the persisted entity
     */
    public BankAccountDTO save(BankAccountDTO bankAccountDTO) {
        log.debug("Request to save BankAccount : {}", bankAccountDTO);
        BankAccount bankAccount = bankAccountMapper.toEntity(bankAccountDTO);
        bankAccount = bankAccountRepository.save(bankAccount);
        return bankAccountMapper.toDto(bankAccount);
    }

    /**
     * Get all the bankAccounts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BankAccountDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BankAccounts");
        return bankAccountRepository.findAll(pageable)
            .map(bankAccountMapper::toDto);
    }

    /**
     * Get one bankAccount by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public BankAccountDTO findOne(Long id) {
        log.debug("Request to get BankAccount : {}", id);
        BankAccount bankAccount = bankAccountRepository.findOne(id);
        return bankAccountMapper.toDto(bankAccount);
    }

    /**
     * Delete the bankAccount by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete BankAccount : {}", id);
        bankAccountRepository.delete(id);
    }

    @Transactional(readOnly = true)
    public List<BankAccountDTO> findAllByUserSaveEconomy(Long id) {
        List<BankAccount> listBankAccount = this.bankAccountRepository.findAllByUserSaveEconomyId(id);
        return this.bankAccountMapper.toDto(listBankAccount);
    }

    @Transactional(readOnly = true)
    public List<BankAccountDTO> findAllByUserSaveEconomyAndActive(Long id) {
        List<BankAccount> listBankAccount = this.bankAccountRepository.findAllByUserSaveEconomyIdAndActive(id,true);
        return this.bankAccountMapper.toDto(listBankAccount);
    }

    @Transactional(readOnly = true)
    public BankAccountDTO findWalletByUser(Long id) {
        log.debug("Request to get BankAccount : {}", id);
        Long idd = new Long(1);
        BankingEntityDTO bankEntity = bankingEntityService.findOne(idd);
        BankingEntity bank = this.bankingEntityMapper.toEntity(bankEntity);
        UserSaveEconomy user = this.userSaveEconomyMapper.toEntity(this.userSaveEconomyService.findOne(id));
        BankAccount bankAccount = bankAccountRepository.getBankAccountByBankEntityAndUserSaveEconomy(bank ,user);
        return bankAccountMapper.toDto(bankAccount);
    }
    public List<BankAccount> toEntityList(List<BankAccountDTO> dto){
        return this.bankAccountMapper.toEntity(dto);
    }
    public BankAccountDTO toDTO(BankAccount entity){
        return this.bankAccountMapper.toDto(entity);
    }

}
