package com.universalcode.org.safeeconomy.service.mapper;

import com.universalcode.org.safeeconomy.domain.*;
import com.universalcode.org.safeeconomy.service.dto.BinnacleDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Binnacle and its DTO BinnacleDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BinnacleMapper extends EntityMapper<BinnacleDTO, Binnacle> {



    default Binnacle fromId(Long id) {
        if (id == null) {
            return null;
        }
        Binnacle binnacle = new Binnacle();
        binnacle.setId(id);
        return binnacle;
    }
}
