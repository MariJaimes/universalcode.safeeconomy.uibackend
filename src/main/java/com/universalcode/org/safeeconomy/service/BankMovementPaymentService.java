package com.universalcode.org.safeeconomy.service;

import com.universalcode.org.safeeconomy.domain.BankAccount;
import com.universalcode.org.safeeconomy.domain.BankMovementPayment;
import com.universalcode.org.safeeconomy.domain.UserSaveEconomy;
import com.universalcode.org.safeeconomy.repository.BankMovementPaymentRepository;
import com.universalcode.org.safeeconomy.service.dto.BankMovementPaymentDTO;
import com.universalcode.org.safeeconomy.service.mapper.BankMovementPaymentMapper;
import com.universalcode.org.safeeconomy.service.mapper.BankAccountMapper;
import com.universalcode.org.safeeconomy.service.mapper.UserSaveEconomyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Service Implementation for managing BankMovementPayment.
 */
@Service
@Transactional
public class BankMovementPaymentService {

    private final Logger log = LoggerFactory.getLogger(BankMovementPaymentService.class);

    private final BankMovementPaymentRepository bankMovementPaymentRepository;

    private final BankMovementPaymentMapper bankMovementPaymentMapper;

    private final BankAccountMapper bankAccountMapper;

    private final BankAccountService bankAccountService;

    private final UserSaveEconomyService userSaveEconomyService;

    private final UserSaveEconomyMapper userSaveEconomyMapper;

    public BankMovementPaymentService(BankMovementPaymentRepository bankMovementPaymentRepository,
                                      BankMovementPaymentMapper bankMovementPaymentMapper,
                                      BankAccountMapper bankAccountMapper,
                                      BankAccountService bankAccountService,
                                      UserSaveEconomyService userSaveEconomyService,
                                      UserSaveEconomyMapper userSaveEconomyMapper) {

        this.bankMovementPaymentRepository = bankMovementPaymentRepository;
        this.bankMovementPaymentMapper = bankMovementPaymentMapper;
        this.bankAccountMapper = bankAccountMapper;
        this.bankAccountService = bankAccountService;
        this.userSaveEconomyMapper = userSaveEconomyMapper;
        this.userSaveEconomyService = userSaveEconomyService;
    }

    /**
     * Save a bankMovementPayment.
     *
     * @param bankMovementPaymentDTO the entity to save
     * @return the persisted entity
     */
    public BankMovementPaymentDTO save(BankMovementPaymentDTO bankMovementPaymentDTO) {
        log.debug("Request to save BankMovementPayment : {}", bankMovementPaymentDTO);
        BankMovementPayment bankMovementPayment = bankMovementPaymentMapper.toEntity(bankMovementPaymentDTO);
        bankMovementPayment = bankMovementPaymentRepository.save(bankMovementPayment);
        return bankMovementPaymentMapper.toDto(bankMovementPayment);
    }

    /**
     * Get all the bankMovementPayments.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BankMovementPaymentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BankMovementPayments");
        return bankMovementPaymentRepository.findAll(pageable)
            .map(bankMovementPaymentMapper::toDto);
    }

    @Transactional(readOnly = true)
    public List<BankMovementPaymentDTO> findAllMoves() {
        List<BankMovementPayment> listMovement = bankMovementPaymentRepository.findAll();
        return bankMovementPaymentMapper.toDto(listMovement);
    }

    @Transactional(readOnly = true)
    public List<BankMovementPaymentDTO> findAllMovesByAccount(Long id) {
        BankAccount bankAccount = this.bankAccountMapper.toEntity(this.bankAccountService.findOne(id));
        List<BankMovementPayment> listMovement = bankMovementPaymentRepository.findAllByBankAccountOrderByDateOfMovementDesc(bankAccount);
        return bankMovementPaymentMapper.toDto(listMovement);
    }

    @Transactional(readOnly = true)
    public List<BankMovementPaymentDTO> findAllMovesByUser(Long id) {
        UserSaveEconomy userSaveEconomy = this.userSaveEconomyMapper.toEntity(this.userSaveEconomyService.findOne(id));
        List<BankMovementPayment> listMovement = bankMovementPaymentRepository.findAllMovesByUserSaveEconomy(userSaveEconomy);
        return bankMovementPaymentMapper.toDto(listMovement);
    }

    @Transactional(readOnly = true)
    public List<BankMovementPaymentDTO> findAllMovesByUserAndActive(Long id) {
        UserSaveEconomy userSaveEconomy = this.userSaveEconomyMapper.toEntity(this.userSaveEconomyService.findOne(id));
        List<BankMovementPayment> listMovement = bankMovementPaymentRepository.findAllMovesByUserSaveEconomyAndBankAccount_ActiveOrderByDateOfMovementDesc(userSaveEconomy,true);
        return bankMovementPaymentMapper.toDto(listMovement);
    }

    /**
     * Get one bankMovementPayment by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public BankMovementPaymentDTO findOne(Long id) {
        log.debug("Request to get BankMovementPayment : {}", id);
        BankMovementPayment bankMovementPayment = bankMovementPaymentRepository.findOne(id);
        return bankMovementPaymentMapper.toDto(bankMovementPayment);
    }

    /**
     * Delete the bankMovementPayment by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete BankMovementPayment : {}", id);
        bankMovementPaymentRepository.delete(id);
    }
}
