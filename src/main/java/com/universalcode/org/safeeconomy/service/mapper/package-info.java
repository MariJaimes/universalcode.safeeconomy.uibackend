/**
 * MapStruct mappers for mapping domain objects and Data Transfer Objects.
 */
package com.universalcode.org.safeeconomy.service.mapper;
