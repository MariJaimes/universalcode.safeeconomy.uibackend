package com.universalcode.org.safeeconomy.service;

import com.universalcode.org.safeeconomy.domain.User;
import com.universalcode.org.safeeconomy.domain.UserSaveEconomy;
import com.universalcode.org.safeeconomy.repository.UserSaveEconomyRepository;
import com.universalcode.org.safeeconomy.service.dto.UserSaveEconomyDTO;
import com.universalcode.org.safeeconomy.service.mapper.UserSaveEconomyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing UserSaveEconomy.
 */
@Service
@Transactional
public class UserSaveEconomyService {

    private final Logger log = LoggerFactory.getLogger(UserSaveEconomyService.class);

    private final UserSaveEconomyRepository userSaveEconomyRepository;

    private static final int TOKEN_LENGTH = 6;

    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    private final UserSaveEconomyMapper userSaveEconomyMapper;

    private final UserService userService;

    private final MailService mailService;

    public UserSaveEconomyService(UserSaveEconomyRepository userSaveEconomyRepository,
                                  UserSaveEconomyMapper userSaveEconomyMapper,
                                  UserService userService,
                                  MailService mailService) {
        this.userSaveEconomyRepository = userSaveEconomyRepository;
        this.userSaveEconomyMapper = userSaveEconomyMapper;
        this.userService = userService;
        this.mailService = mailService;
    }

    /**
     * Save a userSaveEconomy.
     *
     * @param userSaveEconomyDTO the entity to save
     * @return the persisted entity
     */
    public UserSaveEconomyDTO save(UserSaveEconomyDTO userSaveEconomyDTO) {
        log.debug("Request to save UserSaveEconomy : {}", userSaveEconomyDTO);
        UserSaveEconomy userSaveEconomy = userSaveEconomyMapper.toEntity(userSaveEconomyDTO);
        userSaveEconomy = userSaveEconomyRepository.save(userSaveEconomy);
        return userSaveEconomyMapper.toDto(userSaveEconomy);
    }

    /**
     * Get all the userSaveEconomies.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UserSaveEconomyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserSaveEconomies");
        return userSaveEconomyRepository.findAll(pageable)
            .map(userSaveEconomyMapper::toDto);
    }

    /**
     * Get one userSaveEconomy by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public UserSaveEconomyDTO findOne(Long id) {
        log.debug("Request to get UserSaveEconomy : {}", id);
        UserSaveEconomy userSaveEconomy = userSaveEconomyRepository.findOne(id);
        return userSaveEconomyMapper.toDto(userSaveEconomy);
    }

    /**
     * Delete the userSaveEconomy by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete UserSaveEconomy : {}", id);
        userSaveEconomyRepository.delete(id);
    }
    public UserSaveEconomyDTO getSafeEconomyUserbyJhiUser(Long userJHIID){
        log.debug("Request to fecth UserSafeEconomy by JHI_User");
        User user =userService.getUserById(userJHIID);
        return userSaveEconomyMapper.toDto(userSaveEconomyRepository.getUserSaveEconomyByUser(user));
    }
    public UserSaveEconomyDTO getUserByLogin(String login) {
        User user = this.userService.getUserbyLogin(login);
        UserSaveEconomy userSE = this.userSaveEconomyRepository.getUserSaveEconomyByUser(user);
        if(user.getActivated()) {
            return this.userSaveEconomyMapper.toDto(this.sendTokenEmail(userSE, user));
        }else{
            this.mailService.sendReactivateMail(user);
        }
        return null;
    }
    public static String randomAlphaNumeric(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }
    public UserSaveEconomy sendTokenEmail (UserSaveEconomy userSafeEconomy, User user) {
        String token =  randomAlphaNumeric(TOKEN_LENGTH);
        userSafeEconomy.setToken(token);
        this.userSaveEconomyRepository.save(userSafeEconomy);
        this.mailService.sendTokentMail(userSafeEconomy, user);
        return userSafeEconomy;
    }
    public UserSaveEconomy toEntity(UserSaveEconomyDTO user){
        return this.userSaveEconomyMapper.toEntity(user);
    }

}
