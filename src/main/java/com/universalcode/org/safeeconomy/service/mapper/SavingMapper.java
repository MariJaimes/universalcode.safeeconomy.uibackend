package com.universalcode.org.safeeconomy.service.mapper;

import com.universalcode.org.safeeconomy.domain.*;
import com.universalcode.org.safeeconomy.service.dto.SavingDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Saving and its DTO SavingDTO.
 */
@Mapper(componentModel = "spring", uses = {UserSaveEconomyMapper.class, BankAccountMapper.class})
public interface SavingMapper extends EntityMapper<SavingDTO, Saving> {

    @Mapping(source = "userSaveEconomy.id", target = "userSaveEconomyId")
    @Mapping(source = "bankAccount.id", target = "bankAccountId")
    SavingDTO toDto(Saving saving);

    @Mapping(source = "userSaveEconomyId", target = "userSaveEconomy")
    @Mapping(source = "bankAccountId", target = "bankAccount")
    Saving toEntity(SavingDTO savingDTO);

    default Saving fromId(Long id) {
        if (id == null) {
            return null;
        }
        Saving saving = new Saving();
        saving.setId(id);
        return saving;
    }
}
