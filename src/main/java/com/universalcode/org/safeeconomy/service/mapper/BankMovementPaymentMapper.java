package com.universalcode.org.safeeconomy.service.mapper;

import com.universalcode.org.safeeconomy.domain.*;
import com.universalcode.org.safeeconomy.service.dto.BankMovementPaymentDTO;

import org.mapstruct.*;

import java.util.List;

/**
 * Mapper for the entity BankMovementPayment and its DTO BankMovementPaymentDTO.
 */
@Mapper(componentModel = "spring", uses = {UserSaveEconomyMapper.class, BankAccountMapper.class})
public interface BankMovementPaymentMapper extends EntityMapper<BankMovementPaymentDTO, BankMovementPayment> {

    List<BankMovementPayment>  toEntity(List<BankMovementPaymentDTO> dtoList);
    List<BankMovementPaymentDTO> toDto(List<BankMovementPayment> entityList);

    @Mapping(source = "userSaveEconomy.id", target = "userSaveEconomyId")
    @Mapping(source = "bankAccount.id", target = "bankAccountId")
    BankMovementPaymentDTO toDto(BankMovementPayment bankMovementPayment);

    @Mapping(source = "userSaveEconomyId", target = "userSaveEconomy")
    @Mapping(source = "bankAccountId", target = "bankAccount")
    BankMovementPayment toEntity(BankMovementPaymentDTO bankMovementPaymentDTO);

    default BankMovementPayment fromId(Long id) {
        if (id == null) {
            return null;
        }
        BankMovementPayment bankMovementPayment = new BankMovementPayment();
        bankMovementPayment.setId(id);
        return bankMovementPayment;
    }
}
