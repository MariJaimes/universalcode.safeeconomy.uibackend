/**
 * View Models used by Spring MVC REST controllers.
 */
package com.universalcode.org.safeeconomy.web.rest.vm;
