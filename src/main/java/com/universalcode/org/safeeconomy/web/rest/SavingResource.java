package com.universalcode.org.safeeconomy.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.universalcode.org.safeeconomy.domain.Saving;
import com.universalcode.org.safeeconomy.service.SavingService;
import com.universalcode.org.safeeconomy.service.UserSaveEconomyService;
import com.universalcode.org.safeeconomy.web.rest.errors.BadRequestAlertException;
import com.universalcode.org.safeeconomy.web.rest.util.HeaderUtil;
import com.universalcode.org.safeeconomy.web.rest.util.PaginationUtil;
import com.universalcode.org.safeeconomy.service.dto.SavingDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Saving.
 */
@RestController
@RequestMapping("/api")
public class SavingResource {

    private final Logger log = LoggerFactory.getLogger(SavingResource.class);

    private static final String ENTITY_NAME = "saving";

    private final SavingService savingService;

    private final UserSaveEconomyService userSaveEconomyService;

    public SavingResource(SavingService savingService,
                          UserSaveEconomyService userSaveEconomyService) {
        this.savingService = savingService;
        this.userSaveEconomyService = userSaveEconomyService;
    }

    /**
     * POST  /savings : Create a new saving.
     *
     * @param savingDTO the savingDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new savingDTO, or with status 400 (Bad Request) if the saving has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/savings")
    @Timed
    public ResponseEntity<SavingDTO> createSaving(@RequestBody SavingDTO savingDTO) throws URISyntaxException {
        log.debug("REST request to save Saving : {}", savingDTO);
        if (savingDTO.getId() != null) {
            throw new BadRequestAlertException("A new saving cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SavingDTO result = savingService.save(savingDTO);
        return ResponseEntity.created(new URI("/api/savings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /savings : Updates an existing saving.
     *
     * @param savingDTO the savingDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated savingDTO,
     * or with status 400 (Bad Request) if the savingDTO is not valid,
     * or with status 500 (Internal Server Error) if the savingDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/savings")
    @Timed
    public ResponseEntity<SavingDTO> updateSaving(@RequestBody SavingDTO savingDTO) throws URISyntaxException {
        log.debug("REST request to update Saving : {}", savingDTO);
        if (savingDTO.getId() == null) {
            return createSaving(savingDTO);
        }
        SavingDTO result = savingService.save(savingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, savingDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /savings : get all the savings.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of savings in body
     */
    @GetMapping("/savings")
    @Timed
    public ResponseEntity<List<SavingDTO>> getAllSavings(Pageable pageable) {
        log.debug("REST request to get a page of Savings");
        Page<SavingDTO> page = savingService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/savings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /savings/:id : get the "id" saving.
     *
     * @param id the id of the savingDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the savingDTO, or with status 404 (Not Found)
     */
    @GetMapping("/savings/{id}")
    @Timed
    public ResponseEntity<SavingDTO> getSaving(@PathVariable Long id) {
        log.debug("REST request to get Saving : {}", id);
        SavingDTO savingDTO = savingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(savingDTO));
    }
    @GetMapping("/savings-by-user/{id}")
    @Timed
    public ResponseEntity<List<SavingDTO>> getSavingsByUser(@PathVariable Long id) {
        log.debug("REST request to get Saving : {}", id);
        List<SavingDTO> savingDTOList = this.savingService.getSavingsByUser(id);
        Page<SavingDTO> page = new PageImpl<>(savingDTOList);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bank-accountsByUserpls");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * DELETE  /savings/:id : delete the "id" saving.
     *
     * @param id the id of the savingDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/savings/{id}")
    @Timed
    public ResponseEntity<Void> deleteSaving(@PathVariable Long id) {
        log.debug("REST request to delete Saving : {}", id);
        savingService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
