package com.universalcode.org.safeeconomy.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.universalcode.org.safeeconomy.service.BinnacleService;
import com.universalcode.org.safeeconomy.web.rest.errors.BadRequestAlertException;
import com.universalcode.org.safeeconomy.web.rest.util.HeaderUtil;
import com.universalcode.org.safeeconomy.web.rest.util.PaginationUtil;
import com.universalcode.org.safeeconomy.service.dto.BinnacleDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Binnacle.
 */
@RestController
@RequestMapping("/api")
public class BinnacleResource {

    private final Logger log = LoggerFactory.getLogger(BinnacleResource.class);

    private static final String ENTITY_NAME = "binnacle";

    private final BinnacleService binnacleService;

    public BinnacleResource(BinnacleService binnacleService) {
        this.binnacleService = binnacleService;
    }

    /**
     * POST  /binnacles : Create a new binnacle.
     *
     * @param binnacleDTO the binnacleDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new binnacleDTO, or with status 400 (Bad Request) if the binnacle has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/binnacles")
    @Timed
    public ResponseEntity<BinnacleDTO> createBinnacle(@RequestBody BinnacleDTO binnacleDTO) throws URISyntaxException {
        log.debug("REST request to save Binnacle : {}", binnacleDTO);
        if (binnacleDTO.getId() != null) {
            throw new BadRequestAlertException("A new binnacle cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BinnacleDTO result = binnacleService.save(binnacleDTO);
        return ResponseEntity.created(new URI("/api/binnacles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /binnacles : Updates an existing binnacle.
     *
     * @param binnacleDTO the binnacleDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated binnacleDTO,
     * or with status 400 (Bad Request) if the binnacleDTO is not valid,
     * or with status 500 (Internal Server Error) if the binnacleDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/binnacles")
    @Timed
    public ResponseEntity<BinnacleDTO> updateBinnacle(@RequestBody BinnacleDTO binnacleDTO) throws URISyntaxException {
        log.debug("REST request to update Binnacle : {}", binnacleDTO);
        if (binnacleDTO.getId() == null) {
            return createBinnacle(binnacleDTO);
        }
        BinnacleDTO result = binnacleService.save(binnacleDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, binnacleDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /binnacles : get all the binnacles.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of binnacles in body
     */
    @GetMapping("/binnacles")
    @Timed
    public ResponseEntity<List<BinnacleDTO>> getAllBinnacles(Pageable pageable) {
        log.debug("REST request to get a page of Binnacles");
        Page<BinnacleDTO> page = binnacleService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/binnacles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /binnacles/:id : get the "id" binnacle.
     *
     * @param id the id of the binnacleDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the binnacleDTO, or with status 404 (Not Found)
     */
    @GetMapping("/binnacles/{id}")
    @Timed
    public ResponseEntity<BinnacleDTO> getBinnacle(@PathVariable Long id) {
        log.debug("REST request to get Binnacle : {}", id);
        BinnacleDTO binnacleDTO = binnacleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(binnacleDTO));
    }

    /**
     * DELETE  /binnacles/:id : delete the "id" binnacle.
     *
     * @param id the id of the binnacleDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/binnacles/{id}")
    @Timed
    public ResponseEntity<Void> deleteBinnacle(@PathVariable Long id) {
        log.debug("REST request to delete Binnacle : {}", id);
        binnacleService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
