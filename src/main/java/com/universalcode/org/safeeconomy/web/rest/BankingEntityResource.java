package com.universalcode.org.safeeconomy.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.universalcode.org.safeeconomy.service.BankingEntityService;
import com.universalcode.org.safeeconomy.web.rest.errors.BadRequestAlertException;
import com.universalcode.org.safeeconomy.web.rest.util.HeaderUtil;
import com.universalcode.org.safeeconomy.web.rest.util.PaginationUtil;
import com.universalcode.org.safeeconomy.service.dto.BankingEntityDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing BankingEntity.
 */
@RestController
@RequestMapping("/api")
public class BankingEntityResource {

    private final Logger log = LoggerFactory.getLogger(BankingEntityResource.class);

    private static final String ENTITY_NAME = "bankingEntity";

    private final BankingEntityService bankingEntityService;

    public BankingEntityResource(BankingEntityService bankingEntityService) {
        this.bankingEntityService = bankingEntityService;
    }

    /**
     * POST  /banking-entities : Create a new bankingEntity.
     *
     * @param bankingEntityDTO the bankingEntityDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new bankingEntityDTO, or with status 400 (Bad Request) if the bankingEntity has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/banking-entities")
    @Timed
    public ResponseEntity<BankingEntityDTO> createBankingEntity(@RequestBody BankingEntityDTO bankingEntityDTO) throws URISyntaxException {
        log.debug("REST request to save BankingEntity : {}", bankingEntityDTO);
        if (bankingEntityDTO.getId() != null) {
            throw new BadRequestAlertException("A new bankingEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BankingEntityDTO result = bankingEntityService.save(bankingEntityDTO);
        return ResponseEntity.created(new URI("/api/banking-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /banking-entities : Updates an existing bankingEntity.
     *
     * @param bankingEntityDTO the bankingEntityDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated bankingEntityDTO,
     * or with status 400 (Bad Request) if the bankingEntityDTO is not valid,
     * or with status 500 (Internal Server Error) if the bankingEntityDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/banking-entities")
    @Timed
    public ResponseEntity<BankingEntityDTO> updateBankingEntity(@RequestBody BankingEntityDTO bankingEntityDTO) throws URISyntaxException {
        log.debug("REST request to update BankingEntity : {}", bankingEntityDTO);
        if (bankingEntityDTO.getId() == null) {
            return createBankingEntity(bankingEntityDTO);
        }
        BankingEntityDTO result = bankingEntityService.save(bankingEntityDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, bankingEntityDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /banking-entities : get all the bankingEntities.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of bankingEntities in body
     */
    @GetMapping("/banking-entities")
    @Timed
    public ResponseEntity<List<BankingEntityDTO>> getAllBankingEntities(Pageable pageable) {
        log.debug("REST request to get a page of BankingEntities");
        Page<BankingEntityDTO> page = bankingEntityService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/banking-entities");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /banking-entities/:id : get the "id" bankingEntity.
     *
     * @param id the id of the bankingEntityDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the bankingEntityDTO, or with status 404 (Not Found)
     */
    @GetMapping("/banking-entities/{id}")
    @Timed
    public ResponseEntity<BankingEntityDTO> getBankingEntity(@PathVariable Long id) {
        log.debug("REST request to get BankingEntity : {}", id);
        BankingEntityDTO bankingEntityDTO = bankingEntityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(bankingEntityDTO));
    }

    /**
     * DELETE  /banking-entities/:id : delete the "id" bankingEntity.
     *
     * @param id the id of the bankingEntityDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/banking-entities/{id}")
    @Timed
    public ResponseEntity<Void> deleteBankingEntity(@PathVariable Long id) {
        log.debug("REST request to delete BankingEntity : {}", id);
        bankingEntityService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/getAllBankEntity")
    @Timed
    public ResponseEntity<List<BankingEntityDTO>> getAllEntity() {

        List<BankingEntityDTO> listBankEntityDTO = this.bankingEntityService.findAllBankEntity();
        Page<BankingEntityDTO> page = new PageImpl<>(listBankEntityDTO);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/banking-entities");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    @GetMapping("/getAllBankEntitysByActive")
    @Timed
    public ResponseEntity<List<BankingEntityDTO>> getAllBankEntityByActive() {
        List<BankingEntityDTO> listBankEntityDTO = this.bankingEntityService.findAllBankEntityByActive();
        Page<BankingEntityDTO> page = new PageImpl<>(listBankEntityDTO);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/getAllBankEntitysByActive");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
