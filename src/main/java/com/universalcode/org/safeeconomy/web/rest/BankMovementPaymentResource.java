package com.universalcode.org.safeeconomy.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.universalcode.org.safeeconomy.domain.BankMovementPayment;
import com.universalcode.org.safeeconomy.service.BankMovementPaymentService;
import com.universalcode.org.safeeconomy.service.dto.BankAccountDTO;
import com.universalcode.org.safeeconomy.web.rest.errors.BadRequestAlertException;
import com.universalcode.org.safeeconomy.web.rest.util.HeaderUtil;
import com.universalcode.org.safeeconomy.web.rest.util.PaginationUtil;
import com.universalcode.org.safeeconomy.service.dto.BankMovementPaymentDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing BankMovementPayment.
 */
@RestController
@RequestMapping("/api")
public class BankMovementPaymentResource {

    private final Logger log = LoggerFactory.getLogger(BankMovementPaymentResource.class);

    private static final String ENTITY_NAME = "bankMovementPayment";

    private final BankMovementPaymentService bankMovementPaymentService;

    public BankMovementPaymentResource(BankMovementPaymentService bankMovementPaymentService) {
        this.bankMovementPaymentService = bankMovementPaymentService;
    }

    /**
     * POST  /bank-movement-payments : Create a new bankMovementPayment.
     *
     * @param bankMovementPaymentDTO the bankMovementPaymentDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new bankMovementPaymentDTO, or with status 400 (Bad Request) if the bankMovementPayment has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/bank-movement-payments")
    @Timed
    public ResponseEntity<BankMovementPaymentDTO> createBankMovementPayment(@RequestBody BankMovementPaymentDTO bankMovementPaymentDTO) throws URISyntaxException {
        log.debug("REST request to save BankMovementPayment : {}", bankMovementPaymentDTO);
        if (bankMovementPaymentDTO.getId() != null) {
            throw new BadRequestAlertException("A new bankMovementPayment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BankMovementPaymentDTO result = bankMovementPaymentService.save(bankMovementPaymentDTO);
        return ResponseEntity.created(new URI("/api/bank-movement-payments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /bank-movement-payments : Updates an existing bankMovementPayment.
     *
     * @param bankMovementPaymentDTO the bankMovementPaymentDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated bankMovementPaymentDTO,
     * or with status 400 (Bad Request) if the bankMovementPaymentDTO is not valid,
     * or with status 500 (Internal Server Error) if the bankMovementPaymentDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/bank-movement-payments")
    @Timed
    public ResponseEntity<BankMovementPaymentDTO> updateBankMovementPayment(@RequestBody BankMovementPaymentDTO bankMovementPaymentDTO) throws URISyntaxException {
        log.debug("REST request to update BankMovementPayment : {}", bankMovementPaymentDTO);
        if (bankMovementPaymentDTO.getId() == null) {
            return createBankMovementPayment(bankMovementPaymentDTO);
        }
        BankMovementPaymentDTO result = bankMovementPaymentService.save(bankMovementPaymentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, bankMovementPaymentDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /bank-movement-payments : get all the bankMovementPayments.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of bankMovementPayments in body
     */
    @GetMapping("/bank-movement-payments")
    @Timed
    public ResponseEntity<List<BankMovementPaymentDTO>> getAllBankMovementPayments(Pageable pageable) {
        log.debug("REST request to get a page of BankMovementPayments");
        Page<BankMovementPaymentDTO> page = bankMovementPaymentService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bank-movement-payments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/bank-movement-payments-by-account")
    @Timed
    public ResponseEntity<List<BankMovementPaymentDTO>> getAllBankMovementPaymentsByAccount(Pageable pageable) {
        log.debug("REST request to get a page of BankMovementPayments");
        Page<BankMovementPaymentDTO> page = bankMovementPaymentService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bank-movement-payments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /bank-movement-payments/:id : get the "id" bankMovementPayment.
     *
     * @param id the id of the bankMovementPaymentDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the bankMovementPaymentDTO, or with status 404 (Not Found)
     */
    @GetMapping("/bank-movement-payments/{id}")
    @Timed
    public ResponseEntity<BankMovementPaymentDTO> getBankMovementPayment(@PathVariable Long id) {
        log.debug("REST request to get BankMovementPayment : {}", id);
        BankMovementPaymentDTO bankMovementPaymentDTO = bankMovementPaymentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(bankMovementPaymentDTO));
    }

    /**
     * DELETE  /bank-movement-payments/:id : delete the "id" bankMovementPayment.
     *
     * @param id the id of the bankMovementPaymentDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/bank-movement-payments/{id}")
    @Timed
    public ResponseEntity<Void> deleteBankMovementPayment(@PathVariable Long id) {
        log.debug("REST request to delete BankMovementPayment : {}", id);
        bankMovementPaymentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/bank-movements/payments")
    @Timed
    public ResponseEntity<List<BankMovementPaymentDTO>> getAllBankMovementPayments() {

        List<BankMovementPaymentDTO> listMovementDTO = this.bankMovementPaymentService.findAllMoves();
        Page<BankMovementPaymentDTO> page = new PageImpl<>(listMovementDTO);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bank-movement-payments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/bank-movements-by-bankAccount/{id}")
    @Timed
    public ResponseEntity<List<BankMovementPaymentDTO>> getAllBankMovementPaymentsByBankAccount(@PathVariable Long id) {

        List<BankMovementPaymentDTO> listMovementDTO = this.bankMovementPaymentService.findAllMovesByAccount(id);
        Page<BankMovementPaymentDTO> page = new PageImpl<>(listMovementDTO);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bank-movement-payments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @GetMapping("/bank-movements-by-userSaveEconomy/{id}")
    @Timed
    public ResponseEntity<List<BankMovementPaymentDTO>> getAllBankMovementPaymentsByUserSaveEconomy(@PathVariable Long id) {

        List<BankMovementPaymentDTO> listMovementDTO = this.bankMovementPaymentService.findAllMovesByUser(id);
        Page<BankMovementPaymentDTO> page = new PageImpl<>(listMovementDTO);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/bank-movements-by-userSaveEconomy/{id}");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/bank-movements-by-userSaveEconomyAndActive/{id}")
    @Timed
    public ResponseEntity<List<BankMovementPaymentDTO>> getAllBankMovementPaymentsByUserSaveEconomyAndActive(@PathVariable Long id) {
        List<BankMovementPaymentDTO> listMovementDTO = this.bankMovementPaymentService.findAllMovesByUserAndActive(id);
        Page<BankMovementPaymentDTO> page = new PageImpl<>(listMovementDTO);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/bank-movements-by-userSaveEconomyAndActive/{id}");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
