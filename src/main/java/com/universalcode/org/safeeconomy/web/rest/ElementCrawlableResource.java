package com.universalcode.org.safeeconomy.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.universalcode.org.safeeconomy.service.ElementCrawlableService;
import com.universalcode.org.safeeconomy.web.rest.errors.BadRequestAlertException;
import com.universalcode.org.safeeconomy.web.rest.util.HeaderUtil;
import com.universalcode.org.safeeconomy.web.rest.util.PaginationUtil;
import com.universalcode.org.safeeconomy.service.dto.ElementCrawlableDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ElementCrawlable.
 */
@RestController
@RequestMapping("/api")
public class ElementCrawlableResource {

    private final Logger log = LoggerFactory.getLogger(ElementCrawlableResource.class);

    private static final String ENTITY_NAME = "elementCrawlable";

    private final ElementCrawlableService elementCrawlableService;

    public ElementCrawlableResource(ElementCrawlableService elementCrawlableService) {
        this.elementCrawlableService = elementCrawlableService;
    }

    /**
     * POST  /element-crawlables : Create a new elementCrawlable.
     *
     * @param elementCrawlableDTO the elementCrawlableDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new elementCrawlableDTO, or with status 400 (Bad Request) if the elementCrawlable has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/element-crawlables")
    @Timed
    public ResponseEntity<ElementCrawlableDTO> createElementCrawlable(@RequestBody ElementCrawlableDTO elementCrawlableDTO) throws URISyntaxException {
        log.debug("REST request to save ElementCrawlable : {}", elementCrawlableDTO);
        if (elementCrawlableDTO.getId() != null) {
            throw new BadRequestAlertException("A new elementCrawlable cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ElementCrawlableDTO result = elementCrawlableService.save(elementCrawlableDTO);
        return ResponseEntity.created(new URI("/api/element-crawlables/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /element-crawlables : Updates an existing elementCrawlable.
     *
     * @param elementCrawlableDTO the elementCrawlableDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated elementCrawlableDTO,
     * or with status 400 (Bad Request) if the elementCrawlableDTO is not valid,
     * or with status 500 (Internal Server Error) if the elementCrawlableDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/element-crawlables")
    @Timed
    public ResponseEntity<ElementCrawlableDTO> updateElementCrawlable(@RequestBody ElementCrawlableDTO elementCrawlableDTO) throws URISyntaxException {
        log.debug("REST request to update ElementCrawlable : {}", elementCrawlableDTO);
        if (elementCrawlableDTO.getId() == null) {
            return createElementCrawlable(elementCrawlableDTO);
        }
        ElementCrawlableDTO result = elementCrawlableService.save(elementCrawlableDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, elementCrawlableDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /element-crawlables : get all the elementCrawlables.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of elementCrawlables in body
     */
    @GetMapping("/element-crawlables")
    @Timed
    public ResponseEntity<List<ElementCrawlableDTO>> getAllElementCrawlables(Pageable pageable) {
        log.debug("REST request to get a page of ElementCrawlables");
        Page<ElementCrawlableDTO> page = elementCrawlableService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/element-crawlables");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /element-crawlables/:id : get the "id" elementCrawlable.
     *
     * @param id the id of the elementCrawlableDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the elementCrawlableDTO, or with status 404 (Not Found)
     */
    @GetMapping("/element-crawlables/{id}")
    @Timed
    public ResponseEntity<ElementCrawlableDTO> getElementCrawlable(@PathVariable Long id) {
        log.debug("REST request to get ElementCrawlable : {}", id);
        ElementCrawlableDTO elementCrawlableDTO = elementCrawlableService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(elementCrawlableDTO));
    }

    /**
     * DELETE  /element-crawlables/:id : delete the "id" elementCrawlable.
     *
     * @param id the id of the elementCrawlableDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/element-crawlables/{id}")
    @Timed
    public ResponseEntity<Void> deleteElementCrawlable(@PathVariable Long id) {
        log.debug("REST request to delete ElementCrawlable : {}", id);
        elementCrawlableService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
