package com.universalcode.org.safeeconomy.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.universalcode.org.safeeconomy.config.Constants;
import com.universalcode.org.safeeconomy.domain.UserSaveEconomy;
import com.universalcode.org.safeeconomy.service.UserSaveEconomyService;
import com.universalcode.org.safeeconomy.web.rest.errors.BadRequestAlertException;
import com.universalcode.org.safeeconomy.web.rest.util.HeaderUtil;
import com.universalcode.org.safeeconomy.web.rest.util.PaginationUtil;
import com.universalcode.org.safeeconomy.service.dto.UserSaveEconomyDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing UserSaveEconomy.
 */
@RestController
@RequestMapping("/api")
public class UserSaveEconomyResource {

    private final Logger log = LoggerFactory.getLogger(UserSaveEconomyResource.class);

    private static final String ENTITY_NAME = "userSaveEconomy";

    private final UserSaveEconomyService userSaveEconomyService;

    public UserSaveEconomyResource(UserSaveEconomyService userSaveEconomyService) {
        this.userSaveEconomyService = userSaveEconomyService;
    }

    /**
     * POST  /user-save-economies : Create a new userSaveEconomy.
     *
     * @param userSaveEconomyDTO the userSaveEconomyDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userSaveEconomyDTO, or with status 400 (Bad Request) if the userSaveEconomy has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/user-save-economies")
    @Timed
    public ResponseEntity<UserSaveEconomyDTO> createUserSaveEconomy(@RequestBody UserSaveEconomyDTO userSaveEconomyDTO) throws URISyntaxException {
        log.debug("REST request to save UserSaveEconomy : {}", userSaveEconomyDTO);
        if (userSaveEconomyDTO.getId() != null) {
            throw new BadRequestAlertException("A new userSaveEconomy cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserSaveEconomyDTO result = userSaveEconomyService.save(userSaveEconomyDTO);
        return ResponseEntity.created(new URI("/api/user-save-economies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /user-save-economies : Updates an existing userSaveEconomy.
     *
     * @param userSaveEconomyDTO the userSaveEconomyDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userSaveEconomyDTO,
     * or with status 400 (Bad Request) if the userSaveEconomyDTO is not valid,
     * or with status 500 (Internal Server Error) if the userSaveEconomyDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/user-save-economies")
    @Timed
    public ResponseEntity<UserSaveEconomyDTO> updateUserSaveEconomy(@RequestBody UserSaveEconomyDTO userSaveEconomyDTO) throws URISyntaxException {
        log.debug("REST request to update UserSaveEconomy : {}", userSaveEconomyDTO);
        if (userSaveEconomyDTO.getId() == null) {
            return createUserSaveEconomy(userSaveEconomyDTO);
        }
        UserSaveEconomyDTO result = userSaveEconomyService.save(userSaveEconomyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, userSaveEconomyDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /user-save-economies : get all the userSaveEconomies.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of userSaveEconomies in body
     */
    @GetMapping("/user-save-economies")
    @Timed
    public ResponseEntity<List<UserSaveEconomyDTO>> getAllUserSaveEconomies(Pageable pageable) {
        log.debug("REST request to get a page of UserSaveEconomies");
        Page<UserSaveEconomyDTO> page = userSaveEconomyService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/user-save-economies");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /user-save-economies/:id : get the "id" userSaveEconomy.
     *
     * @param id the id of the userSaveEconomyDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userSaveEconomyDTO, or with status 404 (Not Found)
     */
    @GetMapping("/user-save-economies/{id}")
    @Timed
    public ResponseEntity<UserSaveEconomyDTO> getUserSaveEconomy(@PathVariable Long id) {
        log.debug("REST request to get UserSaveEconomy : {}", id);
        UserSaveEconomyDTO userSaveEconomyDTO = userSaveEconomyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(userSaveEconomyDTO));
    }

    /**
     * DELETE  /user-save-economies/:id : delete the "id" userSaveEconomy.
     *
     * @param id the id of the userSaveEconomyDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/user-save-economies/{id}")
    @Timed
    public ResponseEntity<Void> deleteUserSaveEconomy(@PathVariable Long id) {
        log.debug("REST request to delete UserSaveEconomy : {}", id);
        userSaveEconomyService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/get-safe-economy-user-by-jhi-login/{login:" + Constants.LOGIN_REGEX + "}")
    @Timed
    public ResponseEntity<UserSaveEconomyDTO> getSafeEconomyUserByLogin(@PathVariable String login) {
        log.debug("REST request to Get UserSaveEconomy : {}", login,"by jhi_User");
        UserSaveEconomyDTO user = userSaveEconomyService.getUserByLogin(login);
        if(user == null){
            throw new ExceptionInInitializerError("No user");
        }
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(user));
    }

    @GetMapping("/get-safe-economy-user-by-jhi-user/{id}")
    @Timed
    public ResponseEntity<UserSaveEconomyDTO> getSafeEconomyUserByJHIUser(@PathVariable Long id) {
        log.debug("REST request to Get UserSaveEconomy : {}", id,"by jhi_User");
        UserSaveEconomyDTO user = userSaveEconomyService.getSafeEconomyUserbyJhiUser(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(user));
    }

}
