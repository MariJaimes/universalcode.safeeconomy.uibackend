package com.universalcode.org.safeeconomy.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.universalcode.org.safeeconomy.domain.KeyValue;
import com.universalcode.org.safeeconomy.service.KeyValueService;
import com.universalcode.org.safeeconomy.web.rest.errors.BadRequestAlertException;
import com.universalcode.org.safeeconomy.web.rest.util.HeaderUtil;
import com.universalcode.org.safeeconomy.web.rest.util.PaginationUtil;
import com.universalcode.org.safeeconomy.service.dto.KeyValueDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing KeyValue.
 */
@RestController
@RequestMapping("/api")
public class KeyValueResource {

    private final Logger log = LoggerFactory.getLogger(KeyValueResource.class);

    private static final String ENTITY_NAME = "keyValue";

    private final KeyValueService keyValueService;

    public KeyValueResource(KeyValueService keyValueService) {
        this.keyValueService = keyValueService;
    }

    /**
     * POST  /key-values : Create a new keyValue.
     *
     * @param keyValueDTO the keyValueDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new keyValueDTO, or with status 400 (Bad Request) if the keyValue has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/key-values")
    @Timed
    public ResponseEntity<KeyValueDTO> createKeyValue(@RequestBody KeyValueDTO keyValueDTO) throws URISyntaxException {
        log.debug("REST request to save KeyValue : {}", keyValueDTO);
        if (keyValueDTO.getId() != null) {
            throw new BadRequestAlertException("A new keyValue cannot already have an ID", ENTITY_NAME, "idexists");
        }
        KeyValueDTO result = keyValueService.save(keyValueDTO);
        return ResponseEntity.created(new URI("/api/key-values/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /key-values : Updates an existing keyValue.
     *
     * @param keyValueDTO the keyValueDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated keyValueDTO,
     * or with status 400 (Bad Request) if the keyValueDTO is not valid,
     * or with status 500 (Internal Server Error) if the keyValueDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/key-values")
    @Timed
    public ResponseEntity<KeyValueDTO> updateKeyValue(@RequestBody KeyValueDTO keyValueDTO) throws URISyntaxException {
        log.debug("REST request to update KeyValue : {}", keyValueDTO);
        if (keyValueDTO.getId() == null) {
            return createKeyValue(keyValueDTO);
        }
        KeyValueDTO result = keyValueService.save(keyValueDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, keyValueDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /key-values : get all the keyValues.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of keyValues in body
     */
    @GetMapping("/key-values")
    @Timed
    public ResponseEntity<List<KeyValueDTO>> getAllKeyValues(Pageable pageable) {
        log.debug("REST request to get a page of KeyValues");
        Page<KeyValueDTO> page = keyValueService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/key-values");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/key-valuespls")
    @Timed
    public ResponseEntity<List<KeyValueDTO>> findAllTypes() {
        List<KeyValueDTO> listKeyValueDTO = this.keyValueService.findAllTypes();
        Page<KeyValueDTO> page = new PageImpl<>(listKeyValueDTO);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/key-values");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /key-values/:id : get the "id" keyValue.
     *
     * @param id the id of the keyValueDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the keyValueDTO, or with status 404 (Not Found)
     */
    @GetMapping("/key-values/{id}")
    @Timed
    public ResponseEntity<KeyValueDTO> getKeyValue(@PathVariable Long id) {
        log.debug("REST request to get KeyValue : {}", id);
        KeyValueDTO keyValueDTO = keyValueService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(keyValueDTO));
    }

    /**
     * DELETE  /key-values/:id : delete the "id" keyValue.
     *
     * @param id the id of the keyValueDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/key-values/{id}")
    @Timed
    public ResponseEntity<Void> deleteKeyValue(@PathVariable Long id) {
        log.debug("REST request to delete KeyValue : {}", id);
        keyValueService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/getAllKeyValue")
    @Timed
    public ResponseEntity<List<KeyValueDTO>> getAllKeyValue() {

        List<KeyValueDTO> listBankEntityDTO = this.keyValueService.getAllKeyValue();
        Page<KeyValueDTO> page = new PageImpl<>(listBankEntityDTO);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/key-values");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
