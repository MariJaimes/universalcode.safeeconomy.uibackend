package com.universalcode.org.safeeconomy.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Loan.
 */
@Entity
@Table(name = "loan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Loan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "rate")
    private String rate;

    @Column(name = "is_monthly")
    private Boolean isMonthly;

    @Column(name = "is_paid")
    private Boolean isPaid;

    @Column(name = "finish_date")
    private Instant finishDate;

    @ManyToOne
    private UserSaveEconomy userSaveEconomy;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Loan name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Loan description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRate() {
        return rate;
    }

    public Loan rate(String rate) {
        this.rate = rate;
        return this;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public Boolean isIsMonthly() {
        return isMonthly;
    }

    public Loan isMonthly(Boolean isMonthly) {
        this.isMonthly = isMonthly;
        return this;
    }

    public void setIsMonthly(Boolean isMonthly) {
        this.isMonthly = isMonthly;
    }

    public Boolean isIsPaid() {
        return isPaid;
    }

    public Loan isPaid(Boolean isPaid) {
        this.isPaid = isPaid;
        return this;
    }

    public void setIsPaid(Boolean isPaid) {
        this.isPaid = isPaid;
    }

    public Instant getFinishDate() {
        return finishDate;
    }

    public Loan finishDate(Instant finishDate) {
        this.finishDate = finishDate;
        return this;
    }

    public void setFinishDate(Instant finishDate) {
        this.finishDate = finishDate;
    }

    public UserSaveEconomy getUserSaveEconomy() {
        return userSaveEconomy;
    }

    public Loan userSaveEconomy(UserSaveEconomy userSaveEconomy) {
        this.userSaveEconomy = userSaveEconomy;
        return this;
    }

    public void setUserSaveEconomy(UserSaveEconomy userSaveEconomy) {
        this.userSaveEconomy = userSaveEconomy;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Loan loan = (Loan) o;
        if (loan.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), loan.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Loan{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", rate='" + getRate() + "'" +
            ", isMonthly='" + isIsMonthly() + "'" +
            ", isPaid='" + isIsPaid() + "'" +
            ", finishDate='" + getFinishDate() + "'" +
            "}";
    }
}
