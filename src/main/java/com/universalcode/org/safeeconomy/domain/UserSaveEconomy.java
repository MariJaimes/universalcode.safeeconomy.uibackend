package com.universalcode.org.safeeconomy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A UserSaveEconomy.
 */
@Entity
@Table(name = "user_save_economy")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UserSaveEconomy implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "token")
    private String token;

    @Column(name = "last_visit_date")
    private Instant lastVisitDate;

    @Column(name = "image")
    private String image;

    @Column(name = "fact")
    private String fact;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @OneToMany(mappedBy = "userSaveEconomy")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<BankAccount> bankAccounts = new HashSet<>();

    @OneToMany(mappedBy = "userSaveEconomy")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<BankMovementPayment> paumenths = new HashSet<>();

    @OneToMany(mappedBy = "userSaveEconomy")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Loan> loans = new HashSet<>();

    @OneToMany(mappedBy = "userSaveEconomy")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Saving> savings = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public UserSaveEconomy name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public UserSaveEconomy lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTelephone() {
        return telephone;
    }

    public UserSaveEconomy telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getToken() {
        return token;
    }

    public UserSaveEconomy token(String token) {
        this.token = token;
        return this;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Instant getLastVisitDate() {
        return lastVisitDate;
    }

    public UserSaveEconomy lastVisitDate(Instant lastVisitDate) {
        this.lastVisitDate = lastVisitDate;
        return this;
    }

    public void setLastVisitDate(Instant lastVisitDate) {
        this.lastVisitDate = lastVisitDate;
    }

    public String getImage() {
        return image;
    }

    public UserSaveEconomy image(String image) {
        this.image = image;
        return this;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFact() {
        return fact;
    }

    public UserSaveEconomy fact(String fact) {
        this.fact = fact;
        return this;
    }

    public void setFact(String fact) {
        this.fact = fact;
    }

    public User getUser() {
        return user;
    }

    public UserSaveEconomy user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    public UserSaveEconomy bankAccounts(Set<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
        return this;
    }

    public UserSaveEconomy addBankAccounts(BankAccount bankAccount) {
        this.bankAccounts.add(bankAccount);
        bankAccount.setUserSaveEconomy(this);
        return this;
    }

    public UserSaveEconomy removeBankAccounts(BankAccount bankAccount) {
        this.bankAccounts.remove(bankAccount);
        bankAccount.setUserSaveEconomy(null);
        return this;
    }

    public void setBankAccounts(Set<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    public Set<BankMovementPayment> getPaumenths() {
        return paumenths;
    }

    public UserSaveEconomy paumenths(Set<BankMovementPayment> bankMovementPayments) {
        this.paumenths = bankMovementPayments;
        return this;
    }

    public UserSaveEconomy addPaumenths(BankMovementPayment bankMovementPayment) {
        this.paumenths.add(bankMovementPayment);
        bankMovementPayment.setUserSaveEconomy(this);
        return this;
    }

    public UserSaveEconomy removePaumenths(BankMovementPayment bankMovementPayment) {
        this.paumenths.remove(bankMovementPayment);
        bankMovementPayment.setUserSaveEconomy(null);
        return this;
    }

    public void setPaumenths(Set<BankMovementPayment> bankMovementPayments) {
        this.paumenths = bankMovementPayments;
    }

    public Set<Loan> getLoans() {
        return loans;
    }

    public UserSaveEconomy loans(Set<Loan> loans) {
        this.loans = loans;
        return this;
    }

    public UserSaveEconomy addLoans(Loan loan) {
        this.loans.add(loan);
        loan.setUserSaveEconomy(this);
        return this;
    }

    public UserSaveEconomy removeLoans(Loan loan) {
        this.loans.remove(loan);
        loan.setUserSaveEconomy(null);
        return this;
    }

    public void setLoans(Set<Loan> loans) {
        this.loans = loans;
    }

    public Set<Saving> getSavings() {
        return savings;
    }

    public UserSaveEconomy savings(Set<Saving> savings) {
        this.savings = savings;
        return this;
    }

    public UserSaveEconomy addSavings(Saving saving) {
        this.savings.add(saving);
        saving.setUserSaveEconomy(this);
        return this;
    }

    public UserSaveEconomy removeSavings(Saving saving) {
        this.savings.remove(saving);
        saving.setUserSaveEconomy(null);
        return this;
    }

    public void setSavings(Set<Saving> savings) {
        this.savings = savings;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserSaveEconomy userSaveEconomy = (UserSaveEconomy) o;
        if (userSaveEconomy.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userSaveEconomy.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserSaveEconomy{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", telephone='" + getTelephone() + "'" +
            ", token='" + getToken() + "'" +
            ", lastVisitDate='" + getLastVisitDate() + "'" +
            ", image='" + getImage() + "'" +
            ", fact='" + getFact() + "'" +
            "}";
    }
}
