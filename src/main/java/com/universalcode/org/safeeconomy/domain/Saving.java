package com.universalcode.org.safeeconomy.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Saving.
 */
@Entity
@Table(name = "saving")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Saving implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "description")
    private String description;

    @Column(name = "date_to_finish")
    private Instant dateToFinish;

    @Column(name = "ammount")
    private Double ammount;

    @Column(name = "saving_percentage")
    private Double savingPercentage;

    @ManyToOne
    private UserSaveEconomy userSaveEconomy;

    @ManyToOne
    private BankAccount bankAccount;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public Saving description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Instant getDateToFinish() {
        return dateToFinish;
    }

    public Saving dateToFinish(Instant dateToFinish) {
        this.dateToFinish = dateToFinish;
        return this;
    }

    public void setDateToFinish(Instant dateToFinish) {
        this.dateToFinish = dateToFinish;
    }

    public Double getAmmount() {
        return ammount;
    }

    public Saving ammount(Double ammount) {
        this.ammount = ammount;
        return this;
    }

    public void setAmmount(Double ammount) {
        this.ammount = ammount;
    }

    public Double getSavingPercentage() {
        return savingPercentage;
    }

    public Saving savingPercentage(Double savingPercentage) {
        this.savingPercentage = savingPercentage;
        return this;
    }

    public void setSavingPercentage(Double savingPercentage) {
        this.savingPercentage = savingPercentage;
    }

    public UserSaveEconomy getUserSaveEconomy() {
        return userSaveEconomy;
    }

    public Saving userSaveEconomy(UserSaveEconomy userSaveEconomy) {
        this.userSaveEconomy = userSaveEconomy;
        return this;
    }

    public void setUserSaveEconomy(UserSaveEconomy userSaveEconomy) {
        this.userSaveEconomy = userSaveEconomy;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public Saving bankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
        return this;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Saving saving = (Saving) o;
        if (saving.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), saving.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Saving{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", dateToFinish='" + getDateToFinish() + "'" +
            ", ammount=" + getAmmount() +
            ", savingPercentage=" + getSavingPercentage() +
            "}";
    }
}
