package com.universalcode.org.safeeconomy.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A BankMovementPayment.
 */
@Entity
@Table(name = "bank_movement_payment")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class BankMovementPayment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "kind_of_movement")
    private String kindOfMovement;

    @Column(name = "date_of_movement")
    private Instant dateOfMovement;

    @Column(name = "description")
    private String description;

    @Column(name = "acreditation_ammount")
    private Double acreditationAmmount;

    @Column(name = "is_paid")
    private Boolean isPaid;

    @Column(name = "jhi_comment")
    private String comment;

    @Column(name = "priority")
    private Integer priority;

    @ManyToOne
    private UserSaveEconomy userSaveEconomy;

    @ManyToOne
    private BankAccount bankAccount;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKindOfMovement() {
        return kindOfMovement;
    }

    public BankMovementPayment kindOfMovement(String kindOfMovement) {
        this.kindOfMovement = kindOfMovement;
        return this;
    }

    public void setKindOfMovement(String kindOfMovement) {
        this.kindOfMovement = kindOfMovement;
    }

    public Instant getDateOfMovement() {
        return dateOfMovement;
    }

    public BankMovementPayment dateOfMovement(Instant dateOfMovement) {
        this.dateOfMovement = dateOfMovement;
        return this;
    }

    public void setDateOfMovement(Instant dateOfMovement) {
        this.dateOfMovement = dateOfMovement;
    }

    public String getDescription() {
        return description;
    }

    public BankMovementPayment description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getAcreditationAmmount() {
        return acreditationAmmount;
    }

    public BankMovementPayment acreditationAmmount(Double acreditationAmmount) {
        this.acreditationAmmount = acreditationAmmount;
        return this;
    }

    public void setAcreditationAmmount(Double acreditationAmmount) {
        this.acreditationAmmount = acreditationAmmount;
    }

    public Boolean isIsPaid() {
        return isPaid;
    }

    public BankMovementPayment isPaid(Boolean isPaid) {
        this.isPaid = isPaid;
        return this;
    }

    public void setIsPaid(Boolean isPaid) {
        this.isPaid = isPaid;
    }

    public String getComment() {
        return comment;
    }

    public BankMovementPayment comment(String comment) {
        this.comment = comment;
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getPriority() {
        return priority;
    }

    public BankMovementPayment priority(Integer priority) {
        this.priority = priority;
        return this;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public UserSaveEconomy getUserSaveEconomy() {
        return userSaveEconomy;
    }

    public BankMovementPayment userSaveEconomy(UserSaveEconomy userSaveEconomy) {
        this.userSaveEconomy = userSaveEconomy;
        return this;
    }

    public void setUserSaveEconomy(UserSaveEconomy userSaveEconomy) {
        this.userSaveEconomy = userSaveEconomy;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public BankMovementPayment bankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
        return this;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BankMovementPayment bankMovementPayment = (BankMovementPayment) o;
        if (bankMovementPayment.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bankMovementPayment.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BankMovementPayment{" +
            "id=" + getId() +
            ", kindOfMovement='" + getKindOfMovement() + "'" +
            ", dateOfMovement='" + getDateOfMovement() + "'" +
            ", description='" + getDescription() + "'" +
            ", acreditationAmmount=" + getAcreditationAmmount() +
            ", isPaid='" + isIsPaid() + "'" +
            ", comment='" + getComment() + "'" +
            ", priority=" + getPriority() +
            "}";
    }
}
