package com.universalcode.org.safeeconomy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A BankAccount.
 */
@Entity
@Table(name = "bank_account")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class BankAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "jhi_password")
    private String password;

    @Column(name = "token")
    private String token;

    @Column(name = "mont")
    private Double mont;

    @Column(name = "budget")
    private Double budget;

    @Column(name = "bank")
    private String bank;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "type_coin")
    private String typeCoin;

    @ManyToOne
    private UserSaveEconomy userSaveEconomy;

    @OneToMany(mappedBy = "bankAccount")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<BankMovementPayment> movements = new HashSet<>();

    @OneToMany(mappedBy = "bankAccount")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Saving> savings = new HashSet<>();

    @ManyToOne
    private BankingEntity bankEntity;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public BankAccount userName(String userName) {
        this.userName = userName;
        return this;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public BankAccount password(String password) {
        this.password = password;
        return this;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public BankAccount token(String token) {
        this.token = token;
        return this;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Double getMont() {
        return mont;
    }

    public BankAccount mont(Double mont) {
        this.mont = mont;
        return this;
    }

    public void setMont(Double mont) {
        this.mont = mont;
    }

    public Double getBudget() {
        return budget;
    }

    public BankAccount budget(Double budget) {
        this.budget = budget;
        return this;
    }

    public void setBudget(Double budget) {
        this.budget = budget;
    }

    public String getBank() {
        return bank;
    }

    public BankAccount bank(String bank) {
        this.bank = bank;
        return this;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public Boolean isActive() {
        return active;
    }

    public BankAccount active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getTypeCoin() {
        return typeCoin;
    }

    public BankAccount typeCoin(String typeCoin) {
        this.typeCoin = typeCoin;
        return this;
    }

    public void setTypeCoin(String typeCoin) {
        this.typeCoin = typeCoin;
    }

    public UserSaveEconomy getUserSaveEconomy() {
        return userSaveEconomy;
    }

    public BankAccount userSaveEconomy(UserSaveEconomy userSaveEconomy) {
        this.userSaveEconomy = userSaveEconomy;
        return this;
    }

    public void setUserSaveEconomy(UserSaveEconomy userSaveEconomy) {
        this.userSaveEconomy = userSaveEconomy;
    }

    public Set<BankMovementPayment> getMovements() {
        return movements;
    }

    public BankAccount movements(Set<BankMovementPayment> bankMovementPayments) {
        this.movements = bankMovementPayments;
        return this;
    }

    public BankAccount addMovements(BankMovementPayment bankMovementPayment) {
        this.movements.add(bankMovementPayment);
        bankMovementPayment.setBankAccount(this);
        return this;
    }

    public BankAccount removeMovements(BankMovementPayment bankMovementPayment) {
        this.movements.remove(bankMovementPayment);
        bankMovementPayment.setBankAccount(null);
        return this;
    }

    public void setMovements(Set<BankMovementPayment> bankMovementPayments) {
        this.movements = bankMovementPayments;
    }

    public Set<Saving> getSavings() {
        return savings;
    }

    public BankAccount savings(Set<Saving> savings) {
        this.savings = savings;
        return this;
    }

    public BankAccount addSavings(Saving saving) {
        this.savings.add(saving);
        saving.setBankAccount(this);
        return this;
    }

    public BankAccount removeSavings(Saving saving) {
        this.savings.remove(saving);
        saving.setBankAccount(null);
        return this;
    }

    public void setSavings(Set<Saving> savings) {
        this.savings = savings;
    }

    public BankingEntity getBankEntity() {
        return bankEntity;
    }

    public BankAccount bankEntity(BankingEntity bankingEntity) {
        this.bankEntity = bankingEntity;
        return this;
    }

    public void setBankEntity(BankingEntity bankingEntity) {
        this.bankEntity = bankingEntity;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BankAccount bankAccount = (BankAccount) o;
        if (bankAccount.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bankAccount.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BankAccount{" +
            "id=" + getId() +
            ", userName='" + getUserName() + "'" +
            ", password='" + getPassword() + "'" +
            ", token='" + getToken() + "'" +
            ", mont=" + getMont() +
            ", budget=" + getBudget() +
            ", bank='" + getBank() + "'" +
            ", active='" + isActive() + "'" +
            ", typeCoin='" + getTypeCoin() + "'" +
            "}";
    }
}
