package com.universalcode.org.safeeconomy.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A ElementCrawlable.
 */
@Entity
@Table(name = "element_crawlable")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ElementCrawlable implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "bank_id")
    private Integer bankId;

    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "url")
    private String url;

    @Column(name = "was_processed")
    private Boolean wasProcessed;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getBankId() {
        return bankId;
    }

    public ElementCrawlable bankId(Integer bankId) {
        this.bankId = bankId;
        return this;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public Integer getUserId() {
        return userId;
    }

    public ElementCrawlable userId(Integer userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUrl() {
        return url;
    }

    public ElementCrawlable url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean isWasProcessed() {
        return wasProcessed;
    }

    public ElementCrawlable wasProcessed(Boolean wasProcessed) {
        this.wasProcessed = wasProcessed;
        return this;
    }

    public void setWasProcessed(Boolean wasProcessed) {
        this.wasProcessed = wasProcessed;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ElementCrawlable elementCrawlable = (ElementCrawlable) o;
        if (elementCrawlable.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), elementCrawlable.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ElementCrawlable{" +
            "id=" + getId() +
            ", bankId=" + getBankId() +
            ", userId=" + getUserId() +
            ", url='" + getUrl() + "'" +
            ", wasProcessed='" + isWasProcessed() + "'" +
            "}";
    }
}
