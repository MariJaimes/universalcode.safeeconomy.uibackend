package com.universalcode.org.safeeconomy.config;

import io.github.jhipster.config.JHipsterProperties;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;

import java.util.concurrent.TimeUnit;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(com.universalcode.org.safeeconomy.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(com.universalcode.org.safeeconomy.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(com.universalcode.org.safeeconomy.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.universalcode.org.safeeconomy.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(com.universalcode.org.safeeconomy.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.universalcode.org.safeeconomy.domain.SocialUserConnection.class.getName(), jcacheConfiguration);
            cm.createCache(com.universalcode.org.safeeconomy.domain.Binnacle.class.getName(), jcacheConfiguration);
            cm.createCache(com.universalcode.org.safeeconomy.domain.UserSaveEconomy.class.getName(), jcacheConfiguration);
            cm.createCache(com.universalcode.org.safeeconomy.domain.UserSaveEconomy.class.getName() + ".bankAccounts", jcacheConfiguration);
            cm.createCache(com.universalcode.org.safeeconomy.domain.UserSaveEconomy.class.getName() + ".paumenths", jcacheConfiguration);
            cm.createCache(com.universalcode.org.safeeconomy.domain.UserSaveEconomy.class.getName() + ".loans", jcacheConfiguration);
            cm.createCache(com.universalcode.org.safeeconomy.domain.BankAccount.class.getName(), jcacheConfiguration);
            cm.createCache(com.universalcode.org.safeeconomy.domain.BankAccount.class.getName() + ".movements", jcacheConfiguration);
            cm.createCache(com.universalcode.org.safeeconomy.domain.BankAccount.class.getName() + ".savings", jcacheConfiguration);
            cm.createCache(com.universalcode.org.safeeconomy.domain.Loan.class.getName(), jcacheConfiguration);
            cm.createCache(com.universalcode.org.safeeconomy.domain.BankMovementPayment.class.getName(), jcacheConfiguration);
            cm.createCache(com.universalcode.org.safeeconomy.domain.BankingEntity.class.getName(), jcacheConfiguration);
            cm.createCache(com.universalcode.org.safeeconomy.domain.KeyValue.class.getName(), jcacheConfiguration);
            cm.createCache(com.universalcode.org.safeeconomy.domain.Saving.class.getName(), jcacheConfiguration);
            cm.createCache(com.universalcode.org.safeeconomy.domain.ElementCrawlable.class.getName(), jcacheConfiguration);
            cm.createCache(com.universalcode.org.safeeconomy.domain.UserSaveEconomy.class.getName() + ".savings", jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
