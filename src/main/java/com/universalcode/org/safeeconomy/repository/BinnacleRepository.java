package com.universalcode.org.safeeconomy.repository;

import com.universalcode.org.safeeconomy.domain.Binnacle;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Binnacle entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BinnacleRepository extends JpaRepository<Binnacle, Long> {

}
