package com.universalcode.org.safeeconomy.repository;

import afu.org.checkerframework.checker.oigj.qual.O;
import com.universalcode.org.safeeconomy.domain.BankAccount;
import com.universalcode.org.safeeconomy.domain.BankMovementPayment;
import com.universalcode.org.safeeconomy.domain.UserSaveEconomy;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the BankMovementPayment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BankMovementPaymentRepository extends JpaRepository<BankMovementPayment, Long> {

    @Override
    List<BankMovementPayment> findAll();
    List<BankMovementPayment> findAllByBankAccount(BankAccount bankAccount);
    List<BankMovementPayment> findAllByBankAccountOrderByDateOfMovementDesc(BankAccount bankAccount);
    List<BankMovementPayment> findAllMovesByUserSaveEconomy(UserSaveEconomy userSaveEconomy);
    List<BankMovementPayment> findAllMovesByUserSaveEconomyAndBankAccount_ActiveOrderByDateOfMovementDesc(UserSaveEconomy userSaveEconomy,boolean Active);
}
