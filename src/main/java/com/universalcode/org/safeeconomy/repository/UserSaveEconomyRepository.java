package com.universalcode.org.safeeconomy.repository;

import com.universalcode.org.safeeconomy.domain.Saving;
import com.universalcode.org.safeeconomy.domain.User;
import com.universalcode.org.safeeconomy.domain.UserSaveEconomy;
import com.universalcode.org.safeeconomy.service.dto.UserSaveEconomyDTO;
import org.springframework.data.domain.Example;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the UserSaveEconomy entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserSaveEconomyRepository extends JpaRepository<UserSaveEconomy, Long> {

    UserSaveEconomy getUserSaveEconomyByUser(User user);

}
