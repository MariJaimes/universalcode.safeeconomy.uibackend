package com.universalcode.org.safeeconomy.repository;

import com.universalcode.org.safeeconomy.domain.BankAccount;
import com.universalcode.org.safeeconomy.domain.Saving;
import com.universalcode.org.safeeconomy.domain.UserSaveEconomy;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**`
 * Spring Data JPA repository for the Saving entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SavingRepository extends JpaRepository<Saving, Long> {

    List<Saving> getSavingsByUserSaveEconomy(UserSaveEconomy user);
    List<Saving> getSavingsByBankAccount(BankAccount bankAccount);

}
