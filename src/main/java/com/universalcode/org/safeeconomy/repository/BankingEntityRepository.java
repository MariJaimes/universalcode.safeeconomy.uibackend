package com.universalcode.org.safeeconomy.repository;

import com.universalcode.org.safeeconomy.domain.BankingEntity;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the BankingEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BankingEntityRepository extends JpaRepository<BankingEntity, Long> {
    @Override
    List<BankingEntity> findAll();
    List<BankingEntity> findAllBankEntityByActive(boolean active);

}
