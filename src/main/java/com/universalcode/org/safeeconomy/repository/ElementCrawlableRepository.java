package com.universalcode.org.safeeconomy.repository;

import com.universalcode.org.safeeconomy.domain.ElementCrawlable;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ElementCrawlable entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ElementCrawlableRepository extends JpaRepository<ElementCrawlable, Long> {

}
