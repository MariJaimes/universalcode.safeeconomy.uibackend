package com.universalcode.org.safeeconomy.repository;

import com.universalcode.org.safeeconomy.domain.KeyValue;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the KeyValue entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KeyValueRepository extends JpaRepository<KeyValue, Long> {
    @Override
    List<KeyValue> findAll();
}
