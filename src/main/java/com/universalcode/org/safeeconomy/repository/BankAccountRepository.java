package com.universalcode.org.safeeconomy.repository;

import com.universalcode.org.safeeconomy.domain.BankAccount;
import com.universalcode.org.safeeconomy.domain.BankingEntity;
import com.universalcode.org.safeeconomy.domain.UserSaveEconomy;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the BankAccount entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {
    BankAccount getBankAccountByBankEntityAndUserSaveEconomy( BankingEntity bank, UserSaveEconomy user);
    List<BankAccount> findAllByUserSaveEconomy(UserSaveEconomy user);
    List<BankAccount> findAllByUserSaveEconomyId(Long id);
    List<BankAccount> findAllByUserSaveEconomyIdAndActive(Long id,Boolean active);

}
