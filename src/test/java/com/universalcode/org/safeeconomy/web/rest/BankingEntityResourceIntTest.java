package com.universalcode.org.safeeconomy.web.rest;

import com.universalcode.org.safeeconomy.SafeEconomyApp;

import com.universalcode.org.safeeconomy.domain.BankingEntity;
import com.universalcode.org.safeeconomy.repository.BankingEntityRepository;
import com.universalcode.org.safeeconomy.service.BankingEntityService;
import com.universalcode.org.safeeconomy.service.dto.BankingEntityDTO;
import com.universalcode.org.safeeconomy.service.mapper.BankingEntityMapper;
import com.universalcode.org.safeeconomy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.universalcode.org.safeeconomy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the BankingEntityResource REST controller.
 *
 * @see BankingEntityResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SafeEconomyApp.class)
public class BankingEntityResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final String DEFAULT_LOGO = "AAAAAAAAAA";
    private static final String UPDATED_LOGO = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    @Autowired
    private BankingEntityRepository bankingEntityRepository;

    @Autowired
    private BankingEntityMapper bankingEntityMapper;

    @Autowired
    private BankingEntityService bankingEntityService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restBankingEntityMockMvc;

    private BankingEntity bankingEntity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BankingEntityResource bankingEntityResource = new BankingEntityResource(bankingEntityService);
        this.restBankingEntityMockMvc = MockMvcBuilders.standaloneSetup(bankingEntityResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BankingEntity createEntity(EntityManager em) {
        BankingEntity bankingEntity = new BankingEntity()
            .name(DEFAULT_NAME)
            .url(DEFAULT_URL)
            .logo(DEFAULT_LOGO)
            .active(DEFAULT_ACTIVE);
        return bankingEntity;
    }

    @Before
    public void initTest() {
        bankingEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createBankingEntity() throws Exception {
        int databaseSizeBeforeCreate = bankingEntityRepository.findAll().size();

        // Create the BankingEntity
        BankingEntityDTO bankingEntityDTO = bankingEntityMapper.toDto(bankingEntity);
        restBankingEntityMockMvc.perform(post("/api/banking-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankingEntityDTO)))
            .andExpect(status().isCreated());

        // Validate the BankingEntity in the database
        List<BankingEntity> bankingEntityList = bankingEntityRepository.findAll();
        assertThat(bankingEntityList).hasSize(databaseSizeBeforeCreate + 1);
        BankingEntity testBankingEntity = bankingEntityList.get(bankingEntityList.size() - 1);
        assertThat(testBankingEntity.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testBankingEntity.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testBankingEntity.getLogo()).isEqualTo(DEFAULT_LOGO);
        assertThat(testBankingEntity.isActive()).isEqualTo(DEFAULT_ACTIVE);
    }

    @Test
    @Transactional
    public void createBankingEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bankingEntityRepository.findAll().size();

        // Create the BankingEntity with an existing ID
        bankingEntity.setId(1L);
        BankingEntityDTO bankingEntityDTO = bankingEntityMapper.toDto(bankingEntity);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBankingEntityMockMvc.perform(post("/api/banking-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankingEntityDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BankingEntity in the database
        List<BankingEntity> bankingEntityList = bankingEntityRepository.findAll();
        assertThat(bankingEntityList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllBankingEntities() throws Exception {
        // Initialize the database
        bankingEntityRepository.saveAndFlush(bankingEntity);

        // Get all the bankingEntityList
        restBankingEntityMockMvc.perform(get("/api/banking-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bankingEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].logo").value(hasItem(DEFAULT_LOGO.toString())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }

    @Test
    @Transactional
    public void getBankingEntity() throws Exception {
        // Initialize the database
        bankingEntityRepository.saveAndFlush(bankingEntity);

        // Get the bankingEntity
        restBankingEntityMockMvc.perform(get("/api/banking-entities/{id}", bankingEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bankingEntity.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.logo").value(DEFAULT_LOGO.toString()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingBankingEntity() throws Exception {
        // Get the bankingEntity
        restBankingEntityMockMvc.perform(get("/api/banking-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBankingEntity() throws Exception {
        // Initialize the database
        bankingEntityRepository.saveAndFlush(bankingEntity);
        int databaseSizeBeforeUpdate = bankingEntityRepository.findAll().size();

        // Update the bankingEntity
        BankingEntity updatedBankingEntity = bankingEntityRepository.findOne(bankingEntity.getId());
        // Disconnect from session so that the updates on updatedBankingEntity are not directly saved in db
        em.detach(updatedBankingEntity);
        updatedBankingEntity
            .name(UPDATED_NAME)
            .url(UPDATED_URL)
            .logo(UPDATED_LOGO)
            .active(UPDATED_ACTIVE);
        BankingEntityDTO bankingEntityDTO = bankingEntityMapper.toDto(updatedBankingEntity);

        restBankingEntityMockMvc.perform(put("/api/banking-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankingEntityDTO)))
            .andExpect(status().isOk());

        // Validate the BankingEntity in the database
        List<BankingEntity> bankingEntityList = bankingEntityRepository.findAll();
        assertThat(bankingEntityList).hasSize(databaseSizeBeforeUpdate);
        BankingEntity testBankingEntity = bankingEntityList.get(bankingEntityList.size() - 1);
        assertThat(testBankingEntity.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testBankingEntity.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testBankingEntity.getLogo()).isEqualTo(UPDATED_LOGO);
        assertThat(testBankingEntity.isActive()).isEqualTo(UPDATED_ACTIVE);
    }

    @Test
    @Transactional
    public void updateNonExistingBankingEntity() throws Exception {
        int databaseSizeBeforeUpdate = bankingEntityRepository.findAll().size();

        // Create the BankingEntity
        BankingEntityDTO bankingEntityDTO = bankingEntityMapper.toDto(bankingEntity);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restBankingEntityMockMvc.perform(put("/api/banking-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankingEntityDTO)))
            .andExpect(status().isCreated());

        // Validate the BankingEntity in the database
        List<BankingEntity> bankingEntityList = bankingEntityRepository.findAll();
        assertThat(bankingEntityList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteBankingEntity() throws Exception {
        // Initialize the database
        bankingEntityRepository.saveAndFlush(bankingEntity);
        int databaseSizeBeforeDelete = bankingEntityRepository.findAll().size();

        // Get the bankingEntity
        restBankingEntityMockMvc.perform(delete("/api/banking-entities/{id}", bankingEntity.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<BankingEntity> bankingEntityList = bankingEntityRepository.findAll();
        assertThat(bankingEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BankingEntity.class);
        BankingEntity bankingEntity1 = new BankingEntity();
        bankingEntity1.setId(1L);
        BankingEntity bankingEntity2 = new BankingEntity();
        bankingEntity2.setId(bankingEntity1.getId());
        assertThat(bankingEntity1).isEqualTo(bankingEntity2);
        bankingEntity2.setId(2L);
        assertThat(bankingEntity1).isNotEqualTo(bankingEntity2);
        bankingEntity1.setId(null);
        assertThat(bankingEntity1).isNotEqualTo(bankingEntity2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BankingEntityDTO.class);
        BankingEntityDTO bankingEntityDTO1 = new BankingEntityDTO();
        bankingEntityDTO1.setId(1L);
        BankingEntityDTO bankingEntityDTO2 = new BankingEntityDTO();
        assertThat(bankingEntityDTO1).isNotEqualTo(bankingEntityDTO2);
        bankingEntityDTO2.setId(bankingEntityDTO1.getId());
        assertThat(bankingEntityDTO1).isEqualTo(bankingEntityDTO2);
        bankingEntityDTO2.setId(2L);
        assertThat(bankingEntityDTO1).isNotEqualTo(bankingEntityDTO2);
        bankingEntityDTO1.setId(null);
        assertThat(bankingEntityDTO1).isNotEqualTo(bankingEntityDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(bankingEntityMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(bankingEntityMapper.fromId(null)).isNull();
    }
}
