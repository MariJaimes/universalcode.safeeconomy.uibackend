package com.universalcode.org.safeeconomy.web.rest;

import com.universalcode.org.safeeconomy.SafeEconomyApp;

import com.universalcode.org.safeeconomy.domain.BankMovementPayment;
import com.universalcode.org.safeeconomy.repository.BankMovementPaymentRepository;
import com.universalcode.org.safeeconomy.service.BankMovementPaymentService;
import com.universalcode.org.safeeconomy.service.dto.BankMovementPaymentDTO;
import com.universalcode.org.safeeconomy.service.mapper.BankMovementPaymentMapper;
import com.universalcode.org.safeeconomy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.universalcode.org.safeeconomy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the BankMovementPaymentResource REST controller.
 *
 * @see BankMovementPaymentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SafeEconomyApp.class)
public class BankMovementPaymentResourceIntTest {

    private static final String DEFAULT_KIND_OF_MOVEMENT = "AAAAAAAAAA";
    private static final String UPDATED_KIND_OF_MOVEMENT = "BBBBBBBBBB";

    private static final Instant DEFAULT_DATE_OF_MOVEMENT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_OF_MOVEMENT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Double DEFAULT_ACREDITATION_AMMOUNT = 1D;
    private static final Double UPDATED_ACREDITATION_AMMOUNT = 2D;

    private static final Boolean DEFAULT_IS_PAID = false;
    private static final Boolean UPDATED_IS_PAID = true;

    private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENT = "BBBBBBBBBB";

    private static final Integer DEFAULT_PRIORITY = 1;
    private static final Integer UPDATED_PRIORITY = 2;

    @Autowired
    private BankMovementPaymentRepository bankMovementPaymentRepository;

    @Autowired
    private BankMovementPaymentMapper bankMovementPaymentMapper;

    @Autowired
    private BankMovementPaymentService bankMovementPaymentService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restBankMovementPaymentMockMvc;

    private BankMovementPayment bankMovementPayment;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BankMovementPaymentResource bankMovementPaymentResource = new BankMovementPaymentResource(bankMovementPaymentService);
        this.restBankMovementPaymentMockMvc = MockMvcBuilders.standaloneSetup(bankMovementPaymentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BankMovementPayment createEntity(EntityManager em) {
        BankMovementPayment bankMovementPayment = new BankMovementPayment()
            .kindOfMovement(DEFAULT_KIND_OF_MOVEMENT)
            .dateOfMovement(DEFAULT_DATE_OF_MOVEMENT)
            .description(DEFAULT_DESCRIPTION)
            .acreditationAmmount(DEFAULT_ACREDITATION_AMMOUNT)
            .isPaid(DEFAULT_IS_PAID)
            .comment(DEFAULT_COMMENT)
            .priority(DEFAULT_PRIORITY);
        return bankMovementPayment;
    }

    @Before
    public void initTest() {
        bankMovementPayment = createEntity(em);
    }

    @Test
    @Transactional
    public void createBankMovementPayment() throws Exception {
        int databaseSizeBeforeCreate = bankMovementPaymentRepository.findAll().size();

        // Create the BankMovementPayment
        BankMovementPaymentDTO bankMovementPaymentDTO = bankMovementPaymentMapper.toDto(bankMovementPayment);
        restBankMovementPaymentMockMvc.perform(post("/api/bank-movement-payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankMovementPaymentDTO)))
            .andExpect(status().isCreated());

        // Validate the BankMovementPayment in the database
        List<BankMovementPayment> bankMovementPaymentList = bankMovementPaymentRepository.findAll();
        assertThat(bankMovementPaymentList).hasSize(databaseSizeBeforeCreate + 1);
        BankMovementPayment testBankMovementPayment = bankMovementPaymentList.get(bankMovementPaymentList.size() - 1);
        assertThat(testBankMovementPayment.getKindOfMovement()).isEqualTo(DEFAULT_KIND_OF_MOVEMENT);
        assertThat(testBankMovementPayment.getDateOfMovement()).isEqualTo(DEFAULT_DATE_OF_MOVEMENT);
        assertThat(testBankMovementPayment.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testBankMovementPayment.getAcreditationAmmount()).isEqualTo(DEFAULT_ACREDITATION_AMMOUNT);
        assertThat(testBankMovementPayment.isIsPaid()).isEqualTo(DEFAULT_IS_PAID);
        assertThat(testBankMovementPayment.getComment()).isEqualTo(DEFAULT_COMMENT);
        assertThat(testBankMovementPayment.getPriority()).isEqualTo(DEFAULT_PRIORITY);
    }

    @Test
    @Transactional
    public void createBankMovementPaymentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bankMovementPaymentRepository.findAll().size();

        // Create the BankMovementPayment with an existing ID
        bankMovementPayment.setId(1L);
        BankMovementPaymentDTO bankMovementPaymentDTO = bankMovementPaymentMapper.toDto(bankMovementPayment);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBankMovementPaymentMockMvc.perform(post("/api/bank-movement-payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankMovementPaymentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BankMovementPayment in the database
        List<BankMovementPayment> bankMovementPaymentList = bankMovementPaymentRepository.findAll();
        assertThat(bankMovementPaymentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllBankMovementPayments() throws Exception {
        // Initialize the database
        bankMovementPaymentRepository.saveAndFlush(bankMovementPayment);

        // Get all the bankMovementPaymentList
        restBankMovementPaymentMockMvc.perform(get("/api/bank-movement-payments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bankMovementPayment.getId().intValue())))
            .andExpect(jsonPath("$.[*].kindOfMovement").value(hasItem(DEFAULT_KIND_OF_MOVEMENT.toString())))
            .andExpect(jsonPath("$.[*].dateOfMovement").value(hasItem(DEFAULT_DATE_OF_MOVEMENT.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].acreditationAmmount").value(hasItem(DEFAULT_ACREDITATION_AMMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].isPaid").value(hasItem(DEFAULT_IS_PAID.booleanValue())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())))
            .andExpect(jsonPath("$.[*].priority").value(hasItem(DEFAULT_PRIORITY)));
    }

    @Test
    @Transactional
    public void getBankMovementPayment() throws Exception {
        // Initialize the database
        bankMovementPaymentRepository.saveAndFlush(bankMovementPayment);

        // Get the bankMovementPayment
        restBankMovementPaymentMockMvc.perform(get("/api/bank-movement-payments/{id}", bankMovementPayment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bankMovementPayment.getId().intValue()))
            .andExpect(jsonPath("$.kindOfMovement").value(DEFAULT_KIND_OF_MOVEMENT.toString()))
            .andExpect(jsonPath("$.dateOfMovement").value(DEFAULT_DATE_OF_MOVEMENT.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.acreditationAmmount").value(DEFAULT_ACREDITATION_AMMOUNT.doubleValue()))
            .andExpect(jsonPath("$.isPaid").value(DEFAULT_IS_PAID.booleanValue()))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT.toString()))
            .andExpect(jsonPath("$.priority").value(DEFAULT_PRIORITY));
    }

    @Test
    @Transactional
    public void getNonExistingBankMovementPayment() throws Exception {
        // Get the bankMovementPayment
        restBankMovementPaymentMockMvc.perform(get("/api/bank-movement-payments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBankMovementPayment() throws Exception {
        // Initialize the database
        bankMovementPaymentRepository.saveAndFlush(bankMovementPayment);
        int databaseSizeBeforeUpdate = bankMovementPaymentRepository.findAll().size();

        // Update the bankMovementPayment
        BankMovementPayment updatedBankMovementPayment = bankMovementPaymentRepository.findOne(bankMovementPayment.getId());
        // Disconnect from session so that the updates on updatedBankMovementPayment are not directly saved in db
        em.detach(updatedBankMovementPayment);
        updatedBankMovementPayment
            .kindOfMovement(UPDATED_KIND_OF_MOVEMENT)
            .dateOfMovement(UPDATED_DATE_OF_MOVEMENT)
            .description(UPDATED_DESCRIPTION)
            .acreditationAmmount(UPDATED_ACREDITATION_AMMOUNT)
            .isPaid(UPDATED_IS_PAID)
            .comment(UPDATED_COMMENT)
            .priority(UPDATED_PRIORITY);
        BankMovementPaymentDTO bankMovementPaymentDTO = bankMovementPaymentMapper.toDto(updatedBankMovementPayment);

        restBankMovementPaymentMockMvc.perform(put("/api/bank-movement-payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankMovementPaymentDTO)))
            .andExpect(status().isOk());

        // Validate the BankMovementPayment in the database
        List<BankMovementPayment> bankMovementPaymentList = bankMovementPaymentRepository.findAll();
        assertThat(bankMovementPaymentList).hasSize(databaseSizeBeforeUpdate);
        BankMovementPayment testBankMovementPayment = bankMovementPaymentList.get(bankMovementPaymentList.size() - 1);
        assertThat(testBankMovementPayment.getKindOfMovement()).isEqualTo(UPDATED_KIND_OF_MOVEMENT);
        assertThat(testBankMovementPayment.getDateOfMovement()).isEqualTo(UPDATED_DATE_OF_MOVEMENT);
        assertThat(testBankMovementPayment.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testBankMovementPayment.getAcreditationAmmount()).isEqualTo(UPDATED_ACREDITATION_AMMOUNT);
        assertThat(testBankMovementPayment.isIsPaid()).isEqualTo(UPDATED_IS_PAID);
        assertThat(testBankMovementPayment.getComment()).isEqualTo(UPDATED_COMMENT);
        assertThat(testBankMovementPayment.getPriority()).isEqualTo(UPDATED_PRIORITY);
    }

    @Test
    @Transactional
    public void updateNonExistingBankMovementPayment() throws Exception {
        int databaseSizeBeforeUpdate = bankMovementPaymentRepository.findAll().size();

        // Create the BankMovementPayment
        BankMovementPaymentDTO bankMovementPaymentDTO = bankMovementPaymentMapper.toDto(bankMovementPayment);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restBankMovementPaymentMockMvc.perform(put("/api/bank-movement-payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankMovementPaymentDTO)))
            .andExpect(status().isCreated());

        // Validate the BankMovementPayment in the database
        List<BankMovementPayment> bankMovementPaymentList = bankMovementPaymentRepository.findAll();
        assertThat(bankMovementPaymentList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteBankMovementPayment() throws Exception {
        // Initialize the database
        bankMovementPaymentRepository.saveAndFlush(bankMovementPayment);
        int databaseSizeBeforeDelete = bankMovementPaymentRepository.findAll().size();

        // Get the bankMovementPayment
        restBankMovementPaymentMockMvc.perform(delete("/api/bank-movement-payments/{id}", bankMovementPayment.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<BankMovementPayment> bankMovementPaymentList = bankMovementPaymentRepository.findAll();
        assertThat(bankMovementPaymentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BankMovementPayment.class);
        BankMovementPayment bankMovementPayment1 = new BankMovementPayment();
        bankMovementPayment1.setId(1L);
        BankMovementPayment bankMovementPayment2 = new BankMovementPayment();
        bankMovementPayment2.setId(bankMovementPayment1.getId());
        assertThat(bankMovementPayment1).isEqualTo(bankMovementPayment2);
        bankMovementPayment2.setId(2L);
        assertThat(bankMovementPayment1).isNotEqualTo(bankMovementPayment2);
        bankMovementPayment1.setId(null);
        assertThat(bankMovementPayment1).isNotEqualTo(bankMovementPayment2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BankMovementPaymentDTO.class);
        BankMovementPaymentDTO bankMovementPaymentDTO1 = new BankMovementPaymentDTO();
        bankMovementPaymentDTO1.setId(1L);
        BankMovementPaymentDTO bankMovementPaymentDTO2 = new BankMovementPaymentDTO();
        assertThat(bankMovementPaymentDTO1).isNotEqualTo(bankMovementPaymentDTO2);
        bankMovementPaymentDTO2.setId(bankMovementPaymentDTO1.getId());
        assertThat(bankMovementPaymentDTO1).isEqualTo(bankMovementPaymentDTO2);
        bankMovementPaymentDTO2.setId(2L);
        assertThat(bankMovementPaymentDTO1).isNotEqualTo(bankMovementPaymentDTO2);
        bankMovementPaymentDTO1.setId(null);
        assertThat(bankMovementPaymentDTO1).isNotEqualTo(bankMovementPaymentDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(bankMovementPaymentMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(bankMovementPaymentMapper.fromId(null)).isNull();
    }
}
