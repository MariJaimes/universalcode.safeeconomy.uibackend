package com.universalcode.org.safeeconomy.web.rest;

import com.universalcode.org.safeeconomy.SafeEconomyApp;

import com.universalcode.org.safeeconomy.domain.Binnacle;
import com.universalcode.org.safeeconomy.repository.BinnacleRepository;
import com.universalcode.org.safeeconomy.service.BinnacleService;
import com.universalcode.org.safeeconomy.service.dto.BinnacleDTO;
import com.universalcode.org.safeeconomy.service.mapper.BinnacleMapper;
import com.universalcode.org.safeeconomy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.universalcode.org.safeeconomy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the BinnacleResource REST controller.
 *
 * @see BinnacleResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SafeEconomyApp.class)
public class BinnacleResourceIntTest {

    private static final String DEFAULT_EVENT = "AAAAAAAAAA";
    private static final String UPDATED_EVENT = "BBBBBBBBBB";

    private static final String DEFAULT_USER = "AAAAAAAAAA";
    private static final String UPDATED_USER = "BBBBBBBBBB";

    private static final Instant DEFAULT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private BinnacleRepository binnacleRepository;

    @Autowired
    private BinnacleMapper binnacleMapper;

    @Autowired
    private BinnacleService binnacleService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restBinnacleMockMvc;

    private Binnacle binnacle;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BinnacleResource binnacleResource = new BinnacleResource(binnacleService);
        this.restBinnacleMockMvc = MockMvcBuilders.standaloneSetup(binnacleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Binnacle createEntity(EntityManager em) {
        Binnacle binnacle = new Binnacle()
            .event(DEFAULT_EVENT)
            .user(DEFAULT_USER)
            .date(DEFAULT_DATE);
        return binnacle;
    }

    @Before
    public void initTest() {
        binnacle = createEntity(em);
    }

    @Test
    @Transactional
    public void createBinnacle() throws Exception {
        int databaseSizeBeforeCreate = binnacleRepository.findAll().size();

        // Create the Binnacle
        BinnacleDTO binnacleDTO = binnacleMapper.toDto(binnacle);
        restBinnacleMockMvc.perform(post("/api/binnacles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(binnacleDTO)))
            .andExpect(status().isCreated());

        // Validate the Binnacle in the database
        List<Binnacle> binnacleList = binnacleRepository.findAll();
        assertThat(binnacleList).hasSize(databaseSizeBeforeCreate + 1);
        Binnacle testBinnacle = binnacleList.get(binnacleList.size() - 1);
        assertThat(testBinnacle.getEvent()).isEqualTo(DEFAULT_EVENT);
        assertThat(testBinnacle.getUser()).isEqualTo(DEFAULT_USER);
        assertThat(testBinnacle.getDate()).isEqualTo(DEFAULT_DATE);
    }

    @Test
    @Transactional
    public void createBinnacleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = binnacleRepository.findAll().size();

        // Create the Binnacle with an existing ID
        binnacle.setId(1L);
        BinnacleDTO binnacleDTO = binnacleMapper.toDto(binnacle);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBinnacleMockMvc.perform(post("/api/binnacles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(binnacleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Binnacle in the database
        List<Binnacle> binnacleList = binnacleRepository.findAll();
        assertThat(binnacleList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllBinnacles() throws Exception {
        // Initialize the database
        binnacleRepository.saveAndFlush(binnacle);

        // Get all the binnacleList
        restBinnacleMockMvc.perform(get("/api/binnacles?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(binnacle.getId().intValue())))
            .andExpect(jsonPath("$.[*].event").value(hasItem(DEFAULT_EVENT.toString())))
            .andExpect(jsonPath("$.[*].user").value(hasItem(DEFAULT_USER.toString())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())));
    }

    @Test
    @Transactional
    public void getBinnacle() throws Exception {
        // Initialize the database
        binnacleRepository.saveAndFlush(binnacle);

        // Get the binnacle
        restBinnacleMockMvc.perform(get("/api/binnacles/{id}", binnacle.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(binnacle.getId().intValue()))
            .andExpect(jsonPath("$.event").value(DEFAULT_EVENT.toString()))
            .andExpect(jsonPath("$.user").value(DEFAULT_USER.toString()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingBinnacle() throws Exception {
        // Get the binnacle
        restBinnacleMockMvc.perform(get("/api/binnacles/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBinnacle() throws Exception {
        // Initialize the database
        binnacleRepository.saveAndFlush(binnacle);
        int databaseSizeBeforeUpdate = binnacleRepository.findAll().size();

        // Update the binnacle
        Binnacle updatedBinnacle = binnacleRepository.findOne(binnacle.getId());
        // Disconnect from session so that the updates on updatedBinnacle are not directly saved in db
        em.detach(updatedBinnacle);
        updatedBinnacle
            .event(UPDATED_EVENT)
            .user(UPDATED_USER)
            .date(UPDATED_DATE);
        BinnacleDTO binnacleDTO = binnacleMapper.toDto(updatedBinnacle);

        restBinnacleMockMvc.perform(put("/api/binnacles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(binnacleDTO)))
            .andExpect(status().isOk());

        // Validate the Binnacle in the database
        List<Binnacle> binnacleList = binnacleRepository.findAll();
        assertThat(binnacleList).hasSize(databaseSizeBeforeUpdate);
        Binnacle testBinnacle = binnacleList.get(binnacleList.size() - 1);
        assertThat(testBinnacle.getEvent()).isEqualTo(UPDATED_EVENT);
        assertThat(testBinnacle.getUser()).isEqualTo(UPDATED_USER);
        assertThat(testBinnacle.getDate()).isEqualTo(UPDATED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingBinnacle() throws Exception {
        int databaseSizeBeforeUpdate = binnacleRepository.findAll().size();

        // Create the Binnacle
        BinnacleDTO binnacleDTO = binnacleMapper.toDto(binnacle);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restBinnacleMockMvc.perform(put("/api/binnacles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(binnacleDTO)))
            .andExpect(status().isCreated());

        // Validate the Binnacle in the database
        List<Binnacle> binnacleList = binnacleRepository.findAll();
        assertThat(binnacleList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteBinnacle() throws Exception {
        // Initialize the database
        binnacleRepository.saveAndFlush(binnacle);
        int databaseSizeBeforeDelete = binnacleRepository.findAll().size();

        // Get the binnacle
        restBinnacleMockMvc.perform(delete("/api/binnacles/{id}", binnacle.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Binnacle> binnacleList = binnacleRepository.findAll();
        assertThat(binnacleList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Binnacle.class);
        Binnacle binnacle1 = new Binnacle();
        binnacle1.setId(1L);
        Binnacle binnacle2 = new Binnacle();
        binnacle2.setId(binnacle1.getId());
        assertThat(binnacle1).isEqualTo(binnacle2);
        binnacle2.setId(2L);
        assertThat(binnacle1).isNotEqualTo(binnacle2);
        binnacle1.setId(null);
        assertThat(binnacle1).isNotEqualTo(binnacle2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BinnacleDTO.class);
        BinnacleDTO binnacleDTO1 = new BinnacleDTO();
        binnacleDTO1.setId(1L);
        BinnacleDTO binnacleDTO2 = new BinnacleDTO();
        assertThat(binnacleDTO1).isNotEqualTo(binnacleDTO2);
        binnacleDTO2.setId(binnacleDTO1.getId());
        assertThat(binnacleDTO1).isEqualTo(binnacleDTO2);
        binnacleDTO2.setId(2L);
        assertThat(binnacleDTO1).isNotEqualTo(binnacleDTO2);
        binnacleDTO1.setId(null);
        assertThat(binnacleDTO1).isNotEqualTo(binnacleDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(binnacleMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(binnacleMapper.fromId(null)).isNull();
    }
}
