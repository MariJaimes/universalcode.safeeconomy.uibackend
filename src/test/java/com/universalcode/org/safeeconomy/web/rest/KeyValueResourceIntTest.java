package com.universalcode.org.safeeconomy.web.rest;

import com.universalcode.org.safeeconomy.SafeEconomyApp;

import com.universalcode.org.safeeconomy.domain.KeyValue;
import com.universalcode.org.safeeconomy.repository.KeyValueRepository;
import com.universalcode.org.safeeconomy.service.KeyValueService;
import com.universalcode.org.safeeconomy.service.dto.KeyValueDTO;
import com.universalcode.org.safeeconomy.service.mapper.KeyValueMapper;
import com.universalcode.org.safeeconomy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.universalcode.org.safeeconomy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the KeyValueResource REST controller.
 *
 * @see KeyValueResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SafeEconomyApp.class)
public class KeyValueResourceIntTest {

    private static final String DEFAULT_GROUP = "AAAAAAAAAA";
    private static final String UPDATED_GROUP = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private KeyValueRepository keyValueRepository;

    @Autowired
    private KeyValueMapper keyValueMapper;

    @Autowired
    private KeyValueService keyValueService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restKeyValueMockMvc;

    private KeyValue keyValue;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final KeyValueResource keyValueResource = new KeyValueResource(keyValueService);
        this.restKeyValueMockMvc = MockMvcBuilders.standaloneSetup(keyValueResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static KeyValue createEntity(EntityManager em) {
        KeyValue keyValue = new KeyValue()
            .group(DEFAULT_GROUP)
            .description(DEFAULT_DESCRIPTION);
        return keyValue;
    }

    @Before
    public void initTest() {
        keyValue = createEntity(em);
    }

    @Test
    @Transactional
    public void createKeyValue() throws Exception {
        int databaseSizeBeforeCreate = keyValueRepository.findAll().size();

        // Create the KeyValue
        KeyValueDTO keyValueDTO = keyValueMapper.toDto(keyValue);
        restKeyValueMockMvc.perform(post("/api/key-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(keyValueDTO)))
            .andExpect(status().isCreated());

        // Validate the KeyValue in the database
        List<KeyValue> keyValueList = keyValueRepository.findAll();
        assertThat(keyValueList).hasSize(databaseSizeBeforeCreate + 1);
        KeyValue testKeyValue = keyValueList.get(keyValueList.size() - 1);
        assertThat(testKeyValue.getGroup()).isEqualTo(DEFAULT_GROUP);
        assertThat(testKeyValue.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createKeyValueWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = keyValueRepository.findAll().size();

        // Create the KeyValue with an existing ID
        keyValue.setId(1L);
        KeyValueDTO keyValueDTO = keyValueMapper.toDto(keyValue);

        // An entity with an existing ID cannot be created, so this API call must fail
        restKeyValueMockMvc.perform(post("/api/key-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(keyValueDTO)))
            .andExpect(status().isBadRequest());

        // Validate the KeyValue in the database
        List<KeyValue> keyValueList = keyValueRepository.findAll();
        assertThat(keyValueList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllKeyValues() throws Exception {
        // Initialize the database
        keyValueRepository.saveAndFlush(keyValue);

        // Get all the keyValueList
        restKeyValueMockMvc.perform(get("/api/key-values?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(keyValue.getId().intValue())))
            .andExpect(jsonPath("$.[*].group").value(hasItem(DEFAULT_GROUP.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getKeyValue() throws Exception {
        // Initialize the database
        keyValueRepository.saveAndFlush(keyValue);

        // Get the keyValue
        restKeyValueMockMvc.perform(get("/api/key-values/{id}", keyValue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(keyValue.getId().intValue()))
            .andExpect(jsonPath("$.group").value(DEFAULT_GROUP.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingKeyValue() throws Exception {
        // Get the keyValue
        restKeyValueMockMvc.perform(get("/api/key-values/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateKeyValue() throws Exception {
        // Initialize the database
        keyValueRepository.saveAndFlush(keyValue);
        int databaseSizeBeforeUpdate = keyValueRepository.findAll().size();

        // Update the keyValue
        KeyValue updatedKeyValue = keyValueRepository.findOne(keyValue.getId());
        // Disconnect from session so that the updates on updatedKeyValue are not directly saved in db
        em.detach(updatedKeyValue);
        updatedKeyValue
            .group(UPDATED_GROUP)
            .description(UPDATED_DESCRIPTION);
        KeyValueDTO keyValueDTO = keyValueMapper.toDto(updatedKeyValue);

        restKeyValueMockMvc.perform(put("/api/key-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(keyValueDTO)))
            .andExpect(status().isOk());

        // Validate the KeyValue in the database
        List<KeyValue> keyValueList = keyValueRepository.findAll();
        assertThat(keyValueList).hasSize(databaseSizeBeforeUpdate);
        KeyValue testKeyValue = keyValueList.get(keyValueList.size() - 1);
        assertThat(testKeyValue.getGroup()).isEqualTo(UPDATED_GROUP);
        assertThat(testKeyValue.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingKeyValue() throws Exception {
        int databaseSizeBeforeUpdate = keyValueRepository.findAll().size();

        // Create the KeyValue
        KeyValueDTO keyValueDTO = keyValueMapper.toDto(keyValue);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restKeyValueMockMvc.perform(put("/api/key-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(keyValueDTO)))
            .andExpect(status().isCreated());

        // Validate the KeyValue in the database
        List<KeyValue> keyValueList = keyValueRepository.findAll();
        assertThat(keyValueList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteKeyValue() throws Exception {
        // Initialize the database
        keyValueRepository.saveAndFlush(keyValue);
        int databaseSizeBeforeDelete = keyValueRepository.findAll().size();

        // Get the keyValue
        restKeyValueMockMvc.perform(delete("/api/key-values/{id}", keyValue.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<KeyValue> keyValueList = keyValueRepository.findAll();
        assertThat(keyValueList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(KeyValue.class);
        KeyValue keyValue1 = new KeyValue();
        keyValue1.setId(1L);
        KeyValue keyValue2 = new KeyValue();
        keyValue2.setId(keyValue1.getId());
        assertThat(keyValue1).isEqualTo(keyValue2);
        keyValue2.setId(2L);
        assertThat(keyValue1).isNotEqualTo(keyValue2);
        keyValue1.setId(null);
        assertThat(keyValue1).isNotEqualTo(keyValue2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(KeyValueDTO.class);
        KeyValueDTO keyValueDTO1 = new KeyValueDTO();
        keyValueDTO1.setId(1L);
        KeyValueDTO keyValueDTO2 = new KeyValueDTO();
        assertThat(keyValueDTO1).isNotEqualTo(keyValueDTO2);
        keyValueDTO2.setId(keyValueDTO1.getId());
        assertThat(keyValueDTO1).isEqualTo(keyValueDTO2);
        keyValueDTO2.setId(2L);
        assertThat(keyValueDTO1).isNotEqualTo(keyValueDTO2);
        keyValueDTO1.setId(null);
        assertThat(keyValueDTO1).isNotEqualTo(keyValueDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(keyValueMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(keyValueMapper.fromId(null)).isNull();
    }
}
