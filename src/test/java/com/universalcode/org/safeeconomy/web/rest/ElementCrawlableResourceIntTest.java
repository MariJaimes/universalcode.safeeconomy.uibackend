package com.universalcode.org.safeeconomy.web.rest;

import com.universalcode.org.safeeconomy.SafeEconomyApp;

import com.universalcode.org.safeeconomy.domain.ElementCrawlable;
import com.universalcode.org.safeeconomy.repository.ElementCrawlableRepository;
import com.universalcode.org.safeeconomy.service.ElementCrawlableService;
import com.universalcode.org.safeeconomy.service.dto.ElementCrawlableDTO;
import com.universalcode.org.safeeconomy.service.mapper.ElementCrawlableMapper;
import com.universalcode.org.safeeconomy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.universalcode.org.safeeconomy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ElementCrawlableResource REST controller.
 *
 * @see ElementCrawlableResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SafeEconomyApp.class)
public class ElementCrawlableResourceIntTest {

    private static final Integer DEFAULT_BANK_ID = 1;
    private static final Integer UPDATED_BANK_ID = 2;

    private static final Integer DEFAULT_USER_ID = 1;
    private static final Integer UPDATED_USER_ID = 2;

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final Boolean DEFAULT_WAS_PROCESSED = false;
    private static final Boolean UPDATED_WAS_PROCESSED = true;

    @Autowired
    private ElementCrawlableRepository elementCrawlableRepository;

    @Autowired
    private ElementCrawlableMapper elementCrawlableMapper;

    @Autowired
    private ElementCrawlableService elementCrawlableService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restElementCrawlableMockMvc;

    private ElementCrawlable elementCrawlable;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ElementCrawlableResource elementCrawlableResource = new ElementCrawlableResource(elementCrawlableService);
        this.restElementCrawlableMockMvc = MockMvcBuilders.standaloneSetup(elementCrawlableResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ElementCrawlable createEntity(EntityManager em) {
        ElementCrawlable elementCrawlable = new ElementCrawlable()
            .bankId(DEFAULT_BANK_ID)
            .userId(DEFAULT_USER_ID)
            .url(DEFAULT_URL)
            .wasProcessed(DEFAULT_WAS_PROCESSED);
        return elementCrawlable;
    }

    @Before
    public void initTest() {
        elementCrawlable = createEntity(em);
    }

    @Test
    @Transactional
    public void createElementCrawlable() throws Exception {
        int databaseSizeBeforeCreate = elementCrawlableRepository.findAll().size();

        // Create the ElementCrawlable
        ElementCrawlableDTO elementCrawlableDTO = elementCrawlableMapper.toDto(elementCrawlable);
        restElementCrawlableMockMvc.perform(post("/api/element-crawlables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(elementCrawlableDTO)))
            .andExpect(status().isCreated());

        // Validate the ElementCrawlable in the database
        List<ElementCrawlable> elementCrawlableList = elementCrawlableRepository.findAll();
        assertThat(elementCrawlableList).hasSize(databaseSizeBeforeCreate + 1);
        ElementCrawlable testElementCrawlable = elementCrawlableList.get(elementCrawlableList.size() - 1);
        assertThat(testElementCrawlable.getBankId()).isEqualTo(DEFAULT_BANK_ID);
        assertThat(testElementCrawlable.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testElementCrawlable.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testElementCrawlable.isWasProcessed()).isEqualTo(DEFAULT_WAS_PROCESSED);
    }

    @Test
    @Transactional
    public void createElementCrawlableWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = elementCrawlableRepository.findAll().size();

        // Create the ElementCrawlable with an existing ID
        elementCrawlable.setId(1L);
        ElementCrawlableDTO elementCrawlableDTO = elementCrawlableMapper.toDto(elementCrawlable);

        // An entity with an existing ID cannot be created, so this API call must fail
        restElementCrawlableMockMvc.perform(post("/api/element-crawlables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(elementCrawlableDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ElementCrawlable in the database
        List<ElementCrawlable> elementCrawlableList = elementCrawlableRepository.findAll();
        assertThat(elementCrawlableList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllElementCrawlables() throws Exception {
        // Initialize the database
        elementCrawlableRepository.saveAndFlush(elementCrawlable);

        // Get all the elementCrawlableList
        restElementCrawlableMockMvc.perform(get("/api/element-crawlables?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(elementCrawlable.getId().intValue())))
            .andExpect(jsonPath("$.[*].bankId").value(hasItem(DEFAULT_BANK_ID)))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].wasProcessed").value(hasItem(DEFAULT_WAS_PROCESSED.booleanValue())));
    }

    @Test
    @Transactional
    public void getElementCrawlable() throws Exception {
        // Initialize the database
        elementCrawlableRepository.saveAndFlush(elementCrawlable);

        // Get the elementCrawlable
        restElementCrawlableMockMvc.perform(get("/api/element-crawlables/{id}", elementCrawlable.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(elementCrawlable.getId().intValue()))
            .andExpect(jsonPath("$.bankId").value(DEFAULT_BANK_ID))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.wasProcessed").value(DEFAULT_WAS_PROCESSED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingElementCrawlable() throws Exception {
        // Get the elementCrawlable
        restElementCrawlableMockMvc.perform(get("/api/element-crawlables/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateElementCrawlable() throws Exception {
        // Initialize the database
        elementCrawlableRepository.saveAndFlush(elementCrawlable);
        int databaseSizeBeforeUpdate = elementCrawlableRepository.findAll().size();

        // Update the elementCrawlable
        ElementCrawlable updatedElementCrawlable = elementCrawlableRepository.findOne(elementCrawlable.getId());
        // Disconnect from session so that the updates on updatedElementCrawlable are not directly saved in db
        em.detach(updatedElementCrawlable);
        updatedElementCrawlable
            .bankId(UPDATED_BANK_ID)
            .userId(UPDATED_USER_ID)
            .url(UPDATED_URL)
            .wasProcessed(UPDATED_WAS_PROCESSED);
        ElementCrawlableDTO elementCrawlableDTO = elementCrawlableMapper.toDto(updatedElementCrawlable);

        restElementCrawlableMockMvc.perform(put("/api/element-crawlables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(elementCrawlableDTO)))
            .andExpect(status().isOk());

        // Validate the ElementCrawlable in the database
        List<ElementCrawlable> elementCrawlableList = elementCrawlableRepository.findAll();
        assertThat(elementCrawlableList).hasSize(databaseSizeBeforeUpdate);
        ElementCrawlable testElementCrawlable = elementCrawlableList.get(elementCrawlableList.size() - 1);
        assertThat(testElementCrawlable.getBankId()).isEqualTo(UPDATED_BANK_ID);
        assertThat(testElementCrawlable.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testElementCrawlable.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testElementCrawlable.isWasProcessed()).isEqualTo(UPDATED_WAS_PROCESSED);
    }

    @Test
    @Transactional
    public void updateNonExistingElementCrawlable() throws Exception {
        int databaseSizeBeforeUpdate = elementCrawlableRepository.findAll().size();

        // Create the ElementCrawlable
        ElementCrawlableDTO elementCrawlableDTO = elementCrawlableMapper.toDto(elementCrawlable);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restElementCrawlableMockMvc.perform(put("/api/element-crawlables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(elementCrawlableDTO)))
            .andExpect(status().isCreated());

        // Validate the ElementCrawlable in the database
        List<ElementCrawlable> elementCrawlableList = elementCrawlableRepository.findAll();
        assertThat(elementCrawlableList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteElementCrawlable() throws Exception {
        // Initialize the database
        elementCrawlableRepository.saveAndFlush(elementCrawlable);
        int databaseSizeBeforeDelete = elementCrawlableRepository.findAll().size();

        // Get the elementCrawlable
        restElementCrawlableMockMvc.perform(delete("/api/element-crawlables/{id}", elementCrawlable.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ElementCrawlable> elementCrawlableList = elementCrawlableRepository.findAll();
        assertThat(elementCrawlableList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ElementCrawlable.class);
        ElementCrawlable elementCrawlable1 = new ElementCrawlable();
        elementCrawlable1.setId(1L);
        ElementCrawlable elementCrawlable2 = new ElementCrawlable();
        elementCrawlable2.setId(elementCrawlable1.getId());
        assertThat(elementCrawlable1).isEqualTo(elementCrawlable2);
        elementCrawlable2.setId(2L);
        assertThat(elementCrawlable1).isNotEqualTo(elementCrawlable2);
        elementCrawlable1.setId(null);
        assertThat(elementCrawlable1).isNotEqualTo(elementCrawlable2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ElementCrawlableDTO.class);
        ElementCrawlableDTO elementCrawlableDTO1 = new ElementCrawlableDTO();
        elementCrawlableDTO1.setId(1L);
        ElementCrawlableDTO elementCrawlableDTO2 = new ElementCrawlableDTO();
        assertThat(elementCrawlableDTO1).isNotEqualTo(elementCrawlableDTO2);
        elementCrawlableDTO2.setId(elementCrawlableDTO1.getId());
        assertThat(elementCrawlableDTO1).isEqualTo(elementCrawlableDTO2);
        elementCrawlableDTO2.setId(2L);
        assertThat(elementCrawlableDTO1).isNotEqualTo(elementCrawlableDTO2);
        elementCrawlableDTO1.setId(null);
        assertThat(elementCrawlableDTO1).isNotEqualTo(elementCrawlableDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(elementCrawlableMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(elementCrawlableMapper.fromId(null)).isNull();
    }
}
