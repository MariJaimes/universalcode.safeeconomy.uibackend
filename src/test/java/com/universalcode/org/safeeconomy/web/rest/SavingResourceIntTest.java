package com.universalcode.org.safeeconomy.web.rest;

import com.universalcode.org.safeeconomy.SafeEconomyApp;

import com.universalcode.org.safeeconomy.domain.Saving;
import com.universalcode.org.safeeconomy.repository.SavingRepository;
import com.universalcode.org.safeeconomy.service.SavingService;
import com.universalcode.org.safeeconomy.service.UserSaveEconomyService;
import com.universalcode.org.safeeconomy.service.dto.SavingDTO;
import com.universalcode.org.safeeconomy.service.mapper.SavingMapper;
import com.universalcode.org.safeeconomy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.universalcode.org.safeeconomy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SavingResource REST controller.
 *
 * @see SavingResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SafeEconomyApp.class)
public class SavingResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Instant DEFAULT_DATE_TO_FINISH = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_TO_FINISH = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Double DEFAULT_AMMOUNT = 1D;
    private static final Double UPDATED_AMMOUNT = 2D;

    private static final Double DEFAULT_SAVING_PERCENTAGE = 1D;
    private static final Double UPDATED_SAVING_PERCENTAGE = 2D;

    @Autowired
    private SavingRepository savingRepository;

    @Autowired
    private SavingMapper savingMapper;

    @Autowired
    private SavingService savingService;

    @Autowired
    private UserSaveEconomyService userSaveEconomyService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSavingMockMvc;

    private Saving saving;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SavingResource savingResource = new SavingResource(savingService,userSaveEconomyService);
        this.restSavingMockMvc = MockMvcBuilders.standaloneSetup(savingResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Saving createEntity(EntityManager em) {
        Saving saving = new Saving()
            .description(DEFAULT_DESCRIPTION)
            .dateToFinish(DEFAULT_DATE_TO_FINISH)
            .ammount(DEFAULT_AMMOUNT)
            .savingPercentage(DEFAULT_SAVING_PERCENTAGE);
        return saving;
    }

    @Before
    public void initTest() {
        saving = createEntity(em);
    }

    @Test
    @Transactional
    public void createSaving() throws Exception {
        int databaseSizeBeforeCreate = savingRepository.findAll().size();

        // Create the Saving
        SavingDTO savingDTO = savingMapper.toDto(saving);
        restSavingMockMvc.perform(post("/api/savings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(savingDTO)))
            .andExpect(status().isCreated());

        // Validate the Saving in the database
        List<Saving> savingList = savingRepository.findAll();
        assertThat(savingList).hasSize(databaseSizeBeforeCreate + 1);
        Saving testSaving = savingList.get(savingList.size() - 1);
        assertThat(testSaving.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testSaving.getDateToFinish()).isEqualTo(DEFAULT_DATE_TO_FINISH);
        assertThat(testSaving.getAmmount()).isEqualTo(DEFAULT_AMMOUNT);
        assertThat(testSaving.getSavingPercentage()).isEqualTo(DEFAULT_SAVING_PERCENTAGE);
    }

    @Test
    @Transactional
    public void createSavingWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = savingRepository.findAll().size();

        // Create the Saving with an existing ID
        saving.setId(1L);
        SavingDTO savingDTO = savingMapper.toDto(saving);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSavingMockMvc.perform(post("/api/savings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(savingDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Saving in the database
        List<Saving> savingList = savingRepository.findAll();
        assertThat(savingList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSavings() throws Exception {
        // Initialize the database
        savingRepository.saveAndFlush(saving);

        // Get all the savingList
        restSavingMockMvc.perform(get("/api/savings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(saving.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].dateToFinish").value(hasItem(DEFAULT_DATE_TO_FINISH.toString())))
            .andExpect(jsonPath("$.[*].ammount").value(hasItem(DEFAULT_AMMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].savingPercentage").value(hasItem(DEFAULT_SAVING_PERCENTAGE.doubleValue())));
    }

    @Test
    @Transactional
    public void getSaving() throws Exception {
        // Initialize the database
        savingRepository.saveAndFlush(saving);

        // Get the saving
        restSavingMockMvc.perform(get("/api/savings/{id}", saving.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(saving.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.dateToFinish").value(DEFAULT_DATE_TO_FINISH.toString()))
            .andExpect(jsonPath("$.ammount").value(DEFAULT_AMMOUNT.doubleValue()))
            .andExpect(jsonPath("$.savingPercentage").value(DEFAULT_SAVING_PERCENTAGE.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSaving() throws Exception {
        // Get the saving
        restSavingMockMvc.perform(get("/api/savings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSaving() throws Exception {
        // Initialize the database
        savingRepository.saveAndFlush(saving);
        int databaseSizeBeforeUpdate = savingRepository.findAll().size();

        // Update the saving
        Saving updatedSaving = savingRepository.findOne(saving.getId());
        // Disconnect from session so that the updates on updatedSaving are not directly saved in db
        em.detach(updatedSaving);
        updatedSaving
            .description(UPDATED_DESCRIPTION)
            .dateToFinish(UPDATED_DATE_TO_FINISH)
            .ammount(UPDATED_AMMOUNT)
            .savingPercentage(UPDATED_SAVING_PERCENTAGE);
        SavingDTO savingDTO = savingMapper.toDto(updatedSaving);

        restSavingMockMvc.perform(put("/api/savings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(savingDTO)))
            .andExpect(status().isOk());

        // Validate the Saving in the database
        List<Saving> savingList = savingRepository.findAll();
        assertThat(savingList).hasSize(databaseSizeBeforeUpdate);
        Saving testSaving = savingList.get(savingList.size() - 1);
        assertThat(testSaving.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testSaving.getDateToFinish()).isEqualTo(UPDATED_DATE_TO_FINISH);
        assertThat(testSaving.getAmmount()).isEqualTo(UPDATED_AMMOUNT);
        assertThat(testSaving.getSavingPercentage()).isEqualTo(UPDATED_SAVING_PERCENTAGE);
    }

    @Test
    @Transactional
    public void updateNonExistingSaving() throws Exception {
        int databaseSizeBeforeUpdate = savingRepository.findAll().size();

        // Create the Saving
        SavingDTO savingDTO = savingMapper.toDto(saving);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSavingMockMvc.perform(put("/api/savings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(savingDTO)))
            .andExpect(status().isCreated());

        // Validate the Saving in the database
        List<Saving> savingList = savingRepository.findAll();
        assertThat(savingList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSaving() throws Exception {
        // Initialize the database
        savingRepository.saveAndFlush(saving);
        int databaseSizeBeforeDelete = savingRepository.findAll().size();

        // Get the saving
        restSavingMockMvc.perform(delete("/api/savings/{id}", saving.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Saving> savingList = savingRepository.findAll();
        assertThat(savingList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Saving.class);
        Saving saving1 = new Saving();
        saving1.setId(1L);
        Saving saving2 = new Saving();
        saving2.setId(saving1.getId());
        assertThat(saving1).isEqualTo(saving2);
        saving2.setId(2L);
        assertThat(saving1).isNotEqualTo(saving2);
        saving1.setId(null);
        assertThat(saving1).isNotEqualTo(saving2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SavingDTO.class);
        SavingDTO savingDTO1 = new SavingDTO();
        savingDTO1.setId(1L);
        SavingDTO savingDTO2 = new SavingDTO();
        assertThat(savingDTO1).isNotEqualTo(savingDTO2);
        savingDTO2.setId(savingDTO1.getId());
        assertThat(savingDTO1).isEqualTo(savingDTO2);
        savingDTO2.setId(2L);
        assertThat(savingDTO1).isNotEqualTo(savingDTO2);
        savingDTO1.setId(null);
        assertThat(savingDTO1).isNotEqualTo(savingDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(savingMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(savingMapper.fromId(null)).isNull();
    }
}
