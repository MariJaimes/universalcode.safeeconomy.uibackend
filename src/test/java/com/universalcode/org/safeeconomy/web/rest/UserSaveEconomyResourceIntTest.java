package com.universalcode.org.safeeconomy.web.rest;

import com.universalcode.org.safeeconomy.SafeEconomyApp;

import com.universalcode.org.safeeconomy.domain.UserSaveEconomy;
import com.universalcode.org.safeeconomy.repository.UserSaveEconomyRepository;
import com.universalcode.org.safeeconomy.service.UserSaveEconomyService;
import com.universalcode.org.safeeconomy.service.dto.UserSaveEconomyDTO;
import com.universalcode.org.safeeconomy.service.mapper.UserSaveEconomyMapper;
import com.universalcode.org.safeeconomy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.universalcode.org.safeeconomy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserSaveEconomyResource REST controller.
 *
 * @see UserSaveEconomyResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SafeEconomyApp.class)
public class UserSaveEconomyResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_TELEPHONE = "AAAAAAAAAA";
    private static final String UPDATED_TELEPHONE = "BBBBBBBBBB";

    private static final String DEFAULT_TOKEN = "AAAAAAAAAA";
    private static final String UPDATED_TOKEN = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_VISIT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_VISIT_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_IMAGE = "AAAAAAAAAA";
    private static final String UPDATED_IMAGE = "BBBBBBBBBB";

    private static final String DEFAULT_FACT = "AAAAAAAAAA";
    private static final String UPDATED_FACT = "BBBBBBBBBB";

    @Autowired
    private UserSaveEconomyRepository userSaveEconomyRepository;

    @Autowired
    private UserSaveEconomyMapper userSaveEconomyMapper;

    @Autowired
    private UserSaveEconomyService userSaveEconomyService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restUserSaveEconomyMockMvc;

    private UserSaveEconomy userSaveEconomy;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserSaveEconomyResource userSaveEconomyResource = new UserSaveEconomyResource(userSaveEconomyService);
        this.restUserSaveEconomyMockMvc = MockMvcBuilders.standaloneSetup(userSaveEconomyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserSaveEconomy createEntity(EntityManager em) {
        UserSaveEconomy userSaveEconomy = new UserSaveEconomy()
            .name(DEFAULT_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .telephone(DEFAULT_TELEPHONE)
            .token(DEFAULT_TOKEN)
            .lastVisitDate(DEFAULT_LAST_VISIT_DATE)
            .image(DEFAULT_IMAGE)
            .fact(DEFAULT_FACT);
        return userSaveEconomy;
    }

    @Before
    public void initTest() {
        userSaveEconomy = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserSaveEconomy() throws Exception {
        int databaseSizeBeforeCreate = userSaveEconomyRepository.findAll().size();

        // Create the UserSaveEconomy
        UserSaveEconomyDTO userSaveEconomyDTO = userSaveEconomyMapper.toDto(userSaveEconomy);
        restUserSaveEconomyMockMvc.perform(post("/api/user-save-economies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userSaveEconomyDTO)))
            .andExpect(status().isCreated());

        // Validate the UserSaveEconomy in the database
        List<UserSaveEconomy> userSaveEconomyList = userSaveEconomyRepository.findAll();
        assertThat(userSaveEconomyList).hasSize(databaseSizeBeforeCreate + 1);
        UserSaveEconomy testUserSaveEconomy = userSaveEconomyList.get(userSaveEconomyList.size() - 1);
        assertThat(testUserSaveEconomy.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testUserSaveEconomy.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testUserSaveEconomy.getTelephone()).isEqualTo(DEFAULT_TELEPHONE);
        assertThat(testUserSaveEconomy.getToken()).isEqualTo(DEFAULT_TOKEN);
        assertThat(testUserSaveEconomy.getLastVisitDate()).isEqualTo(DEFAULT_LAST_VISIT_DATE);
        assertThat(testUserSaveEconomy.getImage()).isEqualTo(DEFAULT_IMAGE);
        assertThat(testUserSaveEconomy.getFact()).isEqualTo(DEFAULT_FACT);
    }

    @Test
    @Transactional
    public void createUserSaveEconomyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userSaveEconomyRepository.findAll().size();

        // Create the UserSaveEconomy with an existing ID
        userSaveEconomy.setId(1L);
        UserSaveEconomyDTO userSaveEconomyDTO = userSaveEconomyMapper.toDto(userSaveEconomy);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserSaveEconomyMockMvc.perform(post("/api/user-save-economies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userSaveEconomyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserSaveEconomy in the database
        List<UserSaveEconomy> userSaveEconomyList = userSaveEconomyRepository.findAll();
        assertThat(userSaveEconomyList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllUserSaveEconomies() throws Exception {
        // Initialize the database
        userSaveEconomyRepository.saveAndFlush(userSaveEconomy);

        // Get all the userSaveEconomyList
        restUserSaveEconomyMockMvc.perform(get("/api/user-save-economies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userSaveEconomy.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
            .andExpect(jsonPath("$.[*].telephone").value(hasItem(DEFAULT_TELEPHONE.toString())))
            .andExpect(jsonPath("$.[*].token").value(hasItem(DEFAULT_TOKEN.toString())))
            .andExpect(jsonPath("$.[*].lastVisitDate").value(hasItem(DEFAULT_LAST_VISIT_DATE.toString())))
            .andExpect(jsonPath("$.[*].image").value(hasItem(DEFAULT_IMAGE.toString())))
            .andExpect(jsonPath("$.[*].fact").value(hasItem(DEFAULT_FACT.toString())));
    }

    @Test
    @Transactional
    public void getUserSaveEconomy() throws Exception {
        // Initialize the database
        userSaveEconomyRepository.saveAndFlush(userSaveEconomy);

        // Get the userSaveEconomy
        restUserSaveEconomyMockMvc.perform(get("/api/user-save-economies/{id}", userSaveEconomy.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userSaveEconomy.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME.toString()))
            .andExpect(jsonPath("$.telephone").value(DEFAULT_TELEPHONE.toString()))
            .andExpect(jsonPath("$.token").value(DEFAULT_TOKEN.toString()))
            .andExpect(jsonPath("$.lastVisitDate").value(DEFAULT_LAST_VISIT_DATE.toString()))
            .andExpect(jsonPath("$.image").value(DEFAULT_IMAGE.toString()))
            .andExpect(jsonPath("$.fact").value(DEFAULT_FACT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingUserSaveEconomy() throws Exception {
        // Get the userSaveEconomy
        restUserSaveEconomyMockMvc.perform(get("/api/user-save-economies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserSaveEconomy() throws Exception {
        // Initialize the database
        userSaveEconomyRepository.saveAndFlush(userSaveEconomy);
        int databaseSizeBeforeUpdate = userSaveEconomyRepository.findAll().size();

        // Update the userSaveEconomy
        UserSaveEconomy updatedUserSaveEconomy = userSaveEconomyRepository.findOne(userSaveEconomy.getId());
        // Disconnect from session so that the updates on updatedUserSaveEconomy are not directly saved in db
        em.detach(updatedUserSaveEconomy);
        updatedUserSaveEconomy
            .name(UPDATED_NAME)
            .lastName(UPDATED_LAST_NAME)
            .telephone(UPDATED_TELEPHONE)
            .token(UPDATED_TOKEN)
            .lastVisitDate(UPDATED_LAST_VISIT_DATE)
            .image(UPDATED_IMAGE)
            .fact(UPDATED_FACT);
        UserSaveEconomyDTO userSaveEconomyDTO = userSaveEconomyMapper.toDto(updatedUserSaveEconomy);

        restUserSaveEconomyMockMvc.perform(put("/api/user-save-economies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userSaveEconomyDTO)))
            .andExpect(status().isOk());

        // Validate the UserSaveEconomy in the database
        List<UserSaveEconomy> userSaveEconomyList = userSaveEconomyRepository.findAll();
        assertThat(userSaveEconomyList).hasSize(databaseSizeBeforeUpdate);
        UserSaveEconomy testUserSaveEconomy = userSaveEconomyList.get(userSaveEconomyList.size() - 1);
        assertThat(testUserSaveEconomy.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testUserSaveEconomy.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testUserSaveEconomy.getTelephone()).isEqualTo(UPDATED_TELEPHONE);
        assertThat(testUserSaveEconomy.getToken()).isEqualTo(UPDATED_TOKEN);
        assertThat(testUserSaveEconomy.getLastVisitDate()).isEqualTo(UPDATED_LAST_VISIT_DATE);
        assertThat(testUserSaveEconomy.getImage()).isEqualTo(UPDATED_IMAGE);
        assertThat(testUserSaveEconomy.getFact()).isEqualTo(UPDATED_FACT);
    }

    @Test
    @Transactional
    public void updateNonExistingUserSaveEconomy() throws Exception {
        int databaseSizeBeforeUpdate = userSaveEconomyRepository.findAll().size();

        // Create the UserSaveEconomy
        UserSaveEconomyDTO userSaveEconomyDTO = userSaveEconomyMapper.toDto(userSaveEconomy);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restUserSaveEconomyMockMvc.perform(put("/api/user-save-economies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userSaveEconomyDTO)))
            .andExpect(status().isCreated());

        // Validate the UserSaveEconomy in the database
        List<UserSaveEconomy> userSaveEconomyList = userSaveEconomyRepository.findAll();
        assertThat(userSaveEconomyList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteUserSaveEconomy() throws Exception {
        // Initialize the database
        userSaveEconomyRepository.saveAndFlush(userSaveEconomy);
        int databaseSizeBeforeDelete = userSaveEconomyRepository.findAll().size();

        // Get the userSaveEconomy
        restUserSaveEconomyMockMvc.perform(delete("/api/user-save-economies/{id}", userSaveEconomy.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<UserSaveEconomy> userSaveEconomyList = userSaveEconomyRepository.findAll();
        assertThat(userSaveEconomyList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserSaveEconomy.class);
        UserSaveEconomy userSaveEconomy1 = new UserSaveEconomy();
        userSaveEconomy1.setId(1L);
        UserSaveEconomy userSaveEconomy2 = new UserSaveEconomy();
        userSaveEconomy2.setId(userSaveEconomy1.getId());
        assertThat(userSaveEconomy1).isEqualTo(userSaveEconomy2);
        userSaveEconomy2.setId(2L);
        assertThat(userSaveEconomy1).isNotEqualTo(userSaveEconomy2);
        userSaveEconomy1.setId(null);
        assertThat(userSaveEconomy1).isNotEqualTo(userSaveEconomy2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserSaveEconomyDTO.class);
        UserSaveEconomyDTO userSaveEconomyDTO1 = new UserSaveEconomyDTO();
        userSaveEconomyDTO1.setId(1L);
        UserSaveEconomyDTO userSaveEconomyDTO2 = new UserSaveEconomyDTO();
        assertThat(userSaveEconomyDTO1).isNotEqualTo(userSaveEconomyDTO2);
        userSaveEconomyDTO2.setId(userSaveEconomyDTO1.getId());
        assertThat(userSaveEconomyDTO1).isEqualTo(userSaveEconomyDTO2);
        userSaveEconomyDTO2.setId(2L);
        assertThat(userSaveEconomyDTO1).isNotEqualTo(userSaveEconomyDTO2);
        userSaveEconomyDTO1.setId(null);
        assertThat(userSaveEconomyDTO1).isNotEqualTo(userSaveEconomyDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(userSaveEconomyMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(userSaveEconomyMapper.fromId(null)).isNull();
    }
}
