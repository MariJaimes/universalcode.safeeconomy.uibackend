/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { SafeEconomyTestModule } from '../../../test.module';
import { BinnacleDetailComponent } from '../../../../../../main/webapp/app/entities/binnacle/binnacle-detail.component';
import { BinnacleService } from '../../../../../../main/webapp/app/entities/binnacle/binnacle.service';
import { Binnacle } from '../../../../../../main/webapp/app/entities/binnacle/binnacle.model';

describe('Component Tests', () => {

    describe('Binnacle Management Detail Component', () => {
        let comp: BinnacleDetailComponent;
        let fixture: ComponentFixture<BinnacleDetailComponent>;
        let service: BinnacleService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SafeEconomyTestModule],
                declarations: [BinnacleDetailComponent],
                providers: [
                    BinnacleService
                ]
            })
            .overrideTemplate(BinnacleDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BinnacleDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BinnacleService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Binnacle(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.binnacle).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
