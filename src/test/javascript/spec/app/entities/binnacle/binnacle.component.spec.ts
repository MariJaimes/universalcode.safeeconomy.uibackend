/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SafeEconomyTestModule } from '../../../test.module';
import { BinnacleComponent } from '../../../../../../main/webapp/app/entities/binnacle/binnacle.component';
import { BinnacleService } from '../../../../../../main/webapp/app/entities/binnacle/binnacle.service';
import { Binnacle } from '../../../../../../main/webapp/app/entities/binnacle/binnacle.model';

describe('Component Tests', () => {

    describe('Binnacle Management Component', () => {
        let comp: BinnacleComponent;
        let fixture: ComponentFixture<BinnacleComponent>;
        let service: BinnacleService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SafeEconomyTestModule],
                declarations: [BinnacleComponent],
                providers: [
                    BinnacleService
                ]
            })
            .overrideTemplate(BinnacleComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BinnacleComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BinnacleService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Binnacle(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.binnacles[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
