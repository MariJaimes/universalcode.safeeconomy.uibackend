/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { SafeEconomyTestModule } from '../../../test.module';
import { BankingEntityDetailComponent } from '../../../../../../main/webapp/app/entities/banking-entity/banking-entity-detail.component';
import { BankingEntityService } from '../../../../../../main/webapp/app/entities/banking-entity/banking-entity.service';
import { BankingEntity } from '../../../../../../main/webapp/app/entities/banking-entity/banking-entity.model';

describe('Component Tests', () => {

    describe('BankingEntity Management Detail Component', () => {
        let comp: BankingEntityDetailComponent;
        let fixture: ComponentFixture<BankingEntityDetailComponent>;
        let service: BankingEntityService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SafeEconomyTestModule],
                declarations: [BankingEntityDetailComponent],
                providers: [
                    BankingEntityService
                ]
            })
            .overrideTemplate(BankingEntityDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BankingEntityDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BankingEntityService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new BankingEntity(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.bankingEntity).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
