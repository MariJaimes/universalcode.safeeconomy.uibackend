/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SafeEconomyTestModule } from '../../../test.module';
import { BankingEntityComponent } from '../../../../../../main/webapp/app/entities/banking-entity/banking-entity.component';
import { BankingEntityService } from '../../../../../../main/webapp/app/entities/banking-entity/banking-entity.service';
import { BankingEntity } from '../../../../../../main/webapp/app/entities/banking-entity/banking-entity.model';

describe('Component Tests', () => {

    describe('BankingEntity Management Component', () => {
        let comp: BankingEntityComponent;
        let fixture: ComponentFixture<BankingEntityComponent>;
        let service: BankingEntityService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SafeEconomyTestModule],
                declarations: [BankingEntityComponent],
                providers: [
                    BankingEntityService
                ]
            })
            .overrideTemplate(BankingEntityComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BankingEntityComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BankingEntityService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new BankingEntity(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.bankingEntities[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
