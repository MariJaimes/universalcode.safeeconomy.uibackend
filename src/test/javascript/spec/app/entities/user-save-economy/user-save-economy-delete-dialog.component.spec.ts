/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { SafeEconomyTestModule } from '../../../test.module';
import { UserSaveEconomyDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/user-save-economy/user-save-economy-delete-dialog.component';
import { UserSaveEconomyService } from '../../../../../../main/webapp/app/entities/user-save-economy/user-save-economy.service';

describe('Component Tests', () => {

    describe('UserSaveEconomy Management Delete Component', () => {
        let comp: UserSaveEconomyDeleteDialogComponent;
        let fixture: ComponentFixture<UserSaveEconomyDeleteDialogComponent>;
        let service: UserSaveEconomyService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SafeEconomyTestModule],
                declarations: [UserSaveEconomyDeleteDialogComponent],
                providers: [
                    UserSaveEconomyService
                ]
            })
            .overrideTemplate(UserSaveEconomyDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(UserSaveEconomyDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(UserSaveEconomyService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
