/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { SafeEconomyTestModule } from '../../../test.module';
import { UserSaveEconomyDetailComponent } from '../../../../../../main/webapp/app/entities/user-save-economy/user-save-economy-detail.component';
import { UserSaveEconomyService } from '../../../../../../main/webapp/app/entities/user-save-economy/user-save-economy.service';
import { UserSaveEconomy } from '../../../../../../main/webapp/app/entities/user-save-economy/user-save-economy.model';

describe('Component Tests', () => {

    describe('UserSaveEconomy Management Detail Component', () => {
        let comp: UserSaveEconomyDetailComponent;
        let fixture: ComponentFixture<UserSaveEconomyDetailComponent>;
        let service: UserSaveEconomyService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SafeEconomyTestModule],
                declarations: [UserSaveEconomyDetailComponent],
                providers: [
                    UserSaveEconomyService
                ]
            })
            .overrideTemplate(UserSaveEconomyDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(UserSaveEconomyDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(UserSaveEconomyService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new UserSaveEconomy(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.userSaveEconomy).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
