/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SafeEconomyTestModule } from '../../../test.module';
import { UserSaveEconomyComponent } from '../../../../../../main/webapp/app/entities/user-save-economy/user-save-economy.component';
import { UserSaveEconomyService } from '../../../../../../main/webapp/app/entities/user-save-economy/user-save-economy.service';
import { UserSaveEconomy } from '../../../../../../main/webapp/app/entities/user-save-economy/user-save-economy.model';

describe('Component Tests', () => {

    describe('UserSaveEconomy Management Component', () => {
        let comp: UserSaveEconomyComponent;
        let fixture: ComponentFixture<UserSaveEconomyComponent>;
        let service: UserSaveEconomyService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SafeEconomyTestModule],
                declarations: [UserSaveEconomyComponent],
                providers: [
                    UserSaveEconomyService
                ]
            })
            .overrideTemplate(UserSaveEconomyComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(UserSaveEconomyComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(UserSaveEconomyService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new UserSaveEconomy(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.userSaveEconomies[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
