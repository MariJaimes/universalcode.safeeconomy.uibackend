/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { SafeEconomyTestModule } from '../../../test.module';
import { SavingDetailComponent } from '../../../../../../main/webapp/app/entities/saving/saving-detail.component';
import { SavingService } from '../../../../../../main/webapp/app/entities/saving/saving.service';
import { Saving } from '../../../../../../main/webapp/app/entities/saving/saving.model';

describe('Component Tests', () => {

    describe('Saving Management Detail Component', () => {
        let comp: SavingDetailComponent;
        let fixture: ComponentFixture<SavingDetailComponent>;
        let service: SavingService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SafeEconomyTestModule],
                declarations: [SavingDetailComponent],
                providers: [
                    SavingService
                ]
            })
            .overrideTemplate(SavingDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SavingDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SavingService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Saving(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.saving).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
