/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SafeEconomyTestModule } from '../../../test.module';
import { SavingComponent } from '../../../../../../main/webapp/app/entities/saving/saving.component';
import { SavingService } from '../../../../../../main/webapp/app/entities/saving/saving.service';
import { Saving } from '../../../../../../main/webapp/app/entities/saving/saving.model';

describe('Component Tests', () => {

    describe('Saving Management Component', () => {
        let comp: SavingComponent;
        let fixture: ComponentFixture<SavingComponent>;
        let service: SavingService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SafeEconomyTestModule],
                declarations: [SavingComponent],
                providers: [
                    SavingService
                ]
            })
            .overrideTemplate(SavingComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SavingComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SavingService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Saving(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.savings[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
