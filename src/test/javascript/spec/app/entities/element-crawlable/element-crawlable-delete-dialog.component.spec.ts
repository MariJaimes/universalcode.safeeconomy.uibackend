/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { SafeEconomyTestModule } from '../../../test.module';
import { ElementCrawlableDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/element-crawlable/element-crawlable-delete-dialog.component';
import { ElementCrawlableService } from '../../../../../../main/webapp/app/entities/element-crawlable/element-crawlable.service';

describe('Component Tests', () => {

    describe('ElementCrawlable Management Delete Component', () => {
        let comp: ElementCrawlableDeleteDialogComponent;
        let fixture: ComponentFixture<ElementCrawlableDeleteDialogComponent>;
        let service: ElementCrawlableService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SafeEconomyTestModule],
                declarations: [ElementCrawlableDeleteDialogComponent],
                providers: [
                    ElementCrawlableService
                ]
            })
            .overrideTemplate(ElementCrawlableDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ElementCrawlableDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ElementCrawlableService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
