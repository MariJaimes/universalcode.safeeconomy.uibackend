/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { SafeEconomyTestModule } from '../../../test.module';
import { ElementCrawlableDialogComponent } from '../../../../../../main/webapp/app/entities/element-crawlable/element-crawlable-dialog.component';
import { ElementCrawlableService } from '../../../../../../main/webapp/app/entities/element-crawlable/element-crawlable.service';
import { ElementCrawlable } from '../../../../../../main/webapp/app/entities/element-crawlable/element-crawlable.model';

describe('Component Tests', () => {

    describe('ElementCrawlable Management Dialog Component', () => {
        let comp: ElementCrawlableDialogComponent;
        let fixture: ComponentFixture<ElementCrawlableDialogComponent>;
        let service: ElementCrawlableService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SafeEconomyTestModule],
                declarations: [ElementCrawlableDialogComponent],
                providers: [
                    ElementCrawlableService
                ]
            })
            .overrideTemplate(ElementCrawlableDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ElementCrawlableDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ElementCrawlableService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ElementCrawlable(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.elementCrawlable = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'elementCrawlableListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ElementCrawlable();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.elementCrawlable = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'elementCrawlableListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
