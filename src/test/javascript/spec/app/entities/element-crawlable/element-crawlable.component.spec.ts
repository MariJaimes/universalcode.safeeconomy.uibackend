/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SafeEconomyTestModule } from '../../../test.module';
import { ElementCrawlableComponent } from '../../../../../../main/webapp/app/entities/element-crawlable/element-crawlable.component';
import { ElementCrawlableService } from '../../../../../../main/webapp/app/entities/element-crawlable/element-crawlable.service';
import { ElementCrawlable } from '../../../../../../main/webapp/app/entities/element-crawlable/element-crawlable.model';

describe('Component Tests', () => {

    describe('ElementCrawlable Management Component', () => {
        let comp: ElementCrawlableComponent;
        let fixture: ComponentFixture<ElementCrawlableComponent>;
        let service: ElementCrawlableService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SafeEconomyTestModule],
                declarations: [ElementCrawlableComponent],
                providers: [
                    ElementCrawlableService
                ]
            })
            .overrideTemplate(ElementCrawlableComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ElementCrawlableComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ElementCrawlableService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new ElementCrawlable(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.elementCrawlables[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
