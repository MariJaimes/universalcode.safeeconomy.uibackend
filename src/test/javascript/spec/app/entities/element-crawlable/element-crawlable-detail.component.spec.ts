/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { SafeEconomyTestModule } from '../../../test.module';
import { ElementCrawlableDetailComponent } from '../../../../../../main/webapp/app/entities/element-crawlable/element-crawlable-detail.component';
import { ElementCrawlableService } from '../../../../../../main/webapp/app/entities/element-crawlable/element-crawlable.service';
import { ElementCrawlable } from '../../../../../../main/webapp/app/entities/element-crawlable/element-crawlable.model';

describe('Component Tests', () => {

    describe('ElementCrawlable Management Detail Component', () => {
        let comp: ElementCrawlableDetailComponent;
        let fixture: ComponentFixture<ElementCrawlableDetailComponent>;
        let service: ElementCrawlableService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SafeEconomyTestModule],
                declarations: [ElementCrawlableDetailComponent],
                providers: [
                    ElementCrawlableService
                ]
            })
            .overrideTemplate(ElementCrawlableDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ElementCrawlableDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ElementCrawlableService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new ElementCrawlable(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.elementCrawlable).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
