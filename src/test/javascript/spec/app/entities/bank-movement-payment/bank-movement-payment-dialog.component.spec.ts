/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { SafeEconomyTestModule } from '../../../test.module';
import { BankMovementPaymentDialogComponent } from '../../../../../../main/webapp/app/entities/bank-movement-payment/bank-movement-payment-dialog.component';
import { BankMovementPaymentService } from '../../../../../../main/webapp/app/entities/bank-movement-payment/bank-movement-payment.service';
import { BankMovementPayment } from '../../../../../../main/webapp/app/entities/bank-movement-payment/bank-movement-payment.model';
import { UserSaveEconomyService } from '../../../../../../main/webapp/app/entities/user-save-economy';
import { BankAccountService } from '../../../../../../main/webapp/app/entities/bank-account';

describe('Component Tests', () => {

    describe('BankMovementPayment Management Dialog Component', () => {
        let comp: BankMovementPaymentDialogComponent;
        let fixture: ComponentFixture<BankMovementPaymentDialogComponent>;
        let service: BankMovementPaymentService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SafeEconomyTestModule],
                declarations: [BankMovementPaymentDialogComponent],
                providers: [
                    UserSaveEconomyService,
                    BankAccountService,
                    BankMovementPaymentService
                ]
            })
            .overrideTemplate(BankMovementPaymentDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BankMovementPaymentDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BankMovementPaymentService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new BankMovementPayment(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.bankMovementPayment = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'bankMovementPaymentListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new BankMovementPayment();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.bankMovementPayment = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'bankMovementPaymentListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
