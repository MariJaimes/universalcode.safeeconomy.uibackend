/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SafeEconomyTestModule } from '../../../test.module';
import { BankMovementPaymentComponent } from '../../../../../../main/webapp/app/entities/bank-movement-payment/bank-movement-payment.component';
import { BankMovementPaymentService } from '../../../../../../main/webapp/app/entities/bank-movement-payment/bank-movement-payment.service';
import { BankMovementPayment } from '../../../../../../main/webapp/app/entities/bank-movement-payment/bank-movement-payment.model';

describe('Component Tests', () => {

    describe('BankMovementPayment Management Component', () => {
        let comp: BankMovementPaymentComponent;
        let fixture: ComponentFixture<BankMovementPaymentComponent>;
        let service: BankMovementPaymentService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SafeEconomyTestModule],
                declarations: [BankMovementPaymentComponent],
                providers: [
                    BankMovementPaymentService
                ]
            })
            .overrideTemplate(BankMovementPaymentComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BankMovementPaymentComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BankMovementPaymentService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new BankMovementPayment(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.bankMovementPayments[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
