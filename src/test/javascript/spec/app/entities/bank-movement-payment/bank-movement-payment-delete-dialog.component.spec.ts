/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { SafeEconomyTestModule } from '../../../test.module';
import { BankMovementPaymentDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/bank-movement-payment/bank-movement-payment-delete-dialog.component';
import { BankMovementPaymentService } from '../../../../../../main/webapp/app/entities/bank-movement-payment/bank-movement-payment.service';

describe('Component Tests', () => {

    describe('BankMovementPayment Management Delete Component', () => {
        let comp: BankMovementPaymentDeleteDialogComponent;
        let fixture: ComponentFixture<BankMovementPaymentDeleteDialogComponent>;
        let service: BankMovementPaymentService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SafeEconomyTestModule],
                declarations: [BankMovementPaymentDeleteDialogComponent],
                providers: [
                    BankMovementPaymentService
                ]
            })
            .overrideTemplate(BankMovementPaymentDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BankMovementPaymentDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BankMovementPaymentService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
