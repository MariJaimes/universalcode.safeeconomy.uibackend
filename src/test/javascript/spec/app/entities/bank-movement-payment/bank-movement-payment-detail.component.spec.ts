/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { SafeEconomyTestModule } from '../../../test.module';
import { BankMovementPaymentDetailComponent } from '../../../../../../main/webapp/app/entities/bank-movement-payment/bank-movement-payment-detail.component';
import { BankMovementPaymentService } from '../../../../../../main/webapp/app/entities/bank-movement-payment/bank-movement-payment.service';
import { BankMovementPayment } from '../../../../../../main/webapp/app/entities/bank-movement-payment/bank-movement-payment.model';

describe('Component Tests', () => {

    describe('BankMovementPayment Management Detail Component', () => {
        let comp: BankMovementPaymentDetailComponent;
        let fixture: ComponentFixture<BankMovementPaymentDetailComponent>;
        let service: BankMovementPaymentService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SafeEconomyTestModule],
                declarations: [BankMovementPaymentDetailComponent],
                providers: [
                    BankMovementPaymentService
                ]
            })
            .overrideTemplate(BankMovementPaymentDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BankMovementPaymentDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BankMovementPaymentService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new BankMovementPayment(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.bankMovementPayment).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
