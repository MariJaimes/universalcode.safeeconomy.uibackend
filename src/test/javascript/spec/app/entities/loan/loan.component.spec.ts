/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SafeEconomyTestModule } from '../../../test.module';
import { LoanComponent } from '../../../../../../main/webapp/app/entities/loan/loan.component';
import { LoanService } from '../../../../../../main/webapp/app/entities/loan/loan.service';
import { Loan } from '../../../../../../main/webapp/app/entities/loan/loan.model';

describe('Component Tests', () => {

    describe('Loan Management Component', () => {
        let comp: LoanComponent;
        let fixture: ComponentFixture<LoanComponent>;
        let service: LoanService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SafeEconomyTestModule],
                declarations: [LoanComponent],
                providers: [
                    LoanService
                ]
            })
            .overrideTemplate(LoanComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LoanComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LoanService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Loan(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.loans[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
