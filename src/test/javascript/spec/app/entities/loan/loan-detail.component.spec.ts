/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { SafeEconomyTestModule } from '../../../test.module';
import { LoanDetailComponent } from '../../../../../../main/webapp/app/entities/loan/loan-detail.component';
import { LoanService } from '../../../../../../main/webapp/app/entities/loan/loan.service';
import { Loan } from '../../../../../../main/webapp/app/entities/loan/loan.model';

describe('Component Tests', () => {

    describe('Loan Management Detail Component', () => {
        let comp: LoanDetailComponent;
        let fixture: ComponentFixture<LoanDetailComponent>;
        let service: LoanService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SafeEconomyTestModule],
                declarations: [LoanDetailComponent],
                providers: [
                    LoanService
                ]
            })
            .overrideTemplate(LoanDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LoanDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LoanService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Loan(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.loan).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
